FROM ubuntu

RUN apt-get update && apt-get upgrade -y

# Install LaTex
RUN ln -snf /usr/share/zoneinfo/Etc/UTC /etc/localtime && echo "Etc/UTC" > /etc/timezone
RUN apt-get install -y texlive-latex-base texlive-science texlive-latex-extra texlive-fonts-recommended dvipng cm-super xzdec build-essential latexmk 

# Install Python
RUN apt-get install -y python3-dev python3-distutils python3-pip python3-apt python3-pyqt5
RUN pip3 install --upgrade pip
RUN pip3 install matplotlib torch pandas

# Install Manim
RUN apt-get install -y build-essential libcairo2-dev libpango1.0-dev ffmpeg
RUN pip3 install manim manim-slides
