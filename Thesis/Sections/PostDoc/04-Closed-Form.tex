\section{Non-Recursive Forms of the Expected Gain}

To resolve the above issues, we now continue with deriving non-recursive solutions for the expected gain in the different states we can be in. We will utilize these non-recursive solutions in the next section to examine the behavior of the thresholds $t_1$ and $t_3$ and the expected gain as the number of candidates tends to infinity. While our main focus in this section is to establish the correctness of particular non-recursive solutions, we will also provide insights into the derivation of these solutions. To solve the recursive equations for the expected gain, we will use the fact that we have already proven that the optimal strategy operates in 6 phases. In all of those phases, we know the optimal action for all candidates and can thus simplify our recursive equations.

A non-recursive form for the expected gain for $k \geq t_5-1$ has already been given in Lemma~\ref{lemma:closed_form_t5}. We will now proceed with the case where $t_4 -1 \leq k < t_5-1$. Remember that for candidates at position $t_4 \leq k+1 < t_5$, the optimal strategy is to reject 1-candidates, accept 2-candidates when the current best is not reserved, and reserve 2-candidates when the current best is reserved. Please take a look at Figure~\ref{fig:opt} again for a visualization of the phases.

Throughout this section, we will only prove solutions for $E(k,0,0)$ and $E(k,1,0)$, that is for the states where the second-best candidate so far is not reserved. The two other states where the second-best so far is reserved easily follow using Lemma~\ref{lemma:g_k_drag_out}:
\[ E(k, 0, 1) = E(k,0,0) + g_k \text{ and } E(k,1,1) = E(k,1,0) + g_k. \]

The strategy of rejecting 1-candidates, accepting 2-candidates when the current best is not reserved, and reserving when the current best is reserved, translates to the following set of conditions for the expected gain:

If the maximum expected gain is obtained by rejecting 1-candidates, we have for $b \in \{0,1\}$ that
\begin{flalign}
    \max \left ( E(k,0,b), f_k, E(k,1,b)-c \right ) & = E(k,0,b).
\end{flalign}
If the maximum expected gain is achieved by accepting 2-candidates when the current best is not reserved, then we have
\begin{flalign}
    \max \left ( E(k,0,0), f_k, E(k,0,1)-c \right ) & = f_k.
\end{flalign}
If the maximum expected gain is achieved by accepting 2-candidates when the current best is reserved, then we have
\begin{flalign}
    \max \left ( E(k,1,0), f_k, E(k,1,1)-c \right ) & = E(k,1,1)-c.
\end{flalign}

We can now simplify the recursive definition of the expected gain for the case that $t_4 - 1 \leq k < t_5 - 1$ with the above equations as follows. For the state where neither the current best nor the current second best is reserved, we get that
\begin{equation}
    E(k,0,0) = \frac{k}{k+1} E(k+1,0,0) + \frac{1}{k+1} f_{k+1}.
    \label{eq:t4_E00}
\end{equation}
For the case where the current best is reserved, we get that
\begin{flalign}
    E(k,1,0) & = \frac{k-1}{k+1} E(k+1,1,0) + \frac{1}{k+1} (E(k,1,1) - c) \\
             & = \frac{k}{k+1} E(k+1,1,0) + \frac{1}{k+1} (g_{k+1} - c).
\end{flalign}
The second equality follows using Lemma~\ref{lemma:g_k_drag_out}.
In both cases, we can get rid of the recursion by the following technique, which we will demonstrate for $E(k,0,0)$. 

Let $F(k) := E(k,0,0)/k$. With (\ref{eq:t4_E00}), we get that for $t_4 - 1 \leq k < t_5-1$ we have
\begin{flalign}
    F(k) = \frac{E(k,0,0)}{k} = \frac{E(k+1,0,0)}{k+1} + \frac{f_{k+1}}{k(k+1)} = F(k+1) + \frac{f_{k+1}}{k(k+1)}.
\end{flalign}
Since we already know $E(t_5-1,0,0)$ and thus $F(t_5-1,0,0)$, we can rewrite the above as
\begin{flalign}
    F(k) = F(t_5-1) + \sum_{i = k}^{t_5-2}  \frac{f_{k+1}}{k(k+1)}.
\end{flalign}
We can now easily get back $E(k,0,0) = k F(k)$. An analog step can be made for $E(k,1,0)$. After some rearrangements and rewriting in terms of harmonic numbers, we get the following lemma.

\begin{lemma}
    \label{lemma:closed_form_t4}
    For $t_4 -1 \leq k  \leq t_5-1 =: t'$ we have:
    \begin{itemize}
        \item[(i)] $E(k, 0, 0) = f_k$
        \item[(ii)] $E(k, 0, 1) = f_k + g_k$
        \item[(iii)] $E(k, 1, 0) = \frac{k}{t'}E(t', 1, 0) + \frac{k}{n}(H_{t'-1} - H_{k-1}) - c\frac{t'-k}{t'} + \frac{k(t'-k)}{n(n-1)}$
        \item[(iv)] $E(k, 1, 1) = E(k,1,0) + g_k$
    \end{itemize}
\end{lemma}

We can use the same technique to obtain a non-recursive solution for the expected gain for $t_3-1 \leq k < t_4-1$. To demonstrate that these results can be verified through backward induction, we will provide a proof here.

\begin{lemma}
    For $t_3 -1\leq k  \leq t_4-1 =: t'$ we have:

    \begin{itemize}
        \item[(i)] $E(k, 0, 0) = f_{t'} + \frac{g_{t'} - g_k}{2} - c(H_{t'}-H_{k})$
        \item[(ii)] $\begin{aligned}E(k, 0, 1) = E(k,0,0)+g_k\end{aligned}$
        \item[(iii)] $E(k, 1, 0) = k\frac{E(t',1,0)+c}{t'}+\frac{(2n+3k-t_4)(t'-k)}{2n(n-1)} -c - ck \sum\limits_{i=k}^{t'-1}\frac{H_{t'} - H_{i+1}}{i(i+1)}$
        \item[(iv)] $E(k, 1, 1) = E(k,1,0) + g_k$
    \end{itemize}
    \label{lemma:closed_form_t3}
\end{lemma}

\begin{proof}
    Note that for $t_3 \leq k < t_4$, rejecting 1-candidates is optimal and reserving 2-candidates is optimal.
    The cases (ii) and (iv) follow using Lemma \ref{lemma:g_k_drag_out}.

    Proof by backward induction. The base case $k = t_4 - 1$ follows from Lemma~\ref{lemma:closed_form_t4}. So let the lemma be true for some $t_3-1 \leq k+1 < t_4-1$.
    \begin{itemize}
        \item[(i)] $\begin{aligned}[t]
                    E(k,0,0) & = \frac{k-1}{k+1} E(k+1,0,0) + \frac{E(k+1,0,0)}{k+1} + \frac{E(k+1,0,1)-c}{k+1}       \\
                             & = E(k+1,0,0) + \frac{g_{k+1} - c}{k+1}
                    = E(t_4-1,0,0) + \frac{(t_4-1)(t_4-2)}{2n(n-1)}                                                   \\& - \frac{g_{k+1}}{2} - c(H_{t_4-1}-H_{k+1})  + \frac{g_{k+1} - c}{k+1} \\
                             & = E(t_4-1,0,0) + \frac{(t_4-1)(t_4-2)}{2n(n-1)} - \frac{g_{k}}{2} - c(H_{t_4-1}-H_{k})
                \end{aligned}$

            The first equality is by the recursive definition of the expected gain and the fact that we know the optimal action between $t_3$ and $t_4$. The second equality follows from Lemma \ref{lemma:g_k_drag_out}. The rest follow using the induction hypothesis and simple rearrangements.
        \item[(iii)] $\begin{aligned}[t]
                    E(k,1,0) & = \frac{k-1}{k+1} E(k+1,1,0) + \frac{E(k+1,0,1)}{k+1} + \frac{E(k+1,1,1)-c}{k+1}
                    \\
                             & = \frac{k}{k+1} E(k+1,1,0) + \frac{E(k+1,0,0)+g_{k+1}-c}{k+1}                    \\
                             & = k\frac{E(t_4-1,1,0)+c}{t_4-1}+\frac{(2n+3k-t_4)(t_4-1-k)}{2n(n-1)}-c           \\
                             & - ck \sum\limits_{i=k}^{t_4-2}\frac{H_{t_4-1} - H_{i+1}}{i(i+1)}
                \end{aligned}$

            The first equality is by the recursive definition of the expected gain. The second equality follows from Lemma \ref{lemma:g_k_drag_out}. The last equality makes use of the induction hypothesis and the fact that
            \begin{flalign*}
                \frac{(2n+3k-t_4)(t_4-1-k)}{2n(n-1)} = & \frac{k}{k+1}\frac{(2n+3(k+1)-t_4)(t_4-1-(k+1))}{2n(n-1)} \\
                                                       & + \frac{2f_{t_4-1} + g_{t_4-1} + 3 g_{k+1}}{2(k+1)}.
            \end{flalign*}
    \end{itemize}
    This completes the proof.
\end{proof}

We continue solving the expected gain recursion for earlier phases.

\begin{lemma}
    For $t_2 - 1 \leq k  \leq t_3-1 =: t'$ we have:

    \begin{itemize}
        \item[(i)] $E(k, 0, 0) = k \left ( \frac{E(t',0,0)}{t'} + \frac{t'-k}{t'k} (E(t', 1, 0) - 2c + \frac{t'(t'-1)}{n(n-1)}) -2c \sum\limits_{i=k}^{t'-1}\frac{H_{t'} - H_{i+1}}{i(i+1)} \right )$
        \item[(ii)] $E(k, 0, 1) = E(k, 0, 0) + g_k$
        \item[(iii)] $E(k, 1, 0) = E(t',1,0) - 2c(H_{t'} - H_{k}) - g_k + \frac{t'(t'-1)}{n(n-1)}$
        \item[(iv)] $E(k, 1, 1) = E(k, 1, 0) + g_k$
    \end{itemize}
    \label{lemma:closed_form_t2}
\end{lemma}
\begin{proof}
    
For $t_2 \leq k < t_3$, we are in the phase where both 1- and 2-candidates are reserved regardless of the current state. Thus, once we have reserved a 1-candidate, we will have one reserved for the whole phase.
The expected gain $E(k,1,0)$, and using Lemma~\ref{lemma:g_k_drag_out} also $E(k,1,1) = E(k,1,0) + g_k$, become easy to analyze:
\begin{equation}
E(k, 1, 0) = \frac{k-1}{k+1} E(k+1,1,0) + \frac{2}{k+1} (E(k+,1,1)-c) = E(k,1,0) + \frac{2(g_{k+1}-c)}{k+1}.
\end{equation}

Since we already know $E(t_3-1,1,0)$, we can get rid of the direct recursion and rewrite $E(k,0,0)$ for $t_2 - 1 \leq k < t_3 - 1$ as
\begin{equation}
    E(k, 1, 0) = E(t_3-1, 1, 0) + \sum_{i = k}^{t_3-1} \; \frac{2g_{k+1} - 2c}{k+1}.
\end{equation}
After some rearrangements and rewriting sums in terms of harmonic numbers, we obtain (iii) and using Lemma~\ref{lemma:g_k_drag_out} also (iv).

Now that we have the expected gain in the states $(1,0)$ and $(1,1)$ we can also solve the recursion for the states $(0,0)$ and $(0,1)$:
\begin{equation}
    E(k,0,0) = \frac{k-1}{k+1} E(k+,0,0) + \frac{1}{k+1} (E(k+,0,1)-c) + \frac{1}{k+1} (E(k+,1,0)-c)
\end{equation}
We can again using Lemma~\ref{lemma:g_k_drag_out} rewrite $E(k,0,1)$ as $E(k,0,0)+g_k$ and get that
\begin{equation}
    E(k,0,0) = \frac{k}{k+1} E(k+1,0,0) + \frac{E(k+1,1,0)+g_{k+1}-2c}{k+1}.
\end{equation}
Using our trick from the proof of Lemma~\ref{lemma:closed_form_t4} we write the recursion in terms of $F(k) = E(k,0,0)/k$ and get that
\[ F(k) = F(k+1) + \frac{E(k+1,1,0)+g_{k+1}-2c}{k(k+1)}. \]
This is again a direct recursion. After solving it, we get back $E(k,0,0) = k F(k)$ and after some rearrangements, we get (i) and with Lemma~\ref{lemma:g_k_drag_out} also (ii).

This completes the proof.
\end{proof}

The last phase, where not all candidates are rejected, is between $t_1$ and $t_2$. The non-recursive forms of the expected gain are given in the following lemma.

\begin{lemma}
    For $t_1-1 \leq k \leq t_2-1 =: t'$ we get:

    \begin{itemize}
        \item[(i)] $E(k, 0, 0) = \frac{kE(t',0,0)}{t'} + \frac{t'-k}{t'}(E(t', 1, 0) - c + \frac{g_{t'}}{2})
                - \frac{k(t'-k)}{2n(n-1)}
                - ck \sum\limits_{i = k}^{t'-1}\frac{H_{t'} - H_{i+1}}{i(i+1)}$
        \item[(ii)] $E(k, 0, 1) = E(k, 0, 0) + g_k$
        \item[(iii)] $E(k, 1, 0) = E(t', 1, 0) + \frac{t'(t'-1) - k(k-1)}{2n(n-1)} - c(H_{t'} - H_{k})$
        \item[(iv)] $E(k, 1, 1) = E(k, 1, 0) + g_k$
    \end{itemize}
    \label{lemma:closed_form_t1}
\end{lemma}
\begin{proof}
    Remember that we reject all 2-candidates and reserve all 1-candidates in this phase.
    Let us start with $E(k,1,0)$. Since we reserve all 1-candidates and reject all 2-candidates we will not leave the state (1,0) until the end of the phase. With this knowledge, the recursive definition of the expected gain simplifies to 
    \[E(k,1,0) = \frac{k}{k+1} E(k+1,1,0) + \frac{1}{k+1} (E(k+1,1,1) - c).\]
    Using Lemma~\ref{lemma:g_k_drag_out}, that is $E(k,1,1) = E(k,1,0) + g_k$, we obtain 
    \[E(k,1,0) = E(k+1,1,0) + \frac{g_{k+1} - c}{k+1}.\]
    This recursion is easy to get rid of. Since we already know $E(t_2-1,1,0)$ and the above holds for all $t_1 -1 \leq k < t_2-1$ we get
    \[E(k,1,0) = E(t_2-1,1,0) + \sum_{i=k}^{t_2-2}\frac{g_{k+1} -c}{k+1}. \]
    Now (iii) follows using simple rearrangements. So let us continue with $E(k,0,0)$. Using our knowledge that rejecting 2-candidates and reserving 1-candidates is optimal in the current phase, the recursive definition of the expected gain simplifies to
    \[ E(k,0,0) = \frac{k}{k+1} E(k+1, 0, 0) + \frac{1}{k+1} E(k+1,1,0). \]
    We solved this kind of recursive equation in the previous lemmata. Let $F(k) = E(k,0,0) / k$. Then we get
    \[ F(k) = F(k+1) + \frac{E(k+1,1,0)}{k(k+1)}. \]
    From the previous lemma we know $E(t_2-1,0,0)$ and we get
    \[ F(k) = F(t_2-1) + \sum_{i = k}^{t_2-2} \frac{E(i+1,1,0)}{i(i+1)}. \]
    We then get back $E(k,0,0)$ using $E(k,0,0) = kF(k)$, resulting in
    \[ E(k,0,0) = k\frac{E(t_2-1,0,0)}{t_2-2} + k \sum_{i = k}^{t_2-2} \frac{E(i+1,1,0)}{i(i+1)}\]
    Using (iii) and simple rearrangements, we obtain (i). (ii) and (iii) again follow using the Lemma~\ref{lemma:g_k_drag_out}.
    This completes the proof.
\end{proof}

We skip the last phase where all candidates are rejected. It is easy to see that $E(k,0,0) = E(k+1,0,0)$ for that phase since all candidates are rejected. Hence, the expected gain of the optimal strategy is $E(0,0,0) = E(t_1-1,0,0)$.