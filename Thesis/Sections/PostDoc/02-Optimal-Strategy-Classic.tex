\section{The Classical Case}

We now start to analyze the structure of the optimal strategy for different reservation costs $c$. For $c \leq 0$, the optimal strategy is trivial as one would just reserve all candidates and would have an expected gain of at least $1$. For $c \geq 1$, it is easy to see that the optimal strategy will not reserve since one reservation would make the gain negative and the classic strategy would have been better. 

We now show that for all reservation costs $c \geq f_{k_0}$, which for large $n$ converges to $1/4$, the classic strategy of accepting the first 2-candidate in the second half is optimal. From now on $k_0 = \ceil*{n/2}$. We show that for those reservation costs, there is an optimal strategy that does not reserve. Thus, the classic strategy must be optimal.

\begin{theorem}
    \label{thm:classic_main}
    For $c \geq f_{k_0}$, it is always either optimal to reject or accept a candidate.
\end{theorem}

\begin{proof}
    We show this theorem in three steps. Each step has its own lemma:
    \begin{samepage}
        \begin{itemize}
            \item[(i)] Lemma \ref{lemma:opt_classic_1_candidate_ignore}: Rejecting $1$-candidates is better than reserving.
            \item[(ii)] Lemma \ref{lemma:opt_classic_k_geq_k0_2_candidates}: Accepting $2$-candidates at $k_0 < k \leq n$ is better than reserving.
            \item[(iii)] Lemma \ref{lemma:classic_reject_2_candidates}: Rejecting $2$-candidates at $1 \leq k \leq k_0$ is better than reserving.
        \end{itemize}
    \end{samepage}

    Using all three lemmata, it follows that always either accepting or rejecting is better than reserving. This completes the proof. 
\end{proof}

Let us start with a rather obvious lemma. The expected gain advantage of having reserved the current best candidate is, at max, the probability that this candidate will be the second-best after all interviews. We will provide a proof using backward induction to demonstrate how lemmata like this can be easily proven.

\begin{lemma}
    \label{lemma:E10_leq_E00_minus_fk}
    For $1 \leq k \leq n$ we have that $E(k,1,0) \leq E(k,0,0) + f_k$.
\end{lemma}
\begin{proof}
    Proof by backward induction. The base case $k = n$ is trivial. So let the lemma be true for some $1 < k + 1 \leq n$.
    By the recursive definition of $E$ and the induction hypothesis, we get that
    \begin{flalign*}
        E(k,1,0) \leq \;\; &\frac{k-1}{k+1} (E(k+1,0,0) + f_{k+1})\\
        & + \frac{1}{k+1} \max(E(k+1,0,0)+g_{k+1}, f_{k+1}, E(k+1,1,0)+g_{k+1}-c) \\
        & + \frac{1}{k+1} \max(E(k+1,0,0) + f_{k+1}, g_{k+1}, E(k+1,0,1)+f_{k+1}-c).
    \end{flalign*}
    We can now drag $g_{k+1}$ out of the first maximum and $f_{k+1}$ out of the second maximum and using 
    \[\frac{k}{k+1} f_{k+1} + \frac{1}{k+1}g_{k+1} =  f_k\] 
    we finally get that by the recursive definition of $E$ we have \(E(k,1,0) \leq E(k,0,0) + f_k\).
    This completes the proof.
\end{proof}

Equipped with the above lemma, we can show that, for the reservation costs we are looking at, rejecting 1-candidates is always at least as good as reserving them. By "at least as good" we mean that the expected gain after rejecting is at least as high as after reserving. Thus, the optimal strategy does not have to reserve this candidate since it would also be optimal to reject it. 

\begin{lemma}
    \label{lemma:opt_classic_1_candidate_ignore}
    For $c \geq f_{k_0}$, rejecting $1$-candidates is always at least as good as reserving.
\end{lemma}
\begin{proof}
    Let $1\leq k \leq n$ be arbitrary. By Lemma~\ref{lemma:E10_leq_E00_minus_fk} we know that $E(k,1,0) \leq E(k,0,0) + f_k$. Using Lemma \ref{lemma:g_k_drag_out} we have that $E(k,b,1) = E(k,b,0) + g_k$ for $b \in \{0,1\}$. Furthermore, it is easy to see (Figure~\ref{fig:f_and_g}) and verify that $f_k \leq f_{k_0}$ and hence $f_k - c \leq 0$. Thus, for $b \in \{0,1\}$ we get that
    \[E(k,0,b) = E(k,0,0) + bg_k \geq E(k,0,0) + f_k - c + bg_k \geq E(k,1,b)-c.\]
    We showed that the expected gain after rejecting a 1-candidate at $k$ is at least as high as after reserving it. This completes the proof.
\end{proof}

For showing the next two lemmata, we need a closed form of the expected gain of the optimal strategy $E(k,0,0)$ and $E(k,0,1)$ for $1 \leq k \leq n$. 
See Figure~\ref{fig:E00_E01_CLASSICAL} for a visualization of the expected gain for $n=1000$. 

\begin{figure}[t]
    \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{E00_E10_CLASSICAL.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{CLASSICAL_G_F_C.pdf}
    \end{minipage}

    \caption{Left: $E(k,0,0)$ and $E(k,0,1)$ for $n=1000$. Right: $f_k$ and $g_k$ for $n=1000$. The candidate position $k$ is on the $x$-axis and the corresponding gain/probability on the $y$-axis.}
    \label{fig:E00_E01_CLASSICAL}
\end{figure}

\begin{samepage}
    \begin{lemma}
        \label{lemma:classic_recursion_lemma}
        For $c \geq f_{k_0}$  we have:
        \begin{itemize}
            \item[(i)] $E(k, 0, 0) = \left \{ \begin{array}{ll}
                f_k & \textit{ for }  k_0 \leq k \leq n\\
                f_{k_0} & \textit{ for }  1 \leq k < k_0
            \end{array} \right .$
            \item[(ii)] $E(k, 0, 1) = \left \{ \begin{array}{ll}
                f_k + g_k & \textit{ for }  k \geq k_0 \\
                f_{k_0} + g_k & \textit{ for }  k < k_0
            \end{array} \right .$
        \end{itemize}
    \end{lemma}
\end{samepage}
\begin{proof}
    Proof by backward induction. The base case $k = n$ is trivial. So let the lemma be true for some $1 < k+1 \leq n$. We distinguish the following two cases.

    Case 1: $k \geq k_0$. By the induction hypothesis we then have $E(k+1,0,0)=f_{k+1}$ and $E(k+1,0,1)=f_{k+1} + g_{k+1}$. From Lemma \ref{lemma:opt_classic_1_candidate_ignore} we know that rejecting $1$-candidates is at least as good as reserving. $E(k+1, 0, 0) \geq E(k+1, 1, 0)-c$ and we get
    \begin{equation}
        \label{equation:classic_recursion_case_1_max1}
        \max \left (E(k+1, 0, 0),\, f_{k+1},\, E(k+1, 1, 0)-c\right ) = f_{k+1}. 
    \end{equation}
    For $k + 1 > k_0$ we have $g_{k+1} \geq f_{k+1}$ and get
    \begin{equation}
        \label{equation:classic_recursion_case_1_max2}
        \max \left(E(k+1, 0, 0),\, g_{k+1},\, E(k+1, 0, 1)-c\right) = g_{k+1}
    \end{equation}
    using $f_{k+1} \leq f_{k_0} \leq c$. Using (\ref{equation:classic_recursion_case_1_max1}), (\ref{equation:classic_recursion_case_1_max2}) and the recursive definition of the expected gain, we finally get
    \begin{equation}
        \label{equation:classic_recursion_case_1_E00}
        E(k,0,0) = \frac{k}{k+1} f_{k+1} + \frac{1}{k+1} g_{k+1} = f_k.
    \end{equation}
    $E(k,0,1) = E(k,0,0) + g_k = f_k + g_k$ follows using Lemma \ref{lemma:g_k_drag_out} and (\ref{equation:classic_recursion_case_1_E00}).
    This completes the first case.

    Case 2: $k < k_0$. By the induction hypothesis we have $E(k+1,0,0) = f_{k_0}$ and $E(k+1,0,1) = f_{k_0} + g_{k+1}$.
    We have $f_{k+1} \leq f_{k_0}$ and in this case also $g_{k+1} \leq f_{k_0} \leq c$. Also as in case 1 we have using Lemma \ref{lemma:opt_classic_1_candidate_ignore} $E(k+1, 0, 0) \geq E(k+1, 1, 0)-c$ and we get
    \begin{equation}
        \label{equation:classic_recursion_case_2_max1}
        \max \left (E(k+1, 0, 0),\, f_{k+1},\, E(k+1, 1, 0)-c\right ) = f_{k_0}
    \end{equation}
    \begin{equation}
        \label{equation:classic_recursion_case_2_max2}
        \max \left (E(k+1, 0, 0),\, g_{k+1},\, E(k+1, 0, 1)-c\right ) = f_{k_0}. 
    \end{equation}
    Using (\ref{equation:classic_recursion_case_2_max1}) and (\ref{equation:classic_recursion_case_2_max2}) and the recursive definition of the expected gain, we get
    \[ E(k,0,0) = \frac{k-1}{k+1} f_{k_0} + \frac{1}{k+1} f_{k_0} + + \frac{1}{k+1} f_{k_0} = f_{k_0}.\]
    Again $E(k,0,1) = E(k,0,0) + g_k = f_{k_0} + g_k$ follows using Lemma \ref{lemma:g_k_drag_out}.
    This completes the second case and the proof.
\end{proof}

Equipped with the closed form for $E(k,0,0)$ and $E(k,0,1)$, we can derive the following two lemmata.

\begin{lemma}
    \label{lemma:opt_classic_k_geq_k0_2_candidates}
    For $c \geq f_{k_0}$ and $k_0 < k \leq n$, accepting 2-candidates at position $k$ is at least as good as reserving.
\end{lemma}
\begin{proof}
    Using the closed-form solutions from Lemma~\ref{lemma:classic_recursion_lemma} and $f_{k} \leq f_{k_0} \leq c$ we get 
    \[E(k,0,1) - c = f_{k} + g_{k} - c \leq g_k\]
    and thus accepting 2-candidates is better than reserving. This completes the proof.
\end{proof}

\begin{lemma}
    \label{lemma:classic_reject_2_candidates}
    For  $c \geq f_{k_0}$ and $1 \leq k \leq k_0$, rejecting 2-candidates at position $k$ is at least as good as reserving.
\end{lemma}
\begin{proof}
    Again using the closed form solution from Lemma~\ref{lemma:classic_recursion_lemma} and $g_k \leq f_{k} \leq f_{k_0}$ for $k \leq k_0$ we get
    \[E(k,0,0) = f_{k_0} \geq g_k \geq g_k + f_{k_0} - c = E(k,0,1) - c\]
    and thus rejecting 2-candidates is at least as good as reserving. This completes the proof.
\end{proof}

We have now completed the proof of Theorem~\ref{thm:classic_main}. We now know that there is an optimal strategy that does not reserve candidates for reservation costs $c\geq f_{k_0}$. It follows that the classic strategy of accepting the first 2-candidate in the second half of candidates must also be optimal and that the expected gain is thus $f_{k_0}$. It is easy to see that $f_{k_0} = (n-k_0)/(n(n-1))$ converges to $1/4$. Here are some values for $f_{k_0}$ for small $n$.

\vspace{.3cm}
\begin{tabularx}{\textwidth}{|c||Y|Y|Y|Y|Y|Y|Y|Y|Y|Y|Y|}
    \hline
    $n$ & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 100 & 1000\\
    \hline
    $f_k$ & 0.500 & 0.333 & 0.333 & 0.300 & 0.300 & 0.286 & 0.286 & 0.278 & 0.278 & 0.253 & 0.250\\
    \hline
\end{tabularx}