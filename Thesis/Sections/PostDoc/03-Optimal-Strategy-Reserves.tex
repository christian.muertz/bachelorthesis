\section{Structure of the Optimal Strategy}

The next question that naturally arises is what the optimal strategy looks like for reservation costs $c$ smaller than $f_{k_0}$.
We now show that, in this case, the optimal strategy operates in six phases and is structured as shown in Figure \ref{fig:opt}.
The six phases are defined by the five thresholds $t_1,...,t_5$, with $1 \leq t_1 \leq ... \leq t_5 \leq n$, that depend on the reservation cost $c$ and the number of candidates $n$. For simplicity, we will assume that $c > 0$. For $c=0$ the optimal strategy is trivial since reserving all candidates is optimal.

\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{OPTIMAL_STRATEGY_STRUCTURE.pdf}
    \vspace{-.8cm}
    \caption{Structure of the optimal strategy for $c < f_{k_0}$. Depending on whether the current candidate is a 1- or a 2-candidate and whether the best candidate before the current is reserved or not, the optimal decision can be looked up in dependence on the current candidate's position.}
    \label{fig:opt}
\end{figure}

In the process of showing this structure, we will be able to give the thresholds $t_2$ and $t_4$ in a closed form and the threshold $t_5$ as the last position where a certain probability is larger than $c$. For the thresholds $t_1$ and $t_3$, we can only prove existence. We discuss the reasons for this at the end of this section and then show in the following sections how we can nonetheless calculate the limits of these thresholds as the number of candidates approaches infinity. 

Before we start with the optimal actions for 2-candidates, let us prove a simple lower bound for the expected gain.

\begin{lemma}
    \label{lemma:E_lower_bound}
    For $1 \leq k \leq n$ and $b,s\in \{0,1\}$ we have $E(k,b,s) \geq f_k$.
\end{lemma}
\begin{proof}
    Let $1 \leq k \leq n$ and $b,s \in \{0,1\}$. The probability that there is a next 2-candidate after $k$ and that this candidate is the winning candidate is $f_k$ (Theorem~\ref{thm:f_k}). Thus, the expected gain $E(k,b,s)$ must be at least as high. 
\end{proof}

\subsection{Rejecting 2-candidates}

We begin with the candidate positions where rejecting 2-candidates is optimal. These are all positions before $t_2$. $t_2$ is the first position where reserving 2-candidates is optimal.

\begin{theorem}
    \label{thm:reject_opt}
    It is optimal to reject 2-candidates at \[1 \leq k < t_2 = \left \lceil \frac{1}{2} + \sqrt{\frac{1}{4} + cn(n-1)} \right \rceil. \]
\end{theorem}
\begin{proof}
    Let $1 \leq k < t_2$. Note that $t_2$ is the second root of $p(x) = g_x - c$, ceiled. It is easy to verify that $g_k \leq f_{k}$ iff $g_k \leq f_{k_0}$ (see Figure~\ref{fig:E00_E01_CLASSICAL}). Since $g_k \leq c$ and $c < f_{k_0}$, we have  $g_k \leq f_k$. 

    To prove the theorem, we first show that $E(k,b,0) \geq E(k,b,1)-c$ for $b \in \{0,1\}$ and thus rejecting 2-candidates at $k$ is at least as good as reserving them regardless of whether the current best candidate is reserved or not. So let $b \in \{0,1\}$ and using $g_k \leq c$ we get that
    \[E(k, b, 0) \geq E(k,b,0) + g_k - c = E(k,b,1) - c.\]

    Next, we show that $E(k,b,0) \geq g_k$ for $b \in \{0,1\}$ and thus rejecting 2-candidates at $k$ is at least as good as accepting them regardless of whether the current best candidate is reserved or not. Using the obvious fact that $E(k,1,0) \geq E(k,0,0)$ and $E(k,0,0) \geq f_k$ (Lemma \ref{lemma:E_lower_bound}) we get
    \[ E(k,b,0) \geq E(k,0,0) \geq f_k \geq g_k. \]

    It follows that rejecting 2-candidates at $k$ is optimal and the proof is complete.
\end{proof}

We have just shown that as long as the probability of winning with a 2-candidate is under the reservation cost, rejecting this candidate is optimal. In the next two subsections, we will show that once the probability of winning with a 2-candidate gets above the reservation cost $c$, the optimal strategy will either reserve that candidate or accept it. 

\subsection{Reserving 2-candidates}

The positions where reserving 2-candidates is optimal depend on whether the current best candidate is reserved or not. We start with a lemma that applies to both cases.

\begin{lemma}
    \label{lemma:res_at_least_as_good_as_rejecting}
    Reserving 2-candidates at $t_2 \leq k \leq n$ is at least as good as rejecting.
\end{lemma}
\begin{proof}
    Let $t_2 \leq k \leq n$. First note that $g_k \geq c$ (see proof of Theorem~\ref{thm:reject_opt}).
    For $b \in \{0,1\}$ we have by using Lemma~\ref{lemma:g_k_drag_out} that
    \[ E(k,b,1) - c = E(k,b,0) + g_k - c \geq E(k,b,0). \]
    Thus reserving 2-candidates is at least as good as rejecting them. This completes the proof.
\end{proof}

For the following two theorems, we now just have to show that reserving 2-candidates is at least as good as accepting. 

\begin{theorem}
    \label{thm:reserving_2_opt}
    Reserving $2$-candidates at \[t_2 \leq k < t_4 = \left \lceil \frac{n}{2} + \sqrt{\frac{n^2}{4} - cn(n-1)} \right \rceil\] is optimal.
\end{theorem}

\begin{proof}
    Let $t_2 \leq k < t_4$. First, note that $t_4$ is the second root of $p(x) = f_k - c$ and the first root of $p(x)$ is before $t_2$. Thus $f_k \geq c$ (see Figure~\ref{fig:E00_E01_CLASSICAL}). Using $E(k,b,1) = E(k,b,0) + g_k$ (Lemma~\ref{lemma:g_k_drag_out}) and $E(k,b,0) \geq f_k$ (Lemma~\ref{lemma:E_lower_bound}), we get for all $b \in \{0,1\}$ that
    \[ E(k,b,1) - c = E(k,b,0) + g_k - c \geq g_k + f_k - c \geq g_k. \]
    Thus reserving 2-candidates at $k$ is better than accepting, and we already know it is better than rejecting (Lemma~\ref{lemma:res_at_least_as_good_as_rejecting}). Thus reserving is optimal and the proof is complete.
\end{proof}

Note here that since $t_2$ is the second root of $p_2(x) = g_x-c$ ceiled and $t_4$ is the second root of $p_4(x) = f_x-c$ ceiled, we indeed have that $t_2 \leq t_4$. Let us continue with the case where we have the current best candidate reserved.

\begin{lemma}
    \label{lemma:h_k}
    The probability that the next 1- or 2-candidate after position $1 \leq k \leq n$ is a 1-candidate and the current best candidate is the winning candidate is
    \[h_k = g_k (H_{n-2} - H_{k-2}).\]
\end{lemma}

\begin{proof}
    It is easy to see that the following recursion defines our probability:

    \[ h_n = 0 \text{ and } h_k = \frac{k-1}{k+1} h_{k+1} + \frac{1}{k+1} g_{k+1}\]

    We now show via backward induction that $h_k = g_k (H_{n-2} - H_{k-2})$ is the closed-form solution.
    The base case $k=n$ is trivial. So let the statement be true for some $1 < k+1 \leq n$. Then we have that
    \[ h_k = \frac{k-1}{k+1} h_{k+1} + \frac{g_{k+1}}{k+1}  = \frac{k-1}{k+1} g_{k+1} (H_{n-2} - H_{k-1}) + \frac{g_{k+1}}{k+1} = g_k (H_{n-2} - H_{k-2}). \]
    
    This completes the proof.
\end{proof}

Note here that the two events "there is a next 2-candidate" and "the next 1- or 2-candidate is a 1-candidate and the current best is the winning candidate" are disjunct and thus $f_k + h_k$ is the chance of winning through accepting the next 2-candidate after $k$ if there is one or waiting until the end of the interview and accepting the previously reserved best candidate before or at $k$. 

\begin{figure}
    \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{H_K.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{H_F_K.pdf}
    \end{minipage}

    \caption{Left: $h_k$ for $n=1000$, Right: $f_k + h_k$ for $n=1000$. The candidate position $k$ is on the $x$-axis and the corresponding probability on the $y$-axis.}
    \label{fig:h_k}
\end{figure}


\begin{theorem}
    Reserving $2$-candidates at \[t_2 \leq k < t_5 = \max \{k \in \mathbb{N} \mid h_k + f_k > c\} + 1 \] is optimal when the current best candidate is reserved.
\end{theorem}

\begin{proof}
    For $t_2 \leq k < t_4$ the theorem has been proven above (Theorem~\ref{thm:reserving_2_opt}). So let $t_4 \leq k < t_5$. First note that for $t_4 \leq k < t_5$ we have $h_k + f_k \geq c$ (see Figure~\ref{fig:h_k}). Since $f_k + h_k$ is the probability of winning by accepting the next 2-candidate if there is one or by winning through the current reserved best candidate if there is no next 2-candidate, we must have $E(k,1,1) \geq h_k + f_k$. Again using Lemma~\ref{lemma:g_k_drag_out} we have $E(k,1,1) = E(k,1,0) + g_k$ and we get that
    \[E(k,1,1) - c \geq h_k + f_k + g_k - c \geq g_k.\]

    Thus reserving 2-candidates at $k$ is better than accepting, and we already know it is better than rejecting (Lemma~\ref{lemma:res_at_least_as_good_as_rejecting}). Thus reserving is optimal, and the proof is complete.
\end{proof}

Note here that $t_4 \leq t_5$. Since $f_{k_0} > c$ and $t_4$ is the second root of $p(x) = f_x -c$ ceiled we have that $f_{t_4-1} > c$. Thus, also $f_{t_4-1} + h_{t_4-1} > c$ and we have $t_5 \geq t_4$.

\subsection{Accepting 2-candidates}

The last missing phase for 2-candidates is when to accept them. In the previous two sections, we have shown the optimal action for candidates before $t_4$ for the case when the current best candidate is not reserved and for candidates before $t_5$ when the current best candidate is reserved. We now show that for all other positions accepting 2-candidates is optimal. For the first case where the current best is not reserved, we need the following lemma about 1-candidates.

\begin{lemma}
    It is optimal to reject 1-candidates at $t_4 \leq k \leq n$.
    \label{lemma:ignore_1_from_t4}
\end{lemma}
\begin{proof}
    First note that for $t_4 \leq k \leq n$ we have $f_k \leq c$, since $t_4$ is the second root of $p(x) = f_x - c$. Let $t_4 \leq k \leq n$. We have already shown that rejecting 1-candidates is always as least as good as accepting in Lemma~\ref{lemma:E_lower_bound}. What is left is to show that rejecting 1-candidates at $k$ is also at least as good as reserving. We have shown in Lemma~\ref{lemma:E10_leq_E00_minus_fk} that having the current best candidate reserved gives an advantage of at max $f_k$. Since $f_k \leq c$ we get for $b \in \{0,1\}$ that
    \[ E(k,0,b) \geq E(k,0,b) + f_k - c \geq  E(k,1,b) - c. \]
    This completes the proof.
\end{proof}

\begin{theorem}
    Accepting 2-candidates at $t_4 \leq k \leq n$ is optimal when the current best candidate is not reserved.
\end{theorem}

\begin{proof}
    We already know from Lemma \ref{lemma:ignore_1_from_t4} that rejecting 1-candidates at $k \geq t_4$ is optimal.
    Also, note that $f_k \leq c$ since $t_4$ is the second root of $p(x) = f_x-c$ ceiled.

    We prove the lemma by backward induction. The base case $k = n$ is trivial. It is obviously optimal to accept 2-candidates at the last position because this leads to a guaranteed win.
    
    So let the lemma be true for some $t_4 < k+1 \leq n$. Since we do not have the current best candidate reserved and do not reserve nor accept 1-candidates in the future, the only reserve or accept action we will potentially perform is on 2-candidates. From the induction hypothesis, we know that we will accept the next 2-candidate if we do not accept the current. We know from Theorem~\ref*{thm:f_k} that the probability of winning with the next 2-candidate is $f_k$. Thus, the expected gain of winning with reserving is $f_k + g_k - c$, with accepting is $g_k$ and with rejecting is $f_k$. Using $f_k \leq c$ and $g_k \geq f_k$ we conclude that accepting is optimal. This completes the proof.
\end{proof}

We now prove the closed-form solution for the expected gain for positions $ t_5-1 \leq k \leq n$. After that showing when to accept 2-candidates when the current best candidate is reserved is rather easy. 

\begin{lemma}
    For $k  \geq t_5-1$, we have:
    \begin{itemize}
        \item[(i)] $E(k, 0, 0) = f_k$
        \item[(ii)] $E(k, 0, 1) = f_k +  g_k$
        \item[(iii)] $E(k, 1, 0) = f_k + h_k$
        \item[(iv)] $E(k, 1, 1) = f_k + h_k + g_k$
    \end{itemize}
    \label{lemma:closed_form_t5}
\end{lemma}

\begin{proof}
    Prove by backward induction over the position of the current candidate $k$. The base case $k = n$ is trivial because the expected gain is $1$ iff the second best candidate is reserved and $0$ otherwise. So let the lemma be true for some $t_5 \leq k + 1 \leq n$.

    From Lemma \ref{lemma:ignore_1_from_t4} we know that rejecting $1$-candidates is optimal starting at $t_4$ and thus
    \[\max(E(k+1,0,b), \,f_{k+1},\, E(k+1,1,b)-c) = E(k+1,0,b) \text{ for } b \in \{0,1\}.\]

    % TODO not clear why that follows from induction hypothesis
    From the induction hypothesis, we know that accepting 2-candidates at $k+1$ is optimal and thus
    \[\max \left ( E(k+1,b,0),\, g_{k+1}, \,E(k+1,b,1) - c \right ) = g_{k+1} \text{ for } b \in \{0,1\}.\]

    Using the recursive definition of the expected gain and the above, we get:
    \begin{itemize}
        \item[(i)]
            $\begin{aligned}[t]
                    E(k, 0, 0) & = \frac{k-1}{k+1} E(k+1, 0, 0) + \frac{1}{k+1} E(k+1,0,0) + \frac{1}{k+1} g_{k+1}   \\
                               & = \frac{k}{k+1} f_{k+1} + \frac{1}{k+1} g_{k+1} = f_k
                \end{aligned}$
        \item[(iii)]
            $\begin{aligned}[t]
                    E(k, 1, 0) & = \frac{k-1}{k+1} E(k+1, 1, 0) + \frac{1}{k+1} E(k+1,0,1) + \frac{1}{k+1} g_{k+1}              \\
                               & = \frac{k}{k+1} (f_{k+1} + h_{k+1}) + \frac{1}{k+1}(f_{k+1} + g_{k+1}) + \frac{1}{k+1} g_{k+1} = f_k + h_k
                \end{aligned}$
    \end{itemize}

    The cases (ii) and (iv) now follow using $E(k,b,1) = E(k,b,0)+g_k$ (Lemma \ref{lemma:g_k_drag_out}). This completes the proof.
\end{proof}

Now deriving when to accept 2-candidate in the case that the current best candidate is reserved is easy.

\begin{theorem}
    Accepting 2-candidates at $t_5 \leq k \leq n$ is optimal when the current best candidate is reserved.
\end{theorem}

\begin{proof}
    This follows directly from Lemma \ref{lemma:closed_form_t5}, since for $k \geq t_5$ we have $h_k + f_k \leq c$ and $g_k \geq c$ and thus
    \[E(k,1,1) - c= f_k + g_k + h_k - c \leq g_k \text{ and } E(k,1,0) = f_k + h_k \leq c \leq g_k.\]
    This completes the proof.
\end{proof}

\subsection{Ignoring/Reserving 1-candidates}

We have proved the optimal action for all 2-candidates. What is left are the optimal actions for 1-candidates. We start with a rather simple lemma that accepting 1-candidates is never the only optimal action. Rejecting 1-candidates is always at least as good as accepting. This basically comes down to the fact that the probability of winning with the current 1-candidate is the same as winning with the next 2-candidate if that exists. Thus waiting for the next 2-candidate is always at least as good as accepting and ending the interview process.
\begin{lemma}
    Rejecting 1-candidates is always at least as good as accepting them.
    \label{lemma:1_ignore_better_accept}
\end{lemma}
\begin{proof}
    Let $1 \leq k \leq n$. Lemma \ref{lemma:E_lower_bound} states that for $b,s \in \{0,1\}$ we have $E(k, b, s) \geq f_k$ and thus for $b \in \{0,1\}$ we have
    \[ E(k,0,b) \geq f_k. \]
    This means that rejecting 1-candidates is at least as good as accepting them. This completes the proof.
\end{proof}

We now show that there exist two thresholds $t_1$ and $t_3$ with $t_1 \leq t_2 \leq t_3 \leq t_4$, such that reserving $1$-candidates for $t_1 \leq k < t_3$ is optimal and rejecting for $k<t_1$ and $k \geq t_3$ is optimal. We will be able to prove the existence of these thresholds, but we will not be able to give them explicitly. In the next section, we investigate the limit of these two thresholds as the number of candidates approaches infinity.

\begin{lemma}
    If reserving 1-candidates at position $t_2 \leq k < t_4$ is optimal, then reserving at $k-1$ is also optimal.
    \label{lemma:reserve_before}
\end{lemma}

\begin{proof}
    Let $t_2 \leq k < t_4$. Assume reserving 1-candidates at $k$ is optimal. For such $k$, we know that reserving 2-candidates is optimal (Lemma~\ref{thm:reserving_2_opt}). The fact that reserving 1-candidates is optimal implies that we have
    \[ E(k, 1, 0) - c \geq E(k,0,0). \]
    We now show that we then also have $E(k-1,1,0) - c \geq E(k-1,0,0)$.
    \begin{flalign*}
        E(k-1,1,0) - c & = \frac{k-2}{k} E(k,1,0) + \frac{1}{k} (E(k,1,1)-c) + \frac{1}{k} (E(k, 1,1) - c) - c \\
                       & = \frac{k-1}{k} (E(k,1,0) - c) + \frac{1}{k} (E(k,1,0) - c) + \frac{2g_k-2c}{k}       \\
                       & \geq \frac{k-1}{k} E(k,0,0) + \frac{1}{k} (E(k,1,0) - c) + \frac{g_k-c}{k}            \\
                       & = \frac{k-2}{k} E(k,0,0) + \frac{1}{k} (E(k,1,0)-c) + \frac{1}{k} (E(k,0,1) - c)      \\
                       & = E(k-1,0,0)
    \end{flalign*}
    The above inequality uses $E(k,1,0)-c\geq E(k,0,0)$ and $g_k - c \geq 0$. $E(k-1,1,1) -c \geq E(k-1,0,1)$ follows easily using Lemma~\ref{lemma:g_k_drag_out}. We now proved that reserving is at least as good as rejecting. Since we know that rejecting is at least as good as accepting, we know that reserving is optimal. This completes the proof.
\end{proof}

\begin{lemma}
    If rejecting $1$-candidates at position $1 < k < t_2$ is optimal, then rejecting them at $k-1$ is also optimal.
    \label{lemma:reject_before}
\end{lemma}
\begin{proof}
    For $1< k < t_2 \leq k_0$ we have $g_k \leq f_k$ and $g_k \leq c$. Assume rejecting $1$-candidates at $k$ is optimal. In terms of the expected gain this means: \[E(k,0,0) \geq E(k,1,0)-c\]
    For $E(k-1,0,0)$, we derive using the recursive definition that
    \begin{flalign*}
        E(k-1,0,0) & = \frac{k-2}{k} E(k,0,0)+\frac{1}{k} E(k,0,0) +\frac{1}{k} E(k,0,0)                            \\
                   & \geq \frac{k-1}{k}(E(k,1,0)-c) + \frac{1}{k} E(k,0,0)                                          \\
                   & = \frac{k-2}{k} E(k,1,0) + \frac{1}{k} E(k,1,0) + \frac{1}{k} (E(k,0,0)+c)  -c                 \\
                   & \geq \frac{k-2}{k} E(k,1,0) + \frac{1}{k} E(k,0,1) + \frac{1}{k} E(k,1,0) - c = E(k-1,1,0) - c.
    \end{flalign*}

    The last inequality uses $E(k,0, 0) + c \geq E(k,0,1)$ because $c \geq g_k$ and Lemma \ref{lemma:g_k_drag_out}.
    $E(k-1,0,1) \geq E(k-1,1,1)-c$ follows easily using Lemma~\ref{lemma:g_k_drag_out}. We now proved that rejecting is at least as good as reserving. Since we know that rejecting is also at least as good as accepting, we know that rejecting is optimal. This completes the proof.
\end{proof}

We now have all lemmata to easily prove the optimal actions for 1-candidates. 
 
\begin{theorem}
    There are thresholds $t_1$ and $t_3$ with $t_1 \leq t_2 \leq t_3 \leq t_4$, such that it is optimal to reserve $1$-candidates at $t_1 \leq k < t_3$, and it is optimal to reject at all other positions.
\end{theorem}
\begin{proof}
    First recall that rejecting 1-candidates at $t_4 \leq k \leq n$ is optimal (Lemma~\ref{lemma:ignore_1_from_t4}). There are two possible cases:
    \begin{itemize}
        \item[(1)] It is optimal to reject all $1$-candidates before $t_4$. Then set $t_1=t_2=t_3$ and the theorem is true.
        \item[(2)] There is a position before $t_4$ where rejecting $1$-candidates is not optimal. Since rejecting 1-candidates is at least as good as accepting them, we have a position before $t_4$ where reserving is optimal.

            Let $t_1$ be the first position where reserving $1$-candidates is optimal and $t_3$ the first position after that rejecting is always optimal.
            Using Lemma \ref{lemma:ignore_1_from_t4} we know that $t_3 \leq t_4$. We also know that $t_3 \geq t_2$ because otherwise rejecting 1-candidates at $t_2-1$ would be optimal and thus using Lemma \ref{lemma:reject_before} rejecting is always optimal. Contradiction to the case assumption. Furthermore, we know that $t_1 \leq t_2$ because again using Lemma~\ref{lemma:reserve_before} we know that reserving 1-candidates at $t_2$ is optimal. Lastly we know that reserving at $t_1 \leq k < t_3$ is optimal using Lemma~\ref{lemma:reserve_before} and Lemma~\ref{lemma:reject_before}.
    \end{itemize}
    This completes the proof.
\end{proof}

We now completely proved the structure of the optimal strategy as visualized in Figure~\ref{fig:opt}. Let us now discuss reasons for why we were able to explicitly give the thresholds $t_2$, $t_4$ and $t_5$ but could only prove the existence of $t_1$ and $t_3$. The thresholds $t_2$, $t_4$ and $t_5$ refer to the optimal actions for 2-candidates. When we encounter a 2-candidate the optimal strategy in the future does not depend on whether we reserve that candidate or not (see Figure~\ref{fig:opt}). Also, if that candidate is indeed the winning candidate, we will not reserve or accept any other candidate in the future as there will be no future 1- or 2-candidates. Thus, to determine whether reserving such a candidate is worth it, we just need to compare the probability that this candidate is the winning candidate and the reservation cost $c$.  

For 1-candidates the situation changes. First of all, the optimal strategy in the future depends on whether we have reserved the current best candidate or not. In addition, if this 1-candidate is indeed the winning candidate, we could still reserve or accept a future 2-candidate and lose. Thus, to determine whether we should reserve a 1-candidate, we need the expected gain of the state in which we will end up. Until now, we can only describe the expected gain in different states by recursive equations. Hence, comparing gains in different states algebraically is difficult.