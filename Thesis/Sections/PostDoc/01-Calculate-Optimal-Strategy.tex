\section{Calculating the Optimal Strategy}

Let us start with some important probabilities that will come up again and again. First of all, one should note that each of the possible relative ranks of a candidate is equally likely. That means that a candidate at position $k$ is a 1-candidate with probability $1/k$.  In the proof of the optimal strategy for the classic problem, that is the problem without the ability to reserve, Vanderbei showed the probability that a second-best candidate, after having seen $k$ of the $n$ candidates, will end up being the winning candidate \cite{vanderbei21}. 

\begin{samepage}
    \begin{theorem}
        \label{thm:g_k}
        The probability that the second-best candidate after seeing $k$ candidates, $1 \leq k \leq n$, is the winning candidate is 
        \[ g_k = \frac{k(k-1)}{n(n-1)}. \]
    \end{theorem}
\end{samepage}

\begin{proof}
    See Vanderbei \cite{vanderbei21} (1).
\end{proof}

Furthermore, Vanderbei showed the probability that the best candidate, after having seen \(1 \leq k \leq n\) candidates, is the winning candidate. This is the case when there is exactly one better candidate coming after that candidate. We now show that this is the same probability that the next 2-candidate after position \(k\) is the winning candidate.
\begin{theorem}
    \label{thm:f_k}
    The probability that the best candidate after having seen \(k\) candidates, $1 \leq k \leq n$, is the winning candidate (1), as well as the probability that the first \(2\)-candidate after the \(k\)-th candidate is the winning candidate (2), is given by
    \[f_k = \frac{k(n-k)}{n(n-1)}.\]
\end{theorem}

\begin{proof}
    It can easily be seen that the following recursion describes both probabilities:
    \[ f_n = 0 \text{ and } f_k = \frac{k}{k+1} f_{k+1} + \frac{1}{k+1}g_{k+1} \text{ for } 1 \leq k < n. \]
    For (1), the next candidate after the \(k\)-th candidate is not a 1-candidate with probability \(k/(k+1)\), and the current best candidate remains the best. With
    probability \(1/(k+1)\), the next candidate is a 1-candidate, and the previous best is now the second best. With probability \(g_{k+1}\), that candidate now remains second.
    For (2), the next candidate is not a 2-candidate with probability \(k/(k+1)\), and the probability that the next 2-candidate
    after that element is the winning candidate is just \(f_{k+1}\). With probability \(1/(k+1)\) the next candidate is a 2-candidate, and with probability \(g_{k+1}\) it remains second.

    We now show via backward induction that \(f_k = k(n-k)/(n(n-1))\) is a closed-form solution. The base case \(k = n\) is trivial. So, let the lemma be true for some \(1 < k \leq n\). For \(k-1\), we get
    \[f_{k-1} = \frac{k-1}{k}f_k + \frac{1}{k}g_k = \frac{(k-1)(n-k) + (k-1)}{n(n-1)} = \frac{(k-1)(n-(k-1))}{n(n-1)}.\]
    The first equality is by the recursive definition, and the second follows from the induction hypothesis. This completes the proof.
\end{proof}

Both probabilities are plotted in Figure \ref{fig:f_and_g} for \(n=1000\). The probability that the current second-best candidate remains the
second-best increases as the interviews progress, while the probability that the current best candidate is the
winning candidate increases until half of the interviews are done and then decreases again.

\begin{figure}[tb!]
    \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{F_K.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{G_K.pdf}
    \end{minipage}
    \caption{Left: The probability $f_k$ for $n=1000$. Right: The probability $g_k$ for $n=1000$. The candidate position $k$ is on the $x$-axis, and the corresponding probability is on the $y$-axis.}
    \label{fig:f_and_g}
\end{figure}



We now show that the number of candidates already interviewed $k$, the number of candidates reserved $r$, and whether the best and/or the second-best candidate seen so far is reserved is all we need to give the expected gain of the optimal strategy in the current state.
Let us denote the expected gain in this state as $E(k,r,b,s)$ where $b=1$ if the best candidate seen so far is reserved and $b=0$ otherwise. The same applies to $s$ and the second-best candidate seen so far.

After having seen all candidates and having reserved $r$ of them, the gain is $1-rc$ if we have reserved the second-best candidate and $-rc$ otherwise. Thus, for $b,s \in \{0,1\}$ and $0 \leq r \leq k$ we have that
\[E(n,r,b,s) = s - rc.\]

Now suppose we have only seen $k$ of the $n$ candidates and have reserved $r$ of them already. It is easy to see that since the candidates are in random order, the rank of the next candidate is equally likely to be any from best to worst seen so far. There are three interesting cases. If the next candidate is neither a 1-candidate nor 2-candidate, the optimal strategy obviously rejects it as this candidate can not be the winning candidate. This happens with a probability of $(k-1)/(k+1)$. On the other hand, if the next candidate is a 1- or a 2-candidate, there are three options. We could either accept the candidate, which will end the interview process, or we could reserve it or reject it. The optimal strategy will take the action that maximizes the expected gain. The probability that the next candidate is a 1-candidate is $1/(k+1)$, and we showed in Theorem~\ref{thm:f_k} that the probability, in this case, is $f_{k+1}$ that this candidate is the winning candidate. The probability that the next candidate is a 2-candidate is again $1/(k+1)$, and by Theorem~\ref{thm:g_k}, the probability that this is the winning candidate is $g_{k+1}$. Thus, for \(0 \leq k < n\), $b, s \in \{0,1\}$ and $0 \leq r \leq k$, we have that
\begin{flalign}
    E(k, r, b, s) & = \frac{k-1}{k+1} E(k+1,r,b,s)                \notag                                     \\
                  & + \frac{1}{k+1} \max \left(E(k+1, r, 0, b), f_{k+1} - rc, E(k+1, r+1, 1, b)\right) \notag \\
                  & + \frac{1}{k+1} \max \left(E(k+1, r, b, 0), g_{k+1} -rc, E(k+1, r+1, b, 1)\right).
\end{flalign}

Calculating the optimal strategy now comes down to calculating $E(k,r,b,s)$ in a dynamic programming fashion and comparing the expected gain of the different actions. The overall expected gain of the optimal strategy is $E(0,0,0)$, as this represents the expected gain of the optimal strategy before having seen any candidate and not having reserved anything yet.

With the observation that the optimal action does not depend on how much was reserved up to the current point, the recursive equation can be simplified further. It can easily be shown via backward induction that $E(k,r,b,s) = E'(k,b,s) - rc$ with $E'$ defined as follows.

For $k=n$:
\[ E'(k,b,s) = s \]

For $0 < k < n$:
\begin{flalign}
    E'(k, b, s) & = \frac{k-1}{k+1} E'(k+1, b, s)     \notag                                     \\
               & + \frac{1}{k+1} \max \left(E'(k+1, 0, b), f_{k+1}, E'(k+1, 1, b)-c\right) \notag \\
               & + \frac{1}{k+1} \max \left(E'(k+1, b, 0), g_{k+1}, E'(k+1, b, 1)-c\right)
\end{flalign}

For simplicity, we will, from now on, refer to $E'$ as $E$. $E'$ is basically the expected gain of the optimal strategy in the current state if we have not paid anything to come into this state. Calculating the optimal strategy can now be simplified to calculating $E(k,b,s)$ and then comparing the different expected gains of each action.

We now show that the expected gain advantage of having the current second-best candidate reserved over not having it reserved is $g_k$. Intuitively this should be correct as there are two cases. The reserved candidate is the winning candidate, which happens with probability $g_k$, or the candidate is not the winning candidate, but thus will not affect our future strategy as this fact will reveal as soon as we have to take action again. We now show this formally using backward induction and the recursive definition of the expected gain we introduced.

\begin{lemma}
    \label{lemma:g_k_drag_out}
    $E(k,b,1) = E(k,b,0) + g_k$ with $b \in \{0,1\}$ and $0 \leq k \leq n$.
\end{lemma}

\begin{proof}
    Prove by backward induction over $k$. The base case $k=n$ is trivial. So let $E(k+1,b,1) = E(k+1,b,0) + g_{k+1}$ for some $1 < k+1 \leq n$ and $b \in \{0,1\}$.
    By the recursive definition of $E(k,b,1)$ we have that
    \begin{flalign*}
        E(k,b,1) &= \frac{k-1}{k+1} E(k+1,b,1) \\
        & + \frac{1}{k+1} \max (E(k+1, 0, b), f_{k+1}, E(k+1, 1, b) - c) \\
        & + \frac{1}{k+1} \max (E(k+1, b, 0), g_{k+1}, E(k+1, b, 1) - c).
    \end{flalign*}
    An easy calculation shows that \[\frac{k-1}{k+1}g_{k+1} = \frac{(k-1)k(k+1)}{(k+1)n(n-1)} =  \frac{(k-1)k}{n(n-1)} = g_k. \]
    By the induction hypothesis we have $E(k+1,b,1) = E(k+1,b,0) + g_{k+1}$ and we can conclude that
    \begin{flalign*}
        E(k,b,1) &= \frac{k-1}{k+1} E(k+1,b,0) + \frac{k-1}{k+1} g_{k+1} \\
        & + \frac{1}{k+1} \max (E(k+1, 0, b), f_{k+1}, E(k+1, 1, b) - c) \\
        & + \frac{1}{k+1} \max (E(k+1, b, 0), g_{k+1}, E(k+1, b, 1) - c) \\
        & = E(k,b,0) + g_k.
    \end{flalign*}
    This completes the proof.
\end{proof}

Note that the optimal strategy for related problems can be calculated similarly. One just has to modify the recursive equation in the correct way. For example, for the problem of finding the $m$-th best candidate, one would not only store whether the current best and second best are reserved, but whether the best, second-best, ..., $m$-th best candidate is reserved. $m=1$ is the \textit{Secretary Problem} and $m=2$ is the \textit{Postdoc Problem}.
In Figure \ref{fig:mth_best_and_gain}, the competitive ratio for $m=1,2,3,4,5$ is plotted for the different reservation costs and $n=1000$.

\begin{figure}
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{GAIN_PLOT.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{MTH_BEST_PLOT.pdf}
    \end{minipage}
    \caption{Left: The expected gain of the optimal strategy for the \textit{Postdoc Problem} in the \textit{reservation-per-item} model, for $n=1000$. The reservation cost $c$ is on the $x$-axis and the expected gain is on the $y$-axis. Right: The competitive ratio for selecting the $m$-th best candidate in the \textit{reservation-per-item} model, for $m=1,2,3,4,5$ and $n=1000$. The reservation cost $c$ is on the $x$-axis and the competitive ratio is on the $y$ axis.}
    \label{fig:mth_best_and_gain}
\end{figure}