\section{Extending into Infinity}

In this section, we focus on analyzing the expected gain and the thresholds for the optimal strategy as the number of candidates tends towards infinity. Our goal is to prove that for all thresholds $t_1,t_2,t_3,t_4, t_5$, the ratio $t_i/n$ converges as the number of candidates $n$ goes to infinity. We also show how we can calculate those limits. So far, the number of candidates was implicitly fixed and not variable. To clarify the dependency on the number of candidates, we will introduce some notational changes. Henceforth, the probabilities $g_k$, $f_k$, and $h_k$ will be denoted as $g_k^n$, $f_k^n$, and $h_k^n$, respectively. We will represent the expected gain $E$ as $E_n$, and the thresholds will be expressed as $t_i^n$, where $i \in \{1,2,3,4,5\}$.

Formally we will look at the following limits in this section:
\[ r_i = \lim_{n \to \infty} \frac{t_i^n}{n} \; \textit{ for } \; i \in \{1,2,3,4,5\}. \]

We look at each threshold one after another. After analyzing the limits of the thresholds $t_2$, $t_4$ and $t_5$, we show how we can calculate the limits of the thresholds $t_1$ and $t_3$. Until now we have only proven their existence, but together with the non-recursive solutions for the expected gain we derived in the previous section, we are able to calculate the limits.

\subsection{The Easy Limits}

Since we already have closed-form solutions for the thresholds $t_4^n$ and $t_2^n$, it is easy to obtain their limits $r_4$ and $r_2$ using standard calculus.


\begin{theorem}
    \label{lemma:r_4}
    The limit of the ratio $t^n_4 / n$ as $n$ approaches infinity is
    \[r_4 = \frac{1}{2} + \sqrt{\frac{1}{4} - c}. \]
\end{theorem}
\begin{proof}
    We have by definition that $t_4^n = \lceil n/2 + \sqrt{n^2/4 - cn(n-1)} \rceil$. Using standard calculus, the limit of $\lceil n/2 + \sqrt{n^2/4 - cn(n-1)} \rceil /n$ as $n$ goes to infinity can be calculated.
\end{proof}

\begin{theorem}
    \label{lemma:r_2}
    The limit of the ratio $t_2^n / n$ as n approaches infinity is
    \[r_2 = \sqrt{c}. \]
\end{theorem}
\begin{proof}
    We have by definition that $t_2^n = \lceil 1/2 + \sqrt{1/4 + cn(n-1)} \rceil$. Using standard calculus, the limit of $\lceil 1/2 + \sqrt{1/4 + cn(n-1)} \rceil/n$ as $n$ goes to infinity can be calculated.
\end{proof}

\subsection{The Threshold $r_5$}

Because we lack a closed form for $t_5^n$, establishing the limit $r_5$ is somewhat more challenging. We first prove a lemma that helps us derive limits for thresholds of the form $t_i^n = \max \{t \in \N \mid p(n, t) > 0 \}$.
\begin{lemma}
    \label{theorem:limit_theorem}
    Let $f: \N \times \N \to \R$ be an arbitrary function and
    \begin{itemize}
        \item[(i)] for all $n \in \mathbb{N}$ there exists exactly one $t \in \mathbb{N}$ such that $f(n,t) > 0 \geq f(n,t+1)$
        \item[(ii)]$\hat{f}(r) = \lim_{n \to \infty} f(n, \lceil rn \rceil)$ is well-defined for all $r \in (0,1)$ and differentiable
        \item[(iii)] it exists an $x \in (0,1)$ with $\hat{f}(x) = 0$ and $\hat{f}'(x) < 0$
    \end{itemize}
    then we have
    \[\lim_{n \to \infty} \frac{\max\footnotemark \{t \in \mathbb{N} \mid f(n,t) > 0\}}{n}= x.\]
\end{lemma}

\footnotetext{The maximum of the empty set is here defined to be 0. Since we are looking at finite sets, the maximum exists at all times.}
\begin{proof}
    Let $\varepsilon > 0$ be arbitrary. We show that there is an $N' \in \N$ such that for all $n \geq N'$ we have
    $x - \varepsilon \leq \max \{t \in \mathbb{N} \mid f(n,t) > 0\}/n \leq x + \varepsilon$.

    Since $\hat{f}'(x) < 0$, it exists $x_1,x_2 \in \R, x_1 < x_2$ with $|x_{i} - x| < \varepsilon / 2$ for $i \in \{1,2\}$ and  $\hat{f}(x_1) > 0$ and $\hat{f}(x_2) < 0$.
    By the definition of $\hat{f}$ there must exist an $N$ such that $f(n,\lceil x_1 n \rceil) > 0$ and $f(n,\lceil x_2 n \rceil) < 0$ for all $n > N$.
    With (i), it follows that for all $n > N$, we have
    \[(x-\varepsilon/2)n \leq \lceil x_1 n \rceil \leq \max \{t \in \mathbb{N} \mid f(n,t) > 0\} \leq \lceil x_2n \rceil \leq (x + \varepsilon/2)n + 1\]
    Thus, for $N' = \max \{ N, 2 / \varepsilon \}$, we have $x - \varepsilon \leq \max \{t \in \mathbb{N} \mid f(n,t) > 0\}/n \leq x + \varepsilon$ for all $n > N'$. This completes the proof.
\end{proof}


By definition, we have $t_5^n = \max \{k \in \N \mid h_k + f_k - c > 0 \} + 1$.
We are going to use Lemma~\ref{theorem:limit_theorem} to prove the limit of $t_5^n/n$. It is not too hard to show that $h_k^n + f_k^n - c$ fulfills (i) of Lemma~\ref{theorem:limit_theorem}. Let us continue with (ii).

\begin{lemma}
    \label{lemma:limit_f_plus_h}
    For $r \in (0,1)$ we have \[\lim_{n \to \infty} \left ( h_{\lceil rn \rceil} + f_{\lceil rn \rceil} - c \right ) = r-r^2(1+ \ln(r))-c.\]
\end{lemma}
\begin{proof}
    For $r \in (0,1)$ and $n \in \mathbb{N}$ we have by definition that
    \(h_{\lceil rn \rceil} + f_{\lceil rn \rceil} = g_{\lceil rn \rceil} (H_{n - 2} - H_{\lceil rn - 2\rceil}) + f_{\lceil rn \rceil}.\)
    We first show that $\lim\limits_{n \to \infty} \left (H_{n-2} - H_{\lceil rn - 2\rceil} \right ) = -\ln(r)$.
    Using the fact that $\lim_{n \to \infty} (H_n - (\ln(n) + \gamma)) = 0$, where $\gamma$ is Euler's constant \cite{knuth1} we get
    \[\lim\limits_{n \to \infty} \left ( H_{n-2} - \ln(n-2) - \gamma \right ) - \lim\limits_{n \to \infty} \left ( H_{\lceil rn - 2 \rceil} - \ln(\lceil rn -2 \rceil) - \gamma \right ) = 0. \]
    By standard calculus and the fact that $\ln(a) - \ln(b) = \ln(a/b)$ for all $a,b \in \mathbb{R}$ we get
    \[\lim\limits_{n \to \infty} \left ( \left (H_{n-2} - H_{\lceil rn - 2 \rceil} \right ) - \ln \left (\frac{n-2}{\lceil rn - 2\rceil} \right ) \right ) = 0. \]
    Note that by the continuity of the natural logarithm, we have
    \[\lim\limits_{n \to \infty}  \ln \left (\frac{n-2}{\lceil rn - 2\rceil} \right ) = \ln \left ( \lim\limits_{n \to \infty}   \frac{n-2}{\lceil rn - 2\rceil} \right ) = \ln \left ( \frac{1}{r} \right ) = -\ln(r). \]
    It follows that
    \(\lim\limits_{n \to \infty} \left ( H_{n-2} - H_{\lceil rn - 2 \rceil} \right ) = -ln(r)\).
    Furthermore, for the limits of $g^n$ and $f^n$ we get
    \[\lim\limits_{n \to \infty} g^n_{\lceil rn \rceil}  = \lim\limits_{n \to \infty} \frac{\lceil rn \rceil \lceil rn - 1\rceil}{n(n-1)} = r^2 \text{ and } \lim\limits_{n \to \infty} f^n_{\lceil rn \rceil}  = \lim\limits_{n \to \infty} \frac{\lceil rn \rceil \lceil n - rn \rceil}{n(n-1)} = r - r^2\]

    The lemma follows using standard calculus.
\end{proof}

It is not too hard to show that $p(r) = r-r^2(1 + \log r) - c$ has exactly two roots in \((0,1)\) for $0 < c < 1/4$ and that the slope at the second root is negative.
Thus, all conditions for Lemma~\ref{theorem:limit_theorem} are met, and we can derive this section's main theorem.

\begin{theorem}
    \label{theorem:r_5}
    The limit of the ratio $r_5 = t_5^n / n$ is the second root of
    \[p(r) = r-r^2(1+\ln(r)) - c\]
\end{theorem}

\subsection{The Threshold $r_3$}

Up to this point, we have only proven the existence of the threshold $t_3^n$. If the optimal strategy reserves 1-candidates, the threshold $t_3^n$ is the first position at which rejecting 1-candidates is optimal. If it is never optimal to reserve 1-candidates we set $t_3^n = t_2^n$. Let us define:
\[p_1^n(k) = f_{t_4^n-1}^n + \frac{g_{t_4^n-1}^n - g_k^n}{2} - c(H_{t_4^n-1}-H_{k}) \]
\[p_2^n(k) = k \frac{E(t_4^n-1,1,0)+c}{t_4^n-1}+\frac{(2n+3k-t_4^n)(t_4^n-1-k)}{2n(n-1)}
    -c - ck \sum\limits_{i=k}^{t_4^n-2}\frac{H_{t_4^n-1} - H_{i+1}}{i(i+1)} \]

Note here that for $t_3^n-1 \leq k \leq t_4^n-1$ we have $E(k,0,0) = p_1^n(k)$ and $E(k,1,0) = p_2^n(k)$ (Lemma~\ref{lemma:closed_form_t3}). Let $p_n: (0,1] \mapsto \mathbb{R}$, $p_n(r) = p_2^n(\ceil*{rn}) - c - p_1^n(\ceil*{rn})$. We will now find a $p: (0,1] \mapsto \mathbb{R}$ such that $p_n \overset{n}{\rightrightarrows} p$ on $[a,1]$, $a \in (0,1]$, that is $p_n$ converges uniformly to $p$ on $[a, 1]$. For each summand of $p_n$, we find a corresponding term that it uniformly converges to. We then show that $p$ is a parabola and that one of the roots, if existent, corresponds to the limit $r_3$. Let us start with the uniform convergence proofs.

\begin{lemma}
    \label{lemma:limit_1_harmonic}
    For $a \in (0,1]$ we have that
    \[H_{t_4^n - 1} - H_{\ceil*{rn}} \overset{n}{\rightrightarrows} \ln(\frac{r_4}{r}) \textit{ on } [a,1].\]
\end{lemma}
\begin{proof}
    Let $\varepsilon > 0$ be arbitrary. We prove that there is an $N$ such that for all $n > N$ and $r \in [a,1]$ we have $|H_{t_4^n-1} - H_{\ceil*{rn}} - \ln(r_4/r)| < \varepsilon$.
    
    We know that $\gamma \leq H_n - \ln(x) \leq \gamma + 1/(2n)$, where $\gamma$ is the Euler-Mascheroni constant \cite{knuth1}. Thus we have that \[|H_{t_4^n-1} - H_{\ceil*{rn}} - \ln(\frac{r_4}{r})| \leq |\ln(\frac{t_4^n-1}{\ceil*{rn}}) - \ln(\frac{r_4}{r})| + \frac{1}{2t_4^n - 2} + \frac{1}{2\ceil*{rn}}.\]

    Since we have already shown that $t_4^n/n$ converges to $r_4$ (Lemma~\ref{lemma:r_4}), $t_4^n$ diverges and thus there is an $N_1$ such that $1/(2t_4^n -2) < \varepsilon/4$ for all $n > N_1$. 
    Since $r \geq a > 0$, there is an $N_2 > N_1$ such that $1/(2\ceil*{rn}) < \varepsilon/4$ for all $n > N_2$. 
    Now we are left to show that there is $N_3 > N_2$ such that $|\ln(\frac{t_4^n-1}{\ceil*{rn}}) - \ln(\frac{r_4}{r})| < \varepsilon/2$ for all $n > N_3$. 
    Since $|\ln(\frac{t_4^n - 1}{\ceil*{rn}}) - \ln(\frac{t_4^n}{rn})| \leq |\ln(\frac{rn}{\ceil*{rn}})| + |\ln(\frac{t_4^n -1}{t_4^n})|$, we have an $N_4 > N_3$ such that $|\ln(\frac{t_4^n-1}{\ceil*{rn}}) - \ln(\frac{r_4}{r})| \leq |\ln(\frac{t_4^n}{rn}) - \ln(\frac{r_4}{r})| + \varepsilon/4 = |\ln(\frac{t_4^n}{r_4n})| + \varepsilon/4$ for all $n > N_4$. 
    Since $t_4^n/n$ converges to $r_4$ there is an $N_5 > N_4$ such that $|\ln(\frac{t_4^n}{r_4n})| < \varepsilon/4$. 
    Combining all the above we get that for $n > N_5$ and $r \in [a,1]$, we have $|H_{t_4^n-1} - H_{\ceil*{rn}} - \ln(r_4/r)| < \varepsilon$, which completes the proof.
\end{proof}

\begin{lemma}
    \label{lemma:limit_sum_integral}
    For $a \in (0,1]$ we have that
    \[  \ceil*{rn} \sum\limits_{i=\ceil*{rn}}^{t_4^n-2}\frac{H_{t_4^n-1} - H_{i+1}}{i(i+1)} \rightrightarrows \ln(\frac{r_4}{r}) + \frac{r}{r_4} - 1 \textit{ on } [a,1].\]
\end{lemma}
\begin{proof}
    Again using the fact that $|H_n - \ln(n)| < \gamma + 1/(2n)$, it is easy to show that for every $\varepsilon > 0$ there is an $N \in \N$ such that for all $r \in [a,1]$ and $n > N$, we have
    \[ \left | \ceil*{rn} \sum\limits_{i=\ceil*{rn}}^{t_4^n-2}\frac{H_{t_4^n-1} - H_{i+1}}{i(i+1)} - rn \sum\limits_{i=\ceil*{rn}}^{\ceil*{r_4n}}\frac{\ln(r_4n) - \ln (i)}{i^2} \right | < \varepsilon. \]

    Using $x_i = i/n$, $\Delta x = x_{i} - x_{i+1}= 1/n$ and $f(x) = \ln(r_4/x)/x^2$, we get that

    \[ n \sum_{i=\ceil*{rn}}^{\ceil*{r_4n}}\frac{\ln(r_4n) - \ln (i)}{i^2} = \sum_{i=\ceil*{rn}}^{\ceil*{r_4n}}f(x_i) \Delta x. \]

    This sum is a left approximation for the following integral, and again we have that for every $\varepsilon > 0$ there is an $N \in \N$ such that for every $r \in [a,1]$ and $n > N$ we have
    \[ \left | \int\limits_{\ceil{rn}/n}^{(\ceil{r_4n} + 1)/n} \frac{\ln(r_4/x)}{x^2} \; dx  \; - \sum_{i=\ceil*{rn}}^{\ceil*{r_4n}}f(x_i) \Delta x \;  \right | \leq \frac{M_1(\ceil{r_4n} + 1 - \ceil{rn})^2}{2n^3} < \varepsilon, \]
    where $M_1$ is the maximum absolute value of the derivate of $\ln(r_4/x)/x^2$ on $[a,1]$. Note that $M_1$ does not depend on $r$.

    Lastly it is easy to see that for every $\varepsilon$ there is an $N \in \N$ such that for every $n > N$ and $r \in [a,1]$ we have that
    \[ \left | \int_{(\ceil{rn}/n}^{(\ceil{r_4n} + 1)/n} \frac{\ln(r_4/x)}{x^2} \; dx - \int_r^{r_4} \frac{\ln(r_4/x)}{x^2} \; dx \right | < \varepsilon. \]

    Using
    \[ r \int_r^{r_4} \frac{\ln(r_4/x)}{x^2} \; dx = \ln(\frac{r_4}{r}) + \frac{r}{r_4} - 1, \] 
    the proof is complete.
\end{proof}

Since we already showed that $r_2$, $r_4$ and $r_5$ are the limits of the corresponding threshold ratios, and we have non-recursive solutions for the expected gain, we can derive the limits of the expected gain at those thresholds.

\begin{lemma}
    \label{lemma:limit_E_r4}
    \[\lim_{n \to \infty}E(t_4^n-1, 1, 0) = r_4 \ln(\frac{r_5}{r_4}) - c(1-\frac{2r_4}{r_5}) + r_4(r_5-r_4)\]
\end{lemma}

\begin{proof}
    Since the non-recursive solution of the expected gain at $t_4^n-1$ (Lemma~\ref{lemma:closed_form_t4}) depends on the expected gain at $t_5^n-1$, we first need $\lim_{n \to \infty} E(t^n_{t_5}-1,1,0)$. Lemma~\ref{lemma:closed_form_t5} provides us with the non-recursive solution, and using standard calculus and the knowledge that $t_5^n/n$ converges to $r_5$ (Theorem~\ref{theorem:r_5}), we obtain
    \[\lim_{n \to \infty} E(t^n_{t_5}-1,1,0) = r_5 - r_5^2 - r_5^2 \ln(r_5) = r_5 - r_5^2(1+\ln(r_5)).\]
    From Theorem \ref{theorem:r_5} we know that $r_5-r_5^2(1+\ln(r_5)) = c$ and thus
    \[\lim_{n \to \infty} E(t^n_{t_5-1},1,0) = c. \]
    Furthermore, Lemma~\ref{lemma:closed_form_t4} provides us the non-recursive solution of $E(t_4^n-1, 1, 0)$ and using simple calculus and the above limit we get that
    \[ \lim_{n \to \infty}E(t_4^n-1, 1, 0) = r_4 \ln(\frac{r_5}{r_4}) - c(1-\frac{2r_4}{r_5}) + r_4(r_5-r_4). \]
    This completes the proof.
\end{proof}

Using the above three lemmata, the uniform convergence of $p_n$ on $[a,1]$ follows using simple rearrangements and standard calculus.

\begin{lemma} 
    \label{lemma:uniform_limit_p}
    For $a \in (0,1]$, $p_n \overset{n}{\rightrightarrows} p$ on $[a,1]$ with
    \[ p(r) = -r^2 + r \left (\ln \left ( \frac{r_5}{r_4} \right ) + r_5-2c \left (\frac{1}{r_4} - \frac{1}{r_5} \right ) \right ) - c.\] 
\end{lemma}

Note that $p(r)$ is a parabola that opens downwards, and we can easily find its roots. We will now prove that if a second root of this parabola exists, it is the limit $r_3$. This result should be intuitively clear because the functions $p_n$ represent the advantage of reserving over rejecting. In the limit, the threshold should therefore be at one of the roots.

\begin{theorem}
    \label{thm:has_roots}
    If $p$ has a root $x_0$ with $r_2 \leq x_0 \leq r_4$ and $p'(x_0)<0$ then $r_3 = x_0$.
\end{theorem}

\begin{proof}
    Let $\varepsilon' > 0$ be arbitrary and $\varepsilon = \varepsilon'/2$.
    Since $p'(x_0) < 0$, we have:
    \begin{itemize}
        \item[(i)]  It exists an $x_l$ such that $x_0-\varepsilon < x_l < x_0$, $0 < x_l$ and $p(x_l) > 0$.
        \item[(ii)] It exists an $x_r$ such that $x_0 < x_r < x_0 + \varepsilon$ and $p(x_r) < 0$.
    \end{itemize}

    Now let $\delta = \min(|p(x_l)|, \; |p(x_r)|)$. Since $p$ is a downward opening parabola and $x_0$ is thus the second root, we have for all $r \geq x_r$ that $p(r) \leq p(x_r)$. 

    Because $p_n$ uniformly converges to $p$ on $[x_l, 1]$ and $t_2^n/n$ converges to $r_2$, we have an $N > 2/\varepsilon$ such that:
    \begin{itemize}
        \item[(i)] For all $n \geq N$ and $r \in [x_l,1]$, $|p_n(r) - p(r)| < \delta$.
        \item[(ii)] For all $n \geq N$ we have that $t_2^n < (r_2+\varepsilon)n \leq (x_0 +\varepsilon)n$.
    \end{itemize}

    So let $n > N$. Then, $p_n(x_l) > 0$, $p_n(x_r) < 0$ and $p_n(r) < 0$ for all $r > x_r$. Since we either have $E(t_3^n-1,1,0) - c - E(t_3^n-1,0,0) > 0$ or $t_3^n = t_2^n$, we know that because $p_n(r) > 0$ for all $r\geq x_r$, we have $t_3^n \leq \max(t_2^n, \ceil{nx_r})$. It follows that $t_3^n \leq \max(t_2^n, \ceil*{nx_r}) \leq \ceil*{n(x_0+\varepsilon)}$.

    Since $p_n(x_l) > 0$ we have $t_3^n > \ceil*{nx_l} \geq \ceil*{n(x_0 - \varepsilon)}$. So because $n>N>2/\varepsilon$ we have
    \[ n(x_0-\varepsilon') < \ceil*{n(x_0-\varepsilon)} < t_3^n \leq \ceil*{n(x_0+\varepsilon)} < n(x_0 + \varepsilon').\]

    This completes the proof.
\end{proof}

\begin{theorem}
    \label{thm:no_roots}
    If $p$ has no roots, then $r_3 = r_2 = \sqrt{c}$.
\end{theorem}
\begin{proof}
    Since $p$ is a downward opening parabola, we have that $p(r) < 0$ for all $r \in [r_2,1]$. Thus, for some $\delta > 0$ we have that $p(r) < -2 \delta$ for all $r\in [r_2,1]$. Since $p_n$ uniformly converges to $p$ on $[r_2, 1]$ we have an $N \in \N$ such that for all $n > N$ we have for all $r \in [r_2,1]$ that $p_n(r) < -\delta$. Let $n > N$. We then have that for $t_3^n-1 \leq k \leq t_4^n-1$, $E_n(k,1,0) - c - E_n(k,0,0) < 0$. Thus $t_3^n = t_2^n$ and it follows that $r_3 = r_2$. This completes the proof.
\end{proof}

Thus, we can find the limit $r_3$ by finding the roots of $p$. We can easily do so using the pq-formula. Using numeric methods, it seems that for $c > 0.235469$, $p$ has no roots, and for $c<0.235469$, $p$ has two roots. Figure~\ref{fig:r_3} shows $r_3$ in dependence on the reservation costs and the parabola $p$ for different reservation costs.

\begin{figure}[tb!]
    \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{R3_PARABOLAS.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{R3_PLOT.pdf}
    \end{minipage}
    \caption{Left: The parabola $p$ for varying reservation costs. The ratio $r$ is on the $x$-axis and $p(r)$ on the $y$-axis. Right: The limit $r_3$ in dependence on the reservation cost. The reservation cost is on the $x$-axis and the limit $r_3$ on the $y$-axis.}
    \label{fig:r_3}
\end{figure}

\subsection{The Threshold \texorpdfstring{\ensuremath{r_1}}{r1}}

The last threshold limit that is missing is $r_1$. We use similar methods as in the previous section to find it. So let

\vspace{-0.1in}
 \[p_1^n(k) = \frac{kE(t',0,0)}{t'} + \frac{t'-k}{t'}(E(t', 1, 0) - c + \frac{g_{t'}}{2})
 - \frac{k(t'-k)}{2n(n-1)}
 - ck \sum\limits_{i = k}^{t'-1}\frac{H_{t'} - H_{i+1}}{i(i+1)} \]
\[p_2^n(k) = E(t', 1, 0) + \frac{t'(t'-1) - k(k-1)}{2n(n-1)} - c(H_{t'} - H_{k}),\]
\vspace{0.1in}

where $t' = t_2^n -1$. Note again that for $t_1^n - 1 \leq k \leq t_2^n-1$, these are the non-recursive solutions which means that $E_n(k,0,0) = p_1^n(k)$ and $E_n(k,1,0) = p_2^n(k)$ (Lemma~\ref{lemma:closed_form_t1}).

We again define $p_n: (0,1] \mapsto \mathbb{R}, \; p_n(r) = p_2^n(\ceil{rn}) - c - p_1^n(\ceil{rn})$ and now find a $p: (0,1] \mapsto \mathbb{R}$, such that $p_n \overset{n}{\rightrightarrows} p$ on $[a,1]$ for $a \in (0,1]$. The terms in $p_n$ that are difficult to analyze for the uniform limit are very similar to those in the previous section. We will skip the proofs here because they are almost identical to those for $r_3$.

\begin{lemma}
    \label{lemma:limit_2_h}
    For $a \in (0,1]$ we have that
    \[H_{t_2^n - 1} - H_{\ceil*{rn}} \overset{n}{\rightrightarrows} \ln(\frac{r_2}{r}) \textit{ on } [a,1].\]
\end{lemma}
\begin{proof}
    Similar to the proof of Lemma~\ref{lemma:limit_1_harmonic}.
\end{proof}

\begin{lemma}
    \label{lemma:limit_2_integral}
    For $a \in (0,1]$ we have that
    \[  \ceil*{rn} \sum\limits_{i=\ceil*{rn}}^{t_2^n-2}\frac{H_{t_2^n-1} - H_{i+1}}{i(i+1)} \overset{n}{\rightrightarrows} \ln \left (\frac{r_2}{r} \right ) + \frac{r}{r_2} - 1 \textit{ on } [a,1]. \]
\end{lemma}
\begin{proof}
    Similar to the proof of Lemma~\ref{lemma:limit_sum_integral}.
\end{proof}

We are now just missing the limits of the expected gain at $E(t_2^n-1,0,0)$ and $E(t_2^n-1,1,0)$. Since we already showed that $t_5^n/n$, $t_4^n/n$, $t_3^n/n$ and $t_2^n/n$ converge as $n$ goes to infinity, we can analyze the limits of the expected gain at each of those thresholds. One would start with $\lim_{n \to \infty}E(t_4-1,1,0)$ and work its way down to  $\lim_{n \to \infty}E(t_2-1,1,0)$. To make the equations shorter we define $E(r_i, b,s) = \lim_{n \to \infty} E_n(t_i^n -1, b ,s)$ for $b,s \in \{0,1\}$. 

\begin{lemma}
    ~\\
    \label{lemma:alot_limits}
    \vspace*{-0.15in}
    \begin{itemize}
        \item[(i)] $\begin{aligned}E(r_4, 1, 0) = r_4 \ln \left (\frac{r_5}{r_4} \right ) + r_4(r_5 - r_4) - c \left (1-\frac{2r_4}{r_5} \right )\end{aligned}$
        \item[(ii)] $\begin{aligned}E(r_3,0,0) = \frac{r_4^2 - r_3^2}{2} + c \left (1-\ln \left (\frac{r_4}{r_3} \right ) \right )\end{aligned}$
        \item[(iii)] $\begin{aligned}E(r_3,1,0) = \frac{r_4^2 + 2r_3r_4 - 3r_3^2}{2} + r_3 \frac{E(r_4-1,1,0) - c}{r_4} + c \left (1-\ln \left (\frac{r_4}{r_3} \right ) \right )\end{aligned}$
        \item[(iv)] $\begin{aligned}E(r_2,0,0) = \frac{r_2}{r_3} E(r_3,0,0) + \frac{r_3-r_2}{r_3} \left (E(r3,1,0) - 2c+ r_3^2 \right ) - 2c \left (\ln \left (\frac{r_3}{r_2} \right ) + \frac{r_2}{r_3} - 1 \right )\end{aligned}$
        \item[(v)] $\begin{aligned} E(r_2, 1, 0) = E(r_3,1,0) - 2c \ln \left (\frac{r_3}{r_2} \right ) - r_2^2 + r_3^2 \end{aligned}$
    \end{itemize}
\end{lemma}
\begin{proof}
    Similar to the proof of Lemma~\ref{lemma:limit_E_r4}.
\end{proof}

Now obtaining the uniform limit of $p_n$ on $[a,1]$ comes down to standard calculus, Lemma~\ref{lemma:limit_2_h}, Lemma~\ref{lemma:limit_2_integral} and Lemma~\ref{lemma:alot_limits}. We leave the details to the reader and give the final uniform limit.

\begin{lemma}
    For $a \in (0,1]$, $p_n \overset{n}{\rightrightarrows}  p$ on $[a,1]$ with
    \[ p(r) = -r^2 - qr - c, \; q = \frac{E(r_2,0,0) - E(r_2,1,0)}{r_2} - r_2. \]
\end{lemma}

Again the limit is a simple parabola. As in the previous section, we use the roots of this downwards opening parabola $p$ to derive the limit of $r_1$.

\begin{theorem}
    If $p$ has as two roots $x_0$ and $x_1$ with $0 < x_0 < r_2 < x_1$, then $r_1 = x_0$.
\end{theorem}
\begin{proof}
    The proof is similar to that of Theorem~\ref{thm:has_roots}. Let $\varepsilon > 0$. Since $p$ is a downwards-opening parabola, we have that for $x_0 < x < x_1$, $p(x) > 0$ and for $x < x_0$, $p(x) < 0$. 
    Since $r_2$ is the limit of $(t_2^n-1)/n$, we have an $N_1 \in \mathbb{N}$ such that for all $n > N_1$ we have \[x_{r_2,l} = r_2 - \frac{r_2 - x_0}{2} < \frac{t_2^n-1}{n} < r_2 + \frac{x_1-r_2}{2} = x_{r_2,r}. \]

    
    Since $p_n \overset{n}{\rightrightarrows} p$ on $[x_0,1]$, we have an $N_2 > N_1$ such that for all $n > N_2$ and $r \in [x_{r_2,l}, x_{r_2,r}]$, $p_n(r) > 0$. It follows that $p_n((t_2^n-1)/n) > 0$ and thus $E_n(t_2^n-1, 1,0) - c - E_n(t_2^n-1,0,0)>0$. We, therefore, know that $t_1^n$ is the first position where reserving 1-candidates is optimal. 

    Let $x_{0,l} \in \mathbb{R}$ such that $0 < x_{0,l} < x_0$ and $x_0-x_{0,l} < \varepsilon/2$. Let $x_{0,r} \in \mathbb{R}$ such that $x_0 < x_{0,r} < x_0 + \frac{r_2-x_0}{2}$ and $x_{0,r}-x_0 < \varepsilon/2$. Because of the uniform convergence of $p_n$, there is an $N_3>N_2$ such that for all $n>N_3$, $p_n(x_{0,l}) < 0$ and $p_n(r) > 0$ for $r \in [x_{0,r}, x_{r_2,l}]$. Let now $n > \max(2/\varepsilon, N_3) =: N_4$. We then have $E_n(k,1,0)-c-E_n(k,0,0) > 0$ for $\ceil{nx_{0,r}} \leq k \leq t_2^n-1$ and thus $t_2^n \leq \ceil{nx_{0,r}} < \ceil{n(x_0 + \varepsilon/2)} < (x_0 + \varepsilon)n$. Since $p_n(x_{0,l}) < 0$ we must have $t_3^n > \ceil{nx_{0,l}} > nx_{0,l} > (x_0 - \varepsilon)n$. In summary, for all $n > N_4$, we have $(x_0 - \varepsilon)n < t_1^n < (x_0 + \varepsilon)n$. This completes the proof.
\end{proof}

\begin{theorem}
    If $p$ has no roots, then $r_1 = r_2 = \sqrt{c}$.
\end{theorem}
\begin{proof}
    Analog to proof of Theorem~\ref{thm:no_roots}.
\end{proof}

\subsection{The Expected Gain}

We have now shown that all threshold ratios converge, and we have demonstrated how to calculate them. The only thing missing is the limit of the expected gain for the optimal strategy.
It is easy to see that for $1 \leq k < t_1^n$ the expected gain $E_n(k,0,0)$ does not change since all candidates are rejected. Thus, we have
\[E_n(0,0,0) = E_n(t_1^n - 1, 0, 0).\]
We thus have to find the limit of $E_n(t_1^n - 1, 0, 0)$ as $n$ approaches infinity. This is again not too hard, as we already know that $t_1^n/n$ converges to $r_1$ and we have the non-recursive form of $E_n(t_1^n-1,0,0)$ in Lemma~\ref{lemma:closed_form_t1}. In Lemma~\ref{lemma:alot_limits} we already gave the limit of $E_n(t_2^n-1, 1,0)$ and $E_n(t_2^n-1, 0,0)$, which we denote by $E(r_2,1,0)$ and $E(r_2,0,0)$ respectively. Using standard calculus, we easily get the following lemma.

\begin{theorem}
    The limit of $E_n(0,0,0)$ as $n$ goes to infinity is
    \[\frac{r_1}{r_2}E(r_2,0,0) + \frac{r_2-r_1}{r_1} (E(r_2,1,0) - c + \frac{r_2^2}{2}) - \frac{r_1(r_2-r_1)}{2} - c(\ln(\frac{r_2}{r_1}) + \frac{r_1}{r_2} - 1).\]
\end{theorem}

We can now calculate all threshold limits as well as the limit of the expected gain of the optimal strategy for varying $c$.
The limits of the thresholds and the limit of expected gain are plotted in dependence on $c$ in Figure \ref{fig:limits123}. A table of values can be found in Figure~\ref{data:optimal_gain_and_thresholds}.

\begin{figure}[b!]
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{THRESHOLD_LIMITS.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{GAIN_LIMIT.pdf}
    \end{minipage}

    \caption{Left: The limits of the threshold ratios. The reservation cost $c$ is on the $x$-axis, and the threshold ratios are on the $y$-axis. Right: The limit of the expected gain of the optimal strategy of the \textit{Postdoc Problem} in comparison to the \textit{Secretary Problem}. The reservation cost $c$ is on the $x$-axis and the limit of the expected gain is on the $y$-axis.}
    \label{fig:limits123}
\end{figure}