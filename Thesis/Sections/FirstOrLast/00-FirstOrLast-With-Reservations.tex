\chapter{Best or Worst with Reservations}

In this chapter, we show that the methods we used to derive the optimal strategy for the \textit{Postdoc Problem} in the \textit{reservation-per-item} model can be transferred to similar problems like the \textit{Best or Worst Problem} introduced by Bayon et al. \cite{bestworst16}. As we did with the \textit{Postdoc Problem}, we first show that we can compute the optimal strategy using a set of recursive equations. We obtain similar recursive equations for the expected gain of the optimal strategy in the different states as we did in the previous chapter. We use these equations to prove that we again have a threshold strategy given by two thresholds. Then we derive non-recursive solutions for the expected gain for the three different phases. We again utilize the uniform limits of the expected gain to derive the limits of the thresholds as the number of candidates goes to infinity. 

\section{Calculating the Optimal Strategy}

As in the previous section, the expected gain of the optimal strategy after having seen all $n$ candidates, having reserved $r$ of them and not having accepted is

\[ E(n, r, b, w) = \left \{ \begin{array}{cc}
    1 - rc & \text{ if } b=1 \lor w=1 \\
    -rc & \text{ otherwise }
\end{array} \right . , \]
where $b = 1$ if the best candidate is reserved and $w=1$ if the worst candidate is reserved. 
It is easy to see that the probability that a current best-candidate at position $k$ is the overall best candidate is $k/n$. By symmetry the probability that a worst-candidate at $k$ is the overall worst candidate is also $k/n$. Thus, after having seen $k$ of the candidates, having $r$ of them reserved, and not having accepted yet, we have for $b,w \in \{0,1\}$ that the expected gain of the optimal strategy in this state is 
\begin{flalign*}
    E(k, r, b, w) = & \frac{k-1}{k+1} E(k+1,r,b,w)         \\
                 & + \frac{1}{k+1} \max (E(k+1,r,0,w), (k+1)/n -rc, E(k+1, r+1, 1, w)) \\
                 & + \frac{1}{k+1} \max (E(k+1,r,b,0), (k+1)/n -rc, E(k+1, r+1, b, 1)).
\end{flalign*}

For $k = 0$, that is before we have seen any candidate, we have to be a bit more careful since the first candidate is at the same time the best and the worst candidate seen so far. For $k=0$ we get $E(0, 0, 0, 0) = \max (E(1,0,0,0), 2/n, E(1, 1, 1, 1))$.
$E(0,0,0,0)$ is also the expected gain of the optimal strategy, as it is the expected gain before having seen any candidate and having no reservations made. The optimal decision at each state can now again easily be computed by comparing the different expected gains.

Again, similar to the previous chapter, we can drag the past reservation costs out of the recursion and get $E(k,r,b,w) = E(k,b,w) - rc$, where $E(k,b,w)$ is defined by a similar recursion as above.

The probability that the next best-candidate after position $k$ is the best overall candidate will be of great use in this whole chapter. Again by symmetry, this is also the probability that the next worst-candidate after $k$ is the worst candidate overall.

\begin{lemma}
    The probability that the next best-candidate after position $k$ is a winning candidate is 
    \[F(k) := \frac{k}{n} \sum_{k=i}^{n-1} \frac{1}{i}. \]
\end{lemma}
\begin{proof}
    A proof can be found in one of the earliest works on the \textit{Secretary Problem}, back then called the \textit{Marriage Problem} \cite{lindley61}.
\end{proof}

\section{The Classical Case}

As in the \textit{Postdoc} and the \textit{Secretary Problem}, there is also a classical case for this problem. In this section, we show that for reservation costs $c \geq F(k_0)$, which for large $n$ converges to $\ln(2)/2 \approx 0.3466$, the classic strategy of accepting the first best- or worst-candidate in the second half of candidates is optimal.

Throughout this section, we will, again and again, use the following symmetry lemma, which can easily be proven by backward induction.

\begin{lemma}
    \label{lemma:bow_e_sym}
     For $1 \leq k \leq n$ we have that $E(k,1,0) = E(k,0,1)$.
\end{lemma}

Let us now prove that as long as we have a best(worst)-candidate reserved, we will always reject a worst(best)-candidate. This is true for all reservation costs, not only in the classical case.
\begin{theorem}
    \label{thm:bow_reject_if_other_reserved}
    For $1 \leq k \leq n$  we have:
    \begin{itemize}
        \item[(i)] Rejecting best-candidates at position $k$ is optimal when the current worst-candidate is reserved.
        \item[(ii)] Rejecting worst-candidates at position $k$ is optimal when the current best-candidate is reserved.
    \end{itemize}
\end{theorem}
\begin{proof}
    Prove by backward induction. The base case is $k = n$. This means that the current candidate is the last candidate and we win in both cases with rejecting since we have the other winning candidate reserved. So let the theorem be true for some $1 < k+1 \leq n$.
    
    For the expected gain this means:
    \begin{equation}
        \max (E(k+1, 0, 1), (k+1) / n, E(k+1,1,1) - c) = E(k+1,0,1)
    \end{equation}
    \begin{equation}
        \max (E(k+1, 1, 0), (k+1) / n, E(k+1,1,1) - c) = E(k+1,1,0)
    \end{equation}

    We now proof both cases:

    \begin{itemize}
        \item[(i)]
        $\begin{aligned}[t]
            E(k,0,1) & \geq \frac{k-1}{k+1} E(k+1,0,1) + \frac{E(k+1, 0, 1)}{k+1} + \frac{E(k+1,0,1)}{k+1}       \\
                     & \geq \frac{k-1}{k+1} (E(k+1,1,1) - c) + \frac{E(k+1, 0, 1)}{k+1} + \frac{E(k+1,0,1)}{k+1} \\
                     & \geq \frac{k-1}{k+1} E(k+1,1,1) + \frac{E(k+1, 0, 1)}{k+1} + \frac{E(k+1,1,0)}{k+1} - c       \\
                     & = E(k,1,1) - c
        \end{aligned}$ 
    
        It follows that rejecting best-candidates at $k$ is better than reserving when the current worst-candidate is reserved. Rejecting best-candidates is also always at least as good as accepting when the worst so far is reserved since the probability that the best-candidate is a winning candidate is the same as the probability that the already reserved worst candidate is a winning candidate.
        \item[(ii)] $E(k,1,0) \geq E(k,1,1)-c$ follows by Lemma~\ref{lemma:bow_e_sym}: $E(k,0,1) = E(k,1,0)$. That rejecting is at least as good as accepting is for the same reason as above.
    \end{itemize}
    This completes the proof.
\end{proof}

Let us now give non-recursive solutions for the expected gain. Since we just showed that the optimal strategy will never be in a state where both the current best- and worst-candidate is reserved, we only need the expected gain in all other states of reservation.

\begin{lemma}
    For $c \geq F(k_0)$ and $1 \leq k \leq n$, we have: 
    \begin{itemize}
        \item[(1)] $E(k,0,0) = \left \{ \begin{array}{ll}
            2f_k & \textit{ for } k \geq k_0 \\
            2f_{k_0} & \textit{ otherwise } 
        \end{array} \right .$
        \item[(2)] $E(k,1,0) = E(k,0,1) = \left \{ \begin{array}{ll}
            \frac{k}{n} + F(k) & \textit{ for } k \geq k_0 \\
            \frac{k}{n} +  \frac{kF(k_0) + 2 f_{k_0} (k_0-k)}{k_0} & \textit{ otherwise }
        \end{array} \right .$
    \end{itemize}
\end{lemma}

\begin{proof}
    Proof by backward induction. The base case $k = n$ is trivial. So let the lemma be true for some $1 < k+1 \leq n$.

    Case 1: $k \geq k_0$. By the induction hypothesis we have $E(k+1,0,0) = 2f_{k+1}$ and it is easy to verify that $2f_{k+1} \leq (k+1)/n$. Also we have $c \geq F(k_0) \geq F(k+1)$ and thus get
    \begin{equation}
        \label{eq:max1223}
         \max \{ E(k+1, 0, 0), (k+1)/n, E(k+1,1,0)-c  \} = (k+1)/n.
    \end{equation}
    Using $E(k,0,1) = E(k,1,0)$ (Lemma~\ref{lemma:bow_e_sym}) it directly follows that
    \begin{equation*}
        \label{eq:max12234}
        \max \{ E(k+1, 0, 0), (k+1)/n, E(k+1,0,1)-c  \} = (k+1)/n.
    \end{equation*}
    Using the recursive definition of the expected gain, we finally get
    \[ E(k,0,0) = \frac{k-1}{k+1} 2f_{k+1} + \frac{2}{k+1} \frac{k+1}{n} = 2f_k.\]
    This completes (1). Using Theorem \ref{thm:bow_reject_if_other_reserved} we have that
    \[ \max \{ E(k+1, 1, 0), (k+1)/n, E(k+1, 1, 1) - c \} = E(k+1, 1, 0). \]
    Using (\ref{eq:max1223}) we get that
    \[ E(k,1,0) = \frac{k}{k+1} \left (\frac{k+1}{n} + \frac{k+1}{n} \sum_{i=k+1}^{n-1}\frac{1}{i} \right ) + \frac{1}{k+1}\frac{k+1}{n} = \frac{k}{n} + \frac{k}{n} \sum_{i=k}^{n-1}\frac{1}{i} = F(k).  \]
    This completes (2).

    Case 2: $k < k_0$. First, note that by the induction hypothesis, we have the following not only for $k+1<k_0$ but also for $k+1=k_0$:
    \[E (k+1,0,0) = 2f_{k_0} \; \textit{ and } \; E(k+1,1,0) = \frac{k}{n} +  \frac{kF(k_0) + 2 f(k_0) (k_0-k)}{k_0} \]

    Since $E(k, 1, 0)$ is linear in $k$ and increasing and $E(k_0, 1, 0) - c \leq k_0/n$ we also have $E(k,1,0)-c \leq k_0/n$. It is also easy to verify that $k_0/n \leq 2f_{k_0}$ and thus
    \begin{equation}
        \label{eq:somasdasdasd}
        \max \{ E(k+1,0,0), (k+1)/n, E(k,1,0) - c \} = 2f_{k_0}.
    \end{equation}
    With $E(k+1,1,0) = E(k+1,0,1)$ also
    \[ \max \{ E(k+1,0,0), (k+1)/n, E(k,0,1) - c \} = 2f_{k_0}. \]
    Using the recursive definition of the expected gain, we conclude that
    \( E(k,0,0) = E(k+1,0,0) = 2f_{k_0}.\) 
    This completes (1). For (2) we use Theorem~\ref{thm:bow_reject_if_other_reserved} and (\ref{eq:somasdasdasd}) and get
    \begin{flalign*}
        E(k,1,0) &= \frac{k}{k+1} E(k+1,1,0) + \frac{1}{k+1} E(k+1,0,0) = \frac{k}{n} +  \frac{kF(k_0) + 2 f(k_0) (k_0-k)}{k_0}.
    \end{flalign*}
    This completes (2), the second case and therefore the proof.
\end{proof}

In the proof of the above lemma, we have shown that for $1 \leq k \leq k_0$, we have
\[ \frac{k}{n} + \frac{kF(k_0) + 2f(k_0)(k_0-k)}{k_0} \leq \frac{k_0}{n} + \frac{k_0F(k_0) + 2f(k_0)(k_0-k_0)}{k_0} \leq \frac{k_0}{n} + F(k_0) \leq \frac{k_0}{n} + c. \]
We thus have for $1\leq k \leq k_0$ that $E(k,1,0)-c \leq \frac{k_0}{n} \leq 2f_{k_0} = E(k,0,0)$ and $E(k,0,0) = 2f_{k_0} \geq \frac{k}{n}$. Hence, when we have nothing reserved, rejecting best-candidates is optimal and by symmetry also rejecting worst-candidates at $1\leq k \leq k_0$.

For $k > k_0$ we have $k/n \geq 2f_k$ and because $c \geq F(k_0)$ and $F(k) < F(k_0)$ we get that $E(k,1,0)-c \leq k/n$ and $E(k,0,0) \leq k/n$. Thus when we have nothing reserved, accepting best-candidates is optimal at $k_0 <k \leq n$. By symmetry, the same applies to worst-candidates. Since we start with no reservations and there is no position that we will make a reservation when we have no reservations yet, the optimal strategy does never reserve. We summarize our findings in the main theorem of this section.

\begin{theorem}[Optimal Strategy]
    For $c \geq F(k_0) $ it is optimal to
    \begin{itemize}
        \item[(1)] Accept best- and worst-candidates at $k > k_0$.
        \item[(2)] Reject best- and worst-candidates at $k \leq k_0$.
    \end{itemize}
\end{theorem}

The limit of $F(k_0)$ is obtained using standard calculus and shows to be $\ln(\sqrt{2}) \approx 0.3466$.

\section{Structure of the Optimal Strategy}

In this section, we investigate the case $c < F(k_0)$. We show that the optimal strategy is again a threshold strategy characterized by two thresholds $t_1$ and $t_2$. We have already shown in Lemma~\ref{thm:bow_reject_if_other_reserved} that regardless of the reservation cost, rejecting best-candidates is optimal when the current worst-candidate is reserved and vice versa. Thus, our optimal strategy will never end up having the best- and worst-candidate reserved at the same time. The two missing cases are that we have no reservations and see a new best- or worst-candidate, and that we have reserved the current best- or worst-candidate and see a new best- or worst-candidate.

Let us start with the probability of winning by accepting the next best- or worst-candidate.

\begin{lemma}
    \label{lemma:bow_win_with_next}
    The probability that the next best- or worst-candidate after position $k$, $1 \leq k \leq n$, is a winning candidate is $2f_k$.
\end{lemma}

\begin{proof}
    The recursive equation describing the probability is given by
    \[ G(n) = 0, \; G(k) = \frac{k-1}{k+1} G(k+1) + \frac{2}{k+1} \frac{k+1}{n} = \frac{k-1}{k+1} G(k+1) + \frac{2}{n},\]

    where $1 \leq k < n$. It is easy to verify that indeed $2f_k = \frac{2k(n-k)}{n(n-1)}$ is a closed-form solution for the above equation.
\end{proof}

\begin{lemma}
    \label{lemma:bow_optimal_accept}
    Accepting best(worst)-candidates is optimal when the current worst(best)-candidate is not reserved and \[k \geq t_2 = \max \{ k \in \N \mid F(k) > c \} + 1.\]
\end{lemma}
\begin{proof}
    Prove by backward induction. The base case $k=n$ is trivial since accepting is a guaranteed win. So let $k \geq \max \{ k \in \N \mid F(k) > c \} + 1$ and let the lemma be true for all $k'$, $k < k' \leq n$.  We now show that accepting best-candidates at $k$ is optimal when the current worst-candidate is not reserved. The other part of the lemma then follows again through symmetry.

    If we reject the best-candidate at $k$ we end with no reservations and know from the induction hypothesis that we will accept the next best- or worst-candidate. Thus, by Lemma~\ref{lemma:bow_win_with_next} we then have $E(k,0,0) = 2f_k$. Note that $k > k_0$ since $c < F(k_0)$ and therefore $E(k,0,0) = 2f_k \leq k/n$, which means accepting would have been at least as good as rejecting. 

    If we reserve the best-candidate at $k$ we will, by the induction hypothesis, accept the next best-candidate since we will reject all worst-candidates because we have the best-candidate reserved (Lemma~\ref{thm:bow_reject_if_other_reserved}). Hence, we win with probability $k/n + F(k)$, that is the freshly reserved candidate is the overall best or the next best-candidate is the overall best. Since $F(k) \leq c$, accepting would have been at least as good as reserving. Thus, accepting is optimal. This completes the proof of the theorem.
\end{proof}

\begin{lemma}
    \label{lemma:bow_better_than_accepting1}
    For $1 \leq k \leq k_0$, rejecting candidates at $k$ is at least as good as accepting.
\end{lemma}
\begin{proof}
    We know from the previous section that the probability of winning with the first best- or worst-candidate after $k_0$ is $2f_{k_0}$.
    Since accepting a best- or worst-candidate at $k$ wins with probability $k/n$ and for $1 \leq k \leq k_0$, $k/n \leq 2f_{k_0}$. The lemma follows.
\end{proof}

\begin{lemma}
    \label{lemma:bow_better_than_accepting2}
    For $k_0 < k < t_2$, reserving best(worst)-candidates at $k$ is at least as good as accepting.
\end{lemma}
\begin{proof}
    It is easy to verify that for $k > k_0$, we have that $F(k) > F(k+1)$. Since $F(t_2-1) > c$, we have that for $k_0 < k < t_2$, $F(k) > c$. Accepting a best(worst)-candidate at position $k$ wins with probability $k/n$. Reserving and accepting the next best(worst)-candidate wins with $k/n + F(k)$. Since $F(k) > c$, the lemma follows.
\end{proof}

\begin{lemma}
    \label{lemma:bow_reject_once_reject_ever}
    If rejecting best(worst)-candidates is optimal at some $k+1$, $1 < k+1 \leq n$, it is also at $k$.
\end{lemma}
\begin{proof}
    Let $1 < k+1 \leq n$. We only prove the best-candidates part of the lemma. The second part follows by symmetry.
    So let rejecting best-candidates at $k+1$ be optimal. If the current worst-candidate is reserved, rejecting the best-candidate at $k$ is optimal (Theorem~\ref{thm:bow_reject_if_other_reserved}) and the proof is complete. So assume that the current worst-candidate is not reserved. Since rejecting best-candidates at $k+1$ is optimal, we have that
    \[ E(k+1,0,0) \geq E(k+1,1,0) - c. \] 
    We then get for $k$ that
    \begin{flalign*}
        E(k,0,0) & \geq \frac{k-1}{k+1} E(k+1,0,0) + \frac{1}{k+1} E(k+1,0,0) + \frac{1}{k+1} E(k+1,0,0)\\
                 & \geq \frac{k}{k+1} (E(k+1,1,0) - c) + \frac{1}{k+1} E(k+1,0,0)\\
                 & \geq \frac{k-1}{k+1} E(k+1,1,0) + \frac{1}{k+1} E(k+1,1,0) + \frac{1}{k+1} E(k+1,0,0) - c \\
                 & = E(k,1,0) - c.
    \end{flalign*}
    Since $k/n < (k+1)/n$ and $E(k,0,0) \geq E(k+1,0,0)$, we also have $E(k,0,0) > k/n$.
    This completes the proof.
\end{proof}

\begin{theorem}[Optimal Strategy]
    There are $0 \leq t_1 \leq t_2 \leq n$, such that the optimal strategy is of the form shown in Figure~\ref{fig:bow_opt_strat_struc}. $t_1$ is the first position where candidates are reserved and $t_2$ where candidates are accepted. 
\end{theorem}
\begin{figure}
    \includegraphics{BOW_OPTIMAL_STRATEGY_STRUCTURE.pdf}
    \caption{Structure of the optimal strategy for the \textit{Best or Worst Problem} in the \textit{reservation-per-item} model for $c<F(k_0)$.}
    \label{fig:bow_opt_strat_struc}
\end{figure}
\begin{proof}
    That rejecting best(worst)-candidates is optimal when the current worst(best)-candidate is reserved has been shown in Theorem~\ref{thm:bow_reject_if_other_reserved}. Accepting best(worst)-candidates, when the current worst(best)-candidate is not reserved, starting from $t_2 = \max \{ k \in \N \mid F(k) > c \} + 1$ has been shown in Lemma~\ref{lemma:bow_optimal_accept}. Lemma~\ref{lemma:bow_better_than_accepting1} and Lemma~\ref{lemma:bow_better_than_accepting2} show that for $k<t_2$ accepting is never the only optimal action. Thus reserving or rejecting is optimal. In Lemma~\ref{lemma:bow_reject_once_reject_ever} we have shown that once rejecting becomes optimal it will stay optimal. So let $t_1$ be the first position where reserving is optimal.
\end{proof}

\section{Non-Recursive Forms of the Expected Gain}

As in the chapter about the \textit{Postdoc Problem}, we will be able to give non-recursive equations for the expected gain in the different states. We again can use these equations to derive the limits of the thresholds $t_1$ and $t_2$.

We begin with deriving a non-recursive equation for the expected gain for $k \geq t_2-1$. We know that for $k+1 \geq t_2$ it is optimal to accept best-candidates when the current worst-candidate is not reserved. This implies that
\[\max (E(k+1,0,0), (k+1)/n, E(k+1,1,0)-c) = (k+1)/n.\]
The same applies to worst-candidates and we have
\[\max (E(k+1,0,0), (k+1)/n, E(k+1,0,1)-c) = (k+1)/n.\]
Thus, we get that 
\[ E(k,0,0) = \frac{k-1}{k+1} E(k+1,0,0) + \frac{2}{n}. \]
We have not yet solved a recursion of this form. But it turns out to be not far more difficult than before. Let $G(k) := E(k,0,0)/(k(k-1))$. Then
\[ G(k) = \frac{E(k+1,0,0)}{k(k+1)} + \frac{2}{nk(k-1)} = G(k+1) + \frac{2}{nk(k-1)}. \]
We know that $G(n) = 0$ and the recursive equations holds for all $t_2 -1\leq k \leq$. Thus, we have for $t_2 -1 \leq k \leq n$ that
\[ G(k) = \sum_{i=k}^{n-1}\frac{2}{nk(k-1)} = \frac{2}{n} \left (\frac{n-2}{n-1} - \frac{k-2}{k-1} \right ) = 2 \frac{n-k}{n(n-1)(k-1)}, \]
where the second equality follows using $\sum_{i=1}^n 1/(i(i+1)) = n/(n+1)$.
By the definition of $G$ we then have that \[E(k,0,0) = k(k-1)F(k) = 2 \frac{k(n-k)}{n(n-1)} = 2f_k. \]
This result is no surprise as we have already shown it in Lemma~\ref{lemma:bow_win_with_next}. Nonetheless, the technique will be valuable as we derive more complex equations.

For the case where we have the current worst candidate reserved, we know that for $k+1 \geq t_2$ rejecting best-candidates and accepting worst-candidates is optimal. We thus get that
\[ E(k,1,0) = \frac{k}{k+1} E(k+1,1,0) + \frac{1}{n}.\]
Solving this equation is similar to the equations in the previous chapter, and we get that $E(k,1,0) = k/n + F(k)$. The following lemma summarizes the above.

\begin{lemma}
    For $t_2-1 \leq k \leq n$ we have:
    \begin{itemize}
        \item[(i)] $E(k,0,0) = 2f_k$
        \item[(ii)] $E(k,1,0) = E(k,0,1) = \frac{k}{n} + F(k)$
    \end{itemize}
\end{lemma}

We can do the same for $t_1-1 \leq k < t_2-1$ and get similar recursive equations which we then solve using the methods introduced. After some rearrangements, we get the following lemma.

\begin{lemma}
    \label{lemma:bow_t1}
    For $t_1-1 \leq k \leq t_2-1$ we have:
    \begin{itemize}
        \item[(i)] $E(k,0,0) = \frac{2f_{t_2-1} k(k-1)}{(t_2-1)(t_2-2)} + 2k(k-1) \sum\limits_{i=k}^{t_2-2}\frac{E(i+1,1,0)-c}{(i-1)i(i+1)}$
        \item[(ii)] $E(k,1,0) = E(k,0,1) = E(t_2-1, 1,0) - c(H_{t_2-1} - H_{k})$
    \end{itemize}
\end{lemma}

\section{Extending into Infinity}
We again investigate the limits of the thresholds as the number of candidates goes to infinity. Since the number of candidates $n$ was implicitly given until now, we will now make the dependence more clear. We will denote the expected gain as $E_n$ instead of $E$. The probability $F(k)$ will from now on be called $F_n(k)$. The thresholds $t_1$ and $t_2$ will be referred to as $t_1^n$ and $t_2^n$. Formally, we now investigate $r_1 = \lim_{n \to \infty}t_1^n/n$ and $r_2 = \lim_{n \to \infty} t_2^n/n$.

Let us start with the limit $r_2$. By definition, we have $t_2^n = \max \{ k \in \N \mid F(k) > c\} + 1$. Thus, we can use Theorem~\ref{theorem:limit_theorem} from the \textit{Postdoc} chapter again. For $\lim_{n \to \infty} F_n(\ceil{rn})$ we get the following lemma using a similar technique as in the proof of Lemma~\ref{lemma:limit_f_plus_h}.

\begin{lemma}
    For $0 < r < 1$ we have
    \[ \lim_{n \to \infty} F_n(\ceil{rn})-c = \lim_{n \to \infty} \frac{\ceil{rn}}{n} \sum_{i=\ceil{rn}}^{n-1} \frac{1}{i} = -r\ln(r)-c. \]
\end{lemma}

It is easy to verify that for $c < \ln(\sqrt{2})$, $p(x) = -x\ln(x)-c$ has two roots in $(0,1)$, and the second root $x_1$ has negative slope.
Since $0<x_1<1$ we can rewrite $-x_1\ln(x_1) - c = -r'e^{r'} - c$ with $r' = \ln(x_1)$. We can then express the second root in terms of the \textit{Lambert W function} and get that $r' = \mathcal{W}_0(-c)$ and thus $x_1 = e^{\mathcal{W}_0(-c)}$. Using Theorem~\ref{theorem:limit_theorem} we then get the following theorem.

\begin{theorem}
    For $c < \ln(2)/2$ we have $r_2 = e^{\mathcal{W}_0(-c)}$.
\end{theorem}

For the limit $r_1$ we have to do some more work again. We now derive the uniform limit of the non-recursive equation of the expected gain between $t_1^n-1$ and $t_2^n-1$. We use this uniform limit to derive the limit of the threshold $t_1^n$. So let
\[ p_1^n(k) = \frac{2f_{t_2-1} k(k-1)}{(t_2^n-1)(t_2^n-2)} + 2k(k-1) \sum\limits_{i=k}^{t_2-2}\frac{E(i+1,1,0)-c}{(i-1)i(i+1)}, \]
\[ p_2^n(k) = E(t_2-1, 1,0) - c(H_{t_2-1} - H_{k}). \]

These are just the non-recursive solutions of the expected gain between $t_1^n-1$ and $t_2^n-1$. Let $p_n: (0,1] \mapsto \R, \; p_n(r) = p_2^n(\ceil{rn}) - c - p_1^n(\ceil{rn})$. Similar to the proof of Lemma~\ref{lemma:uniform_limit_p} we can derive the following lemma.

\begin{lemma}
    For $a \in (0,1]$ we have that $p_n \overset{n}{\rightrightarrows} p$ on $[a,1]$ with
    \[ p(r) = r^2(2 + \frac{c}{2r_2^2} - \frac{1}{r_2}) - \frac{c}{2}. \]
\end{lemma}

Note that since $r_2 \geq 1/2$, $p$ is an upwards-opening parabola with two roots. 
Similar to Theorem~\ref{thm:has_roots} and Theorem~\ref{thm:no_roots}, we can prove the following theorem. 

\begin{theorem}
    For $c<\ln(2)/2$ we have that $r_1$ is the second root of $p$ and thus
   \[ r_1 = \sqrt{\frac{r_2^2c}{4r_2^2 - 2r_2 + c}}. \]
\end{theorem}

Since we know that before $t_1^n$, rejecting all candidates is optimal, we have that $E_n(0,0,0) = E_(t_1^n-1,0,0)$.
The limit of $E(t_1^n-1,0,0)$ can be obtained using the non-recursive equation from Lemma~\ref{lemma:bow_t1}.
\begin{theorem}
    For $c<\ln(2)/2$ we have for $n \to \infty$ that
    \[ gain_{OPT} = r_2 -r_2\ln(r_2)-c\ln(r_2/r_1)-c. \]
\end{theorem}

The limit of the expected gain of the optimal strategy and the limits of the threshold ratios are plotted in Figure~\ref{fig:bow_limits}.

\begin{figure}[b!]
    \centering
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{BOW_THRESHOLD_LIMITS.pdf}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{BOW_GAIN_LIMIT.pdf}
    \end{minipage}

    \caption{Left: The limits of the threshold ratios. The reservation cost $c$ is on the $x$-axis, and the threshold ratios are on the $y$-axis. Right: The limit of the expected gain of the optimal strategy. The reservation cost $c$ is on the $x$-axis, and the expected gain is on the $y$-axis.}
    \label{fig:bow_limits}
\end{figure}