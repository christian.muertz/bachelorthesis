pushd OptimalStrategySlides
manim -qk SecretaryOptimalStrategy.py 
manim -qk SecretaryReservationsOptimalStrategy.py
manim -qk PostdocOptimalStrategy.py
manim -qk PostdocReservationsOptimalStrategy.py

popd
pushd StickmanSlides
manim -qk StickmanSlides.py IntroductionSecretary &
manim -qk StickmanSlides.py IntroductionSecretaryReservations &

wait