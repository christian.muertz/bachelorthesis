from manim import *

from thesis.PostDoc.calculations import *
from thesis.constants import *

import os

import sys
sys.path.append("..")
from Utils import *

ASSETS_PATH = os.path.join(os.path.dirname(__file__), "..", "assets")

class StickmanBaseScene(BaseScene):
    # PARAMETERS
    n = 5
    c = 0.2

    interviewer = None

    reservation = False
    candidates = []
    rejected = []

    balance = None
    balance_decimal = None

    def setup_slide(self, quick=False, reservation=False, secretary=True):
        super().setup_slide()

        self.reservation = reservation

        if not quick:
            self.wait()
            self.next_section()

        self.interviewer = Interviewer()
        self.interviewer.move_to(self.top_y + 5.8*LEFT)
        if quick:
            self.add(self.interviewer)
        else:
            self.play(FadeIn(self.interviewer))

        if not quick:
            self.wait()
            self.next_section()

        for i in range(self.n):
            s = Candidate(i+1, winner=(i == 0) if secretary else (i==1))
            s.scale(0.9)
            s.move_to(self.top_pos(i))
            self.candidates.append(s)

        if quick:
            self.add(*self.candidates)
        else:
            self.play(*[FadeIn(c) for c in self.candidates])


        rejected_rects = [Rectangle(BLACK, 2.8, 2).move_to(self.table_pos(i)) for i in range(self.n)]
        
        if quick:
            self.add(*rejected_rects)
        else:
            self.play(*[FadeIn(r) for r in rejected_rects])

        if not quick:
            self.wait()
            self.next_section()

        self.shuffle_candidates()
        if not quick:
            self.play(*self.position_candidates())
        else:
            self.position_candidates(animate=False)
        
        if not quick:
            self.wait()
            self.next_section()

        if not quick:
            self.play(*[c.dim_quality() for c in self.candidates])
        else:
            for c in self.candidates:
                c.dim_quality(animate=False)
        
        self.balance = Group()
        self.balance_decimal = DecimalNumber(1.0, num_decimal_places=1, color=BLACK)
        self.balance_decimal.set_value(0)
        self.balance.add(ImageMobject('../assets/coins.jpg').scale(0.2))
        self.balance.add(self.balance_decimal)
        self.balance.arrange(RIGHT)

        self.balance.next_to(self.interviewer, DOWN)
        if not reservation:
            self.balance.set_opacity(0.0)
            self.balance_decimal.set_opacity(0.0)

        if reservation:
            self.wait()
            self.next_section()
            self.play(FadeIn(self.balance))

        self.wait()
        self.next_section()

    
    top_y = 1.7 * UP

    def top_pos(self, pos):
        return (pos - (self.n-1)/2) * 10*RIGHT/self.n + self.top_y + 1.5*RIGHT
    
    def table_pos(self, pos):
        return (pos - (self.n-1)/2) * 10*RIGHT/self.n + 1.5*DOWN + 1 * RIGHT

    def shuffle_candidates(self, order=[4,5,2,3,1]):
        new_candidates = [None for i in range(len(order))]
        for c in self.candidates:
            new_candidates[order.index(c.quality)] = c
        self.candidates = new_candidates
    
    def position_candidates(self, animate=True):
        animations = []

        # Reorder rejected
        for x in self.rejected:
            rank = len([y for y in self.rejected if y.quality < x.quality])
            if animate:
                animations.append(x.animate.move_to(self.table_pos(rank)))
            else:
                x.move_to(self.table_pos(rank))

        # Reorder pending
        for i, x in enumerate([x for x in self.candidates if x not in self.rejected]):
            if animate:
                animations.append(x.animate.move_to(self.top_pos(i)))
            else:
                x.move_to(self.top_pos(i))

        return animations

    def start_interview(self, i):
        interviewee_pos = self.top_y + 3.5*LEFT

        self.play(
            *self.interviewer.say(None),
            self.candidates[i].animate.move_to(interviewee_pos),
        )

    def reject(self, i):
        self.rejected.append(self.candidates[i])
    
        self.play(*self.interviewer.say("REJECT"))
        self.next_section()
        
        self.play(
            *self.interviewer.say(None),
            *self.candidates[i].reject(),
            *self.position_candidates()
        )

    def reserve(self, i):
        self.rejected.append(self.candidates[i])
    
        self.play(*self.interviewer.say("RESERVE"))
        self.next_section()
        
        self.play(
            self.balance_decimal.animate.set_value(self.balance_decimal.get_value()-self.c),
            *self.interviewer.say(None),
            *self.candidates[i].reserve(),
            *self.position_candidates()
        )

    result_rect = None
    result_text = None

    def accept(self, i):
        self.play(*self.interviewer.say("ACCEPT"))
        self.next_section()

        rect_width = 5
        rect_height = 2 

        b = self.balance_decimal.get_value()

        if self.reservation:
            if self.candidates[i].quality == 1: # WIN
                self.result_rect = Rectangle(GREEN, rect_height, rect_width, fill_color=GREEN, fill_opacity=1)
                self.result_text = Tex(f"WIN: 1-{abs(b)} = {1+b}", color=WHITE)
            else: 
                self.result_rect = Rectangle(RED, rect_height, rect_width,  fill_color=RED, fill_opacity=1)
                self.result_text = Tex(f"LOSE: 0-{abs(b)} = {b}", color=WHITE)

        else:
            if self.candidates[i].quality == 1: # WIN
                self.result_rect = Rectangle(GREEN, rect_height, rect_width,  fill_color=GREEN, fill_opacity=1)
                self.result_text = Tex("WIN", color=WHITE)
            else: 
                self.result_rect = Rectangle(RED, rect_height, rect_width,  fill_color=RED, fill_opacity=1)
                self.result_text = Tex("LOSE", color=WHITE)

        self.play(FadeIn(self.result_rect), FadeIn(self.result_text))

    def clear_result(self):
        return [
            FadeOut(self.result_rect),
            FadeOut(self.result_text),
        ]
    
    def reset(self):
        self.rejected = []
        self.play(
            *self.clear_result(),
            *[a for c in self.candidates for a in c.clear_status()],
            *self.position_candidates(),
            self.balance_decimal.animate.set_value(0.0)
        )

class IntroductionSecretary(StickmanBaseScene):
     def construct(self):
        super().__init__()
        
        self.setup_slide()

        # FIRST ROUND
        self.start_interview(0)
        self.reject(0)
        self.next_section()

        self.start_interview(1)
        self.reject(1)
        self.next_section()

        self.start_interview(2)
        self.accept(2)
        self.next_section()

        # SECOND ROUND
        self.shuffle_candidates(order=[4,1,2,5,3])
        self.reset()

        self.start_interview(0)
        self.reject(0)
        self.next_section()

        self.start_interview(1)
        self.accept(1)
        self.next_section()


class IntroductionSecretaryReservations(StickmanBaseScene):
     def construct(self):
        super().__init__()

        self.setup_slide(quick=True, reservation=True)

        # FIRST ROUND
        self.start_interview(0)
        self.reserve(0)

        self.start_interview(1)
        self.reject(1)

        self.start_interview(2)
        self.accept(2)
        self.next_section()

        # SECOND ROUND
        self.shuffle_candidates(order=[4,1,2,5,3])
        self.reset()

        self.start_interview(0)
        self.reserve(0)

        self.start_interview(1)
        self.accept(1)


class IntroductionPostdocProblem(StickmanBaseScene):
    def construct(self):
        self.setup_slide(quick=True, reservation=False)

        self.next_section()
        
        self.play(
            self.candidates[0].become_winner(True),
            self.candidates[1].become_winner(False)
        )


class IntroductionPostdocReservationsProblem(StickmanBaseScene):
    def construct(self):
        self.setup_slide(quick=True, reservation=True, secretary=False)
        


class Interviewer(Group):
    image = None
    bubble = None
    text = None

    def __init__(self):
        super().__init__()

        self.image = ImageMobject(ASSETS_PATH + '/interviewer.png')
        self.image.height = 2
        self.add(self.image)

        self.bubble = ImageMobject(ASSETS_PATH + '/bubble.png')
        self.bubble.height = 1
        self.bubble.shift(1.5 * UP + 1 * RIGHT)
        self.add(self.bubble)

    def say(self, msg):
        if msg is None:
            if self.text is None:
                return []

            animation = FadeOut(self.text)
            self.text = None
            return [animation]
        
        new_text = Tex("\\textbf{" + msg + "}", font_size=30).move_to(self.bubble.get_center() + 0.18 * UP)

        if self.text:
            animation = ReplacementTransform(self.text, new_text)
        else:
            animation = FadeIn(new_text)

        self.text = new_text

        return [animation]


class Candidate(Group):
    status_banner = None

    def __init__(self, quality, winner):
        super().__init__()

        self.quality = quality
        
        if winner:
            self.image = ImageMobject(ASSETS_PATH + '/stickman_winner.png')
        else:
            self.image = ImageMobject(ASSETS_PATH + '/stickman.png')
        self.image.height = 2
        self.add(self.image)

        self.quality_text = Text(f"{quality}", color=BLACK).move_to(1.5*DOWN)
        self.add(self.quality_text)

    def become_winner(self, winner=True):  
        new_image = None
        if winner:
            new_image = ImageMobject(ASSETS_PATH + '/stickman_winner.png')
        else:
            new_image = ImageMobject(ASSETS_PATH + '/stickman.png')

        new_image.height = self.image.height
        new_image.move_to(self.image)
        return ReplacementTransform(self.image, new_image)

    def reject(self):
        self.status_banner = VGroup()
        self.status_banner.add(Rectangle(RED, .5, 1.5, fill_opacity=1))
        self.status_banner.add(Tex("REJECTED",color=WHITE, font_size=20))
        self.status_banner.move_to(self.image.get_center())
        self.status_banner.rotate(math.pi/4)
        self.add(self.status_banner)
        return [FadeIn(self.status_banner)]
    
    def reserve(self):
        self.status_banner = VGroup()
        self.status_banner.add(Rectangle(ORANGE, .5, 1.5, fill_opacity=1))
        self.status_banner.add(Tex("RESERVED",color=WHITE, font_size=20))
        self.status_banner.move_to(self.image.get_center())
        self.status_banner.rotate(math.pi/4)
        self.add(self.status_banner)
        return [FadeIn(self.status_banner)]
    
    def clear_status(self):
        self.remove(self.status_banner)
        animation = FadeOut(self.status_banner)
        self.status_banner = None
        return [animation]

    def dim_quality(self, animate=True):
        if animate:
            return self.quality_text.animate.set_opacity(0.5)
        else:
            return self.quality_text.set_opacity(0.5)

