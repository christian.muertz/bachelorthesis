from thesis.PostDoc.calculations import *

from manim import *
from manim_slides import Slide
from thesis.constants import *

class Theorem(VGroup):
    def __init__(self, title, body):
        super().__init__()

        t = Tex(body)
        box = RoundedRectangle(
            [0,0.2,0.2,0],
            color=RED, height=t.height + 0.5,
            width=config['frame_width'] - 3.5, 
            stroke_opacity = 0,
            fill_opacity=1,
            fill_color=LIGHTER_GRAY
        )


        header = Rectangle(fill_color=RWTH_DEEP_BLUE_RGB, stroke_width=0, fill_opacity=1, width=box.width, height=.75).next_to(box, UP, buff=0)
        header.round_corners([0.2,0,0,0.2])

        header_text = Tex(title, color=WHITE).move_to(header, aligned_edge=LEFT).shift(0.25*RIGHT)

        self.add(box, t, header, header_text)

class BaseScene(Scene):
    FONT_SIZE = 39

    def setup_slide(self):
        self.camera.background_color = WHITE
        Text.set_default(color=RED) # DO use Tex not Text

        template = TexTemplate(
            preamble = r"""
            \usepackage[english]{babel}
            \usepackage{amsmath}
            \usepackage{amssymb}
            \renewcommand*\rmdefault{cmss}
            """)
        Tex.set_default(color=BLACK, font_size=self.FONT_SIZE, tex_template=template)