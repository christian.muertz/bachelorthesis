from manim import *
from thesis.constants import *

import sys
sys.path.append("..")
from Utils import *
from OptimalStrategySlides import *

DPI = 1200
WIDTH = 5.6704 #in
HEIGHT = 2.8 #in

config.pixel_height = int(HEIGHT*DPI)
config.pixel_width = int(WIDTH*DPI)

config.frame_height = 8.0
config.frame_width = 8.0 * config.pixel_width / config.pixel_height

class SecretaryOptimalStrategy(BaseScene):
    def construct(self):
        super().__init__()
        self.setup_slide(title="SECRETARY PROBLEM")

        n = ValueTracker(5)
        c = ValueTracker(1)

        self.viz = SecretaryStrategyVisualizer(round(n.get_value()), c.get_value())
        viz_updater = lambda x: x.set_nc(round(n.get_value()), c.get_value())
        self.viz.add_updater(viz_updater)
        self.add(self.viz)

        text_n = always_redraw(lambda: Tex(f"$n = {round(n.get_value())}$", font_size=self.FONT_SIZE).shift(3 * UP))
        self.add(text_n)

        self.wait()
        self.next_section()

        self.play(n.animate.set_value(100), run_time=5)
        self.wait()
        self.next_section()

        self.viz.remove_updater(viz_updater)

        # Infinity
        self.remove(text_n)
        self.viz.set_nc(math.inf, c.get_value())
        self.wait()
        self.next_section()

        # Theorem
        self.theorem = Theorem("Optimal Strategy $n \\to \infty$", "Accept the first 1-candidate after the n/e-th candidate.")
        self.theorem.shift(2.8*DOWN)
        self.play(self.viz.tight_and_shift(.7*UP), FadeIn(self.theorem))

        self.wait()
        self.next_section()
