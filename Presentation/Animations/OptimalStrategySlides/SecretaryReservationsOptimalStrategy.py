from manim import *
from thesis.constants import *

import sys
sys.path.append("..")
from Utils import *
from OptimalStrategySlides import *

class SecretaryReservationsOptimalStrategy(BaseScene):
    def construct(self):
        self.setup_slide(title="SECRETARY PROBLEM")

        n = ValueTracker(5)
        c = ValueTracker(.2)

        self.viz = SecretaryStrategyVisualizer(round(n.get_value()), c.get_value())
        viz_updater = lambda x: x.set_nc(round(n.get_value()), c.get_value())
        self.viz.add_updater(viz_updater)
        self.add(self.viz)

        text_n = always_redraw(lambda: Tex(f"$n = {round(n.get_value())}$", color=WHITE, font_size=self.TITLE_FONT_SIZE).move_to(self.title_bar, aligned_edge=RIGHT).shift(.5 * LEFT))
        self.add(text_n)

        text_c = always_redraw(lambda: Tex(f"$c = {c.get_value():.2f}$", color=WHITE, font_size=self.TITLE_FONT_SIZE).move_to(text_n, aligned_edge=RIGHT).shift(2 * LEFT))
        self.add(text_c)

        self.wait()
        self.next_slide()

        # Increase n to 100
        self.play(n.animate.set_value(100), run_time=5)
        self.wait()
        self.next_slide()

        # Decrese c to 0
        self.play(c.animate.set_value(0), run_time=5)
        self.wait()
        self.next_slide()

        # Increase c to critical value
        self.play(c.animate.set_value(0.38), run_time=5)
        self.wait()
        self.next_slide()

        # Increase c to 1
        self.play(c.animate.set_value(1), run_time=5)
        self.wait()
        self.next_slide()

        # Move c back to .2
        self.play(c.animate.set_value(.2), run_time=5)
        self.wait()
        self.next_slide()

        # We are done with moving the values and now look at infinity
        self.viz.remove_updater(viz_updater)

        # # Look at infinity
        self.viz.set_nc(math.inf, c.get_value())
        self.wait()
        self.next_slide()

        # Infinity Theorem
        self.theorem = Theorem("Optimal Strategy $n \\to \infty$ and $c<1/e$", "Reject all 1-candidates until $t_1 = nc$, then reserve all 1-candidates until $t_2=n\mathcal{W}(e^{-c})$, after that accept the first 1-candidate.")
        self.theorem.shift(2.5*DOWN)
        self.play(
            self.viz.tight_and_shift(.7*UP),
            FadeIn(self.theorem)
        )

        self.wait()
        self.next_slide()
