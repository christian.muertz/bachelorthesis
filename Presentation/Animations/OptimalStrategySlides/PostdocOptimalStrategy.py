from manim import *
from thesis.constants import *

import sys
sys.path.append("..")
from Utils import *
from OptimalStrategySlides import *

class PostdocOptimalStrategy(BaseScene):
    def construct(self):
        self.setup_slide(title="POSTDOC PROBLEM")

        n = ValueTracker(5)
        c = ValueTracker(1)

        self.viz = PostdocBasicStrategyVisualizer(round(n.get_value()), c.get_value())
        viz_updater = lambda x: x.set_nc(round(n.get_value()), c.get_value())
        self.viz.add_updater(viz_updater)
        self.add(self.viz)

        text_n = always_redraw(lambda: Tex(f"$n = {round(n.get_value())}$", color=WHITE, font_size=self.TITLE_FONT_SIZE).move_to(self.title_bar, aligned_edge=RIGHT).shift(.5 * LEFT))
        self.add(text_n)

        self.wait()
        self.next_slide()

        self.play(n.animate.set_value(10), run_time=5)
        self.wait()
        self.next_slide()

        self.viz.remove_updater(viz_updater)

        # Infinity
        self.remove(text_n)
        self.viz.set_nc(math.inf, c.get_value())
        self.wait()
        self.next_slide()

        # Theorem
        self.theorem = Theorem("Optimal Strategy $n \\to \infty$", "Accept the first 2-candidate after the $n/2$-th candidate.")
        self.theorem.shift(2.8*DOWN)
        self.play(self.viz.tight_and_shift(.7*UP), FadeIn(self.theorem))

        self.wait()
        self.next_slide()
