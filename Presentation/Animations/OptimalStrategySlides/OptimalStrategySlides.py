import thesis.PostDoc.calculations
import thesis.Secretary.calculations


from manim import *
from manim_slides import Slide
from thesis.constants import *

import sys
sys.path.append("..")
from Utils import *

class SecretaryStrategyVisualizer(VGroup):
    cache_o = {}
    cache_d = {}
    cache_t1 = {}
    cache_t2 = {}

    def __init__(self, n, c):
        super().__init__()
        
        self.set_nc(n,c)

    def set_nc(self, n, c):
        if not self.submobjects:
            self.add(self.create_viz(n,c))

        self.submobjects[0].become(self.create_viz(n,c))

    def tight_and_shift(self, dir):
        new = self.submobjects[0].copy()
        new.arrange(DOWN, .3)
        new.shift(dir)
        return ReplacementTransform(self.submobjects[0], new)
        
    def create_viz(self, n, c):
        if n == math.inf:
            n = 1000
            inf = 1
        else:
            inf = 0

        if c not in self.cache_o:
            self.cache_o[c] = {}
        if c not in self.cache_d:
            self.cache_d[c] = {}
        if c not in self.cache_t1:
            self.cache_t1[c] = {}
        if c not in self.cache_t2:
            self.cache_t2[c] = {}

        o = self.cache_o[c][n] if n in self.cache_o[c] else thesis.Secretary.calculations.calculate_offset_matrix(int(n), c)
        self.cache_o[c][n] = o

        d = self.cache_d[c][n] if n in self.cache_d[c] else thesis.Secretary.calculations.calculate_decisions(o, c)
        self.cache_d[c][n] = d

        t1 = self.cache_t1[c][n] if n in self.cache_t1[c] else thesis.Secretary.calculations.t1(d)
        self.cache_t1[c][n] = t1

        t2 = self.cache_t2[c][n] if n in self.cache_t2[c] else thesis.Secretary.calculations.t2(d)
        self.cache_t2[c][n] = t2

        viz = CaseVisualizer(d[1:], labels={
            1: "$1$",
            t1: f"${t1}$" if not inf else "$t_1$",
            t2: f"${t2}$" if not inf else ("$1/e$" if c == 1 else "$t_2$"),
            int(n): f"${n}$" if not inf else "$\infty$"    
        }, title="1-candidates")

        legend = Legend()

        win = Tex(f"$P(win) = {f'{o[0][0]:.2f}' if not inf else '1/e'}$")
        if c != 1.0 and inf:
            win.set_opacity(0)

        group = VGroup(legend, viz, win)
        group.arrange(DOWN,buff=.7)

        return group

class PostdocBasicStrategyVisualizer(VGroup):
    cache_o = {}
    cache_d = {}
    cache_t1 = {}

    def __init__(self, n, c):
        super().__init__()
        
        self.set_nc(n,c)

    def set_nc(self, n, c):
        if not self.submobjects:
            self.add(self.create_viz(n,c))

        self.submobjects[0].become(self.create_viz(n,c))

    def tight_and_shift(self, dir):
        new = self.submobjects[0].copy()
        new.arrange(DOWN, .3)
        new.shift(dir)
        return ReplacementTransform(self.submobjects[0], new)
        
    def create_viz(self, n, c):
        if n == math.inf:
            n = 1000
            inf = 1
        else:
            inf = 0

        if c not in self.cache_o:
            self.cache_o[c] = {}
        if c not in self.cache_d:
            self.cache_d[c] = {}
        if c not in self.cache_t1:
            self.cache_t1[c] = {}

        o = self.cache_o[c][n] if n in self.cache_o[c] else thesis.PostDoc.calculations.calculate_offset_matrix(int(n), c)
        self.cache_o[c][n] = o

        d = self.cache_d[c][n] if n in self.cache_d[c] else thesis.PostDoc.calculations.calculate_decision_second_matrix(int(n), c)
        self.cache_d[c][n] = d

        t1 = self.cache_t1[c][n] if n in self.cache_t1[c] else [d[k][0][0] & ACTION_ACCEPT > 0 for k in range(n+1)].index(True)
        self.cache_t1[c][n] = t1

        viz = CaseVisualizer([d[k+1][0][0] for k in range(n)], labels={
            1: "$1$",
            t1: f"${t1}$" if not inf else "$1/2$",
            int(n): f"${n}$" if not inf else "$\infty$"    
        }, title="2-candidates")

        legend = Legend()

        win = Tex(f"P(win) = {f'{o[0][0][0]:.2f}' if not inf else '$1/4$'}")

        group = VGroup(legend, viz, win)
        group.arrange(DOWN,buff=.7)

        return group

class PostdocReservationsStrategyVisualizer(VGroup):
    def __init__(self, n, c):
        super().__init__()

        self.set_nc(n,c)

    def set_nc(self, n, c):
        if not self.submobjects:
            self.add(self.create_viz(n,c))

        self.submobjects[0].become(self.create_viz(n,c))

    cache_o = {}
    cache_d1 = {}
    cache_d2 = {}

    def create_viz(self, n, c):
        if c not in self.cache_o:
            self.cache_o[c] = {}
        if c not in self.cache_d1:
            self.cache_d1[c] = {}
        if c not in self.cache_d2:
            self.cache_d2[c] = {}

        o = self.cache_o[c][n] if n in self.cache_o[c] else thesis.PostDoc.calculations.calculate_offset_matrix(n, c)
        d1 = self.cache_d1[c][n] if n in self.cache_d1[c] else thesis.PostDoc.calculations.calculate_decision_first_matrix(n, c, o=o)
        d2 = self.cache_d2[c][n] if n in self.cache_d2[c] else thesis.PostDoc.calculations.calculate_decision_second_matrix(n, c, o=o)

        self.cache_o[c][n] = o
        self.cache_d1[c][n] = d1
        self.cache_d2[c][n] = d2

        t1 = thesis.PostDoc.calculations.t1(d1,d2,c)
        t1_str = f"${t1}$"
        t2 = thesis.PostDoc.calculations.t2(d1,d2,c)
        t2_str = f"${t2}$"
        t3 = thesis.PostDoc.calculations.t3(d1,d2,c)
        t3_str = f"${t3}$"
        t4 = thesis.PostDoc.calculations.t4(d1,d2,c)
        t4_str = f"${t4}$"
        t5 = thesis.PostDoc.calculations.t5(d1,d2,c)
        t5_str = f"${t5}$"

        n_str = f"${n}$"

        case1NO = CaseVisualizer([d1[k][0][0] for k in range(1,n+1)], {1: "$1$", t1: t1_str, t3: t3_str, n: n_str}, "1-candidate $\mid$ previous best not reserved")
        case1NO.move_to(3*LEFT + 1*UP)


        case1R= CaseVisualizer([d1[k][1][0] for k in range(1,n+1)], {1: "$1$", t1: t1_str, t3: t3_str, n: n_str}, "1-candidate $\mid$ previous best reserved")
        case1R.move_to(3*LEFT + 1.5*DOWN)


        case2NO = CaseVisualizer([d2[k][0][0] for k in range(1,n+1)], {1: "$1$", t2: t2_str, t4: t4_str, n: n_str}, "2-candidate $\mid$ best not reserved")
        case2NO.move_to(3*RIGHT + 1*UP)


        case2R= CaseVisualizer([d2[k][1][0] for k in range(1,n+1)], {1: "$1$", t2: t2_str, t5: t5_str, n: n_str}, "2-candidate $\mid$ best reserved")
        case2R.move_to(3*RIGHT + 1.5*DOWN)

        visualizer = VGroup()
        visualizer.add(case1NO, case1R, case2NO, case2R)

        legend = Legend()

        group = VGroup(legend, visualizer)
        group.arrange(DOWN, buff=.6)
        group.shift(.5*DOWN)

        return group

        
class LegendItem(VGroup):
    def __init__(self, color, text):
        super().__init__()

        rect = Rectangle(stroke_opacity=0, fill_opacity=1, fill_color=color, height=.25, width=.5)
        self.add(rect)

        text = Tex(text, font_size=BaseScene.FONT_SIZE).next_to(rect, RIGHT)
        self.add(text)

class Legend(VGroup):
    def __init__(self):
        super().__init__()

        group = VGroup()

        group.add(LegendItem(RWTH_LIGHT_BLUE_RGB, "Reject"))
        group.add(LegendItem(RWTH_BLUE_RGB, "Reserve"))
        group.add(LegendItem(RWTH_DEEP_BLUE_RGB, "Accept"))

        group.arrange(RIGHT, buff=.5)

        self.add(RoundedRectangle(corner_radius=.1, color=LIGHTER_GRAY, width=group.width + .5, height=group.height + .3).move_to(group))
        self.add(group)

class CaseVisualizer(VGroup):
    def __init__(self, decisions, labels, title):
        super().__init__()

        n = len(decisions)

        title = Tex(title, color=BLACK, font_size=BaseScene.FONT_SIZE)
        title.move_to(1*UP)

        hidden_box = Rectangle(color=BLACK, height=1, width=6, stroke_opacity=0)
        self.add(hidden_box)

        border = Rectangle(color=BLACK, height=1, width=5, stroke_width=3.0)

        for i in range(n):
            r = Rectangle(height=border.height, width=border.width/n + 0.01, fill_color=self.decision_color(decisions[i]), fill_opacity=1.0, stroke_opacity=0)

            x = border.get_left()[0] +border.width * i/n + r.width / 2

            r.move_to(np.array((x, 0, 0)))
            self.add(r)

            if i+1 in labels:
                x_tick = x
                tick = Line(np.array((x_tick, r.get_bottom()[1], 0)), np.array((x_tick, r.get_bottom()[1]-0.13, 0)), color=BLACK, stroke_width=3.0)

                l = Tex(labels[i+1], font_size=BaseScene.FONT_SIZE, color=BLACK)
                l.move_to(np.array((x_tick,tick.get_bottom()[1] - 0.2,0)))

                self.add(tick)
                self.add(l)


        self.add(title)
        self.add(border)


    def decision_color(self, d):
        if d & ACTION_IGNORE:  
            return RWTH_LIGHT_LIGHT_BLUE_RGB
        if d & ACTION_ACCEPT: 
            return RWTH_DEEP_BLUE_RGB
        if d & ACTION_RESERVE: 
            return RWTH_BLUE_RGB
        
        raise ValueError()
