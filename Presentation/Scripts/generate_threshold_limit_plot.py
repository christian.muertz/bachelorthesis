from thesis.constants import *
from constants import *
from thesis.PostDoc.calculations import *
from thesis.PostDoc.limits import *

import numpy as np
import matplotlib.pyplot as plt

width_multiplier = .5
aspect_ratio = 11/10 # width/height

plt.rcParams.update({
    "font.size" : 11,
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
})

step= 0.001
cs=[float(c) for c in np.arange(0.0,0.3+step, step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(cs, [cal_r5(c) for c in cs], label="$r_5$")
plt.plot(cs, [cal_r4(c) for c in cs], label="$r_4$")
plt.plot(cs, [cal_r3(c) for c in cs], label="$r_3$")
plt.plot(cs, [cal_r2(c) for c in cs], label="$r_2$")
plt.plot(cs, [cal_r1(c) for c in cs], label="$r_1$")

plt.xlabel("reservation cost")
# plt.ylabel("threshold ratio")

plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('THRESHOLD_LIMITS.pdf'),
    transparent=True
)
# plt.show()