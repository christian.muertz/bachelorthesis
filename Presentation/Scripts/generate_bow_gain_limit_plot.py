from constants import *
from thesis.BestOrWorst.calculations import *
from thesis.BestOrWorst.limits import *

import numpy as np
import matplotlib.pyplot as plt

width_multiplier = .5
aspect_ratio = 11/10 # width/height

plt.rcParams.update({
    "font.size" : font_size,
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
})

step= 0.001
cs=[float(c) for c in np.arange(0.0,0.4+step, step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(cs, [cal_gain(c) for c in cs])
plt.xlabel("reservation cost")
# plt.ylabel("expected gain")

plt.tight_layout()

plt.savefig(
    pdf_path('BOW_GAIN_LIMIT.pdf'),
    transparent=True
)