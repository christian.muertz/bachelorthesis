from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt
import numpy as np
import math

width_multiplier = 1
aspect_ratio = 20/10 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

def E00_t5(n, k, c):
    return f(n,k)

def E10_t5(n, k, c):
    return f(n,k) + h(n,k)

def E00_t4(n, k, c):
    return f(n,k)

def E10_t4(n, k, c):
    o = calculate_offset_matrix(n,c)
    d1 = calculate_decision_first_matrix(n,c,o=o)
    d2 = calculate_decision_second_matrix(n,c,o=o)
    
    t_5 = t5(d1,d2,c)

    return k/(t_5-1) * E10_t5(t_5-1) + k/n * (Harmonic(t_5-2) - Harmonic(k-1)) - c*(t_5 - 1 -k)/(t_5-1) + k*(t_5-1-k)/(n*(n-1))


def E00_t3(n, k, c):
    o = calculate_offset_matrix(n,c)
    d1 = calculate_decision_first_matrix(n,c,o=o)
    d2 = calculate_decision_second_matrix(n,c,o=o)
    
    t_4 = t4(d1,d2,c)

    return f(n,t_4-1) + (g(n,t_4-1)-g(n,k))/2 -  c*(Harmonic(t_4-1) - Harmonic(k))

def E10_t3(n, k, c):
    o = calculate_offset_matrix(n,c)
    d1 = calculate_decision_first_matrix(n,c,o=o)
    d2 = calculate_decision_second_matrix(n,c,o=o)
    
    t_4 = t4(d1,d2,c)

    return k*(E10_t4(t_4-1) + c)/(t_4-1) + (2*n + 3*k - t_4)*(t_4-1-k)/(2*n*(n-1)) -c - c*k * sum([(Harmonic(t_4-1) - Harmonic(i+1))/(i*(i+1)) for i in range(k,t_4-2+1)])

step = 0.01
x = np.arange(step,1, step)


reference = [E00_t3(1000, math.ceil(r*1000), 0.2) for r in x]
for n in [10,20,30,40,50,100, 200, 500]:
    print(n)
    plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

    plt.plot(x, reference, label="$E(r,0,0)$")
    plt.plot(x, [E00_t3(n, math.ceil(r*n), 0.2) for r in x], label="$E_n(\lceil rn \\rceil,0,0)$")
    
    plt.legend()
    plt.savefig(
        pdf_path(f'Uniform_Limit/n{n}-c0.2.pdf'),
        transparent=True
    )
