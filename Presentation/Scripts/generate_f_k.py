from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt

width_multiplier = 0.5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

for n in [5,10,50,100,1000]:
    x = range(1,n+1)
    k0 = math.ceil(n/2)

    plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
    plt.plot(x, [f(n,k) for k in x], marker="." if n < 1000 else "")
    plt.yticks([0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3])

    plt.tight_layout()

    plt.savefig(
        pdf_path(f'F_K/n{n}.pdf'),
        transparent=True
    )

# plt.show()