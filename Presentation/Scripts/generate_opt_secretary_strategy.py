from thesis.constants import *
from thesis.Secretary.calculations import *

from constants import *

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



text_width = 5.6704 # in inches
width_multiplier = .7

plt.rcParams.update({
    "font.size" : 11,
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
})
    
def color(d):
    if d & ACTION_IGNORE:  
        return RWTH_LIGHT_LIGHT_BLUE
    if d & ACTION_RESERVE: 
        return RWTH_BLUE
    if d & ACTION_ACCEPT: 
        return RWTH_DEEP_BLUE
    
    raise ValueError()

def generate(n,c):
    inf = (n == math.inf)
    if inf:
        n = 100000
        aspect_ratio = 2/1 # width/height
    else:
        aspect_ratio = 2/1.2 # width/height

    o = calculate_offset_matrix(n,c)
    d = calculate_decisions(o, c)

    c1 = [[
        color(d[k])
        for k in range(1,n+1)
    ]]

    fig, axs = plt.subplots(1,1,figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
    fig.canvas.draw()
    # fig.suptitle(f'Structure of the optimal strategy')

    if c == 1:
        values = ['Reject',  'Accept']
        colors = [RWTH_LIGHT_BLUE_NORM, RWTH_DEEP_BLUE_NORM]
    else:
        values = ['Reject', 'Reserve', 'Accept']
        colors = [RWTH_LIGHT_BLUE_NORM, RWTH_BLUE_NORM, RWTH_DEEP_BLUE_NORM]

    patches = [ mpatches.Patch(color=colors[i], label=values[i] ) for i in range(len(values)) ]
    fig.legend(handles=patches, loc='upper center', ncol=3, prop={'size': 11})

    axs.set_xlabel("candidate position")
    axs.set_title(f"1-candidate", fontsize=11)
    axs.imshow(c1, aspect=15*n/100, interpolation='none', rasterized=True)

    ticks = [0,n-1]
    labels = [1,n if not inf else "$n$"]
    if t1(d) >= 0:
        ticks.append(t1(d)-1)
        if not inf:
            labels.append(str(t1(d)))
        else:
            labels.append("$t_1$")
    if t2(d) >= 0:
        ticks.append(t2(d)-1)
        if not inf:
            labels.append(str(t2(d)))
        else:
            if c == 1:
                labels.append("$n/e$")
            else:    
                labels.append("$t_2$")

    axs.set_xticks(ticks, labels)
    axs.get_yaxis().set_visible(False)

    fig.subplots_adjust(bottom=0.1, top=0.9, left=0.05, right=0.95)

    if not inf:
        if c != 1:
            fig.text(0.5, 0.05, "$gain_{OPT}" + f' = {o[0][0]:.2f}$', color='black', ha='center')
        else:
            fig.text(0.5, 0.05, "$P(win)" + f' = {o[0][0]:.2f}$', color='black', ha='center')

    if not inf:
        fig.savefig(pdf_path(f'Secretary_Optimal_Strategy/n{n}-c{c}.pdf'))
    else:
        fig.savefig(pdf_path(f'Secretary_Optimal_Strategy/ninf-c{c}.pdf'))

    # plt.show()

generate(5,1)
generate(10,1)
generate(50,1)
generate(100,1)
generate(1000,1)
generate(10000,1)
generate(100000,1)
generate(math.inf,1)

generate(5,.2)
generate(10,.2)
generate(50,.2)
generate(100,.2)
generate(1000,.2)
generate(10000,.2)
generate(100000,.2)
generate(math.inf,.2)
generate(math.inf,.1)
generate(math.inf,.0)
generate(math.inf,.37)