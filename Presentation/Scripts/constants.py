import os

font_family="Computer Modern Sans Serif"
font_size = 11
text_width=5.6704

def pdf_path(name):
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), f"../Images/{name}"))
    os.makedirs(os.path.dirname(path), exist_ok=True)
    return path
