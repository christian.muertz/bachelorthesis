from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt
import numpy as np
import math

width_multiplier = 1
aspect_ratio = 20/10 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

n = 1000
c=0.2


o = calculate_offset_matrix(n,c)
d1 = calculate_decision_first_matrix(n,c,o=o)
d2 = calculate_decision_second_matrix(n,c,o=o)

t_5 = t5(d1,d2,c)
t_4 = t4(d1,d2,c)
t_3 = t3(d1,d2,c)

def E00_t5(n, k, c):
    return f(n,k)

def E10_t5(n, k, c):
    return f(n,k) + h(n,k)

def E00_t4(n, k, c):
    return f(n,k)

def E10_t4(n, k, c):
    return k/(t_5-1) * E10_t5(n, t_5-1, c) + k/n * (Harmonic(t_5-2) - Harmonic(k-1)) - c*(t_5 - 1 -k)/(t_5-1) + k*(t_5-1-k)/(n*(n-1))


def E00_t3(n, k, c):
    return f(n,t_4-1) + (g(n,t_4-1)-g(n,k))/2 -  c*(Harmonic(t_4-1) - Harmonic(k))

def E10_t3(n, k, c):
    return k*(E10_t4(n, t_4-1, c) + c)/(t_4-1) + (2*n + 3*k - t_4)*(t_4-1-k)/(2*n*(n-1)) -c - c*k * sum([(Harmonic(t_4-1) - Harmonic(i+1))/(i*(i+1)) for i in range(k,t_4-2+1)])

step = 0.01
x = np.arange(step,1, step)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(x, [E00_t3(n, math.ceil(r*n), c) for r in x], label="$E(r,0,0)$")
plt.plot(x, [E10_t3(n, math.ceil(r*n), c) - c for r in x], label="$E(r,1,0) - c$")

r3=t_3/n
plt.axvline(r3, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)
plt.xticks([0,0.2,0.4,0.6,0.8,1.0,r3], [0,0.2,0.4,0.6,0.8,1.0,"$r_3$"])

plt.legend()
plt.savefig(
    pdf_path(f'Uniform_Limit.pdf'),
    transparent=True
)
