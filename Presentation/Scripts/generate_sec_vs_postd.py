import thesis.Secretary.calculations as s
import thesis.PostDoc.calculations as p
import thesis.PostDoc.limits as pl

from constants import *
from thesis.constants import RWTH_LIGHT_BLUE_NORM

import math
import numpy as np
import matplotlib.pyplot as plt

width_multiplier = 1
aspect_ratio = 20/7 # width/height

plt.rcParams.update({
    "font.size" : 11,
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
})

step= 0.001
cs=[float(c) for c in np.arange(0.0,1+step, step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

def postdoc(c):
    print(c)
    return pl.cal_gain(c)

def secretary(c):
    print(c)
    return s.calculate_offset_matrix(1000,c)[0][0]

plt.plot(cs, [postdoc(c) for c in cs], label="Postdoc")
plt.plot(cs, [secretary(c) for c in cs], label="Secretary")

plt.xlabel("reservation cost")
plt.ylabel("expected gain")
plt.yticks([0.25, secretary(1), 0.5,.75, 1], ["0.25", "1/e", "0.50", "0.75", "1.00"])

plt.xticks([0, 0.25, 1/math.e, 1], ["0.00", "0.25", "1/e", "1.00"])

plt.axhline(secretary(1), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axhline(postdoc(1), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)

plt.axvline(0.37, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axvline(0.25, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)

plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('SEC_VS_POSTD.pdf'),
    transparent=True
)
# plt.show()