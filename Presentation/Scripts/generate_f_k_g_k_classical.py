from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt

width_multiplier = 0.5
aspect_ratio = 11/10 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

n = 1000
x = range(1,n+1)

k0 = math.ceil(n/2)
c = 0.25

o = calculate_offset_matrix(n,c)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
plt.plot(x, [g(n,k) for k in x], label="$g_k$")
plt.plot(x, [f(n,k) for k in x], label="$f_k$")

plt.xticks([0,k0,n], ["$0$", "$k_0$", "$n$"])
plt.yticks([0,float(f(n, k0)),0.5,0.75,1.0], ["$0$", "$f_{k_0}$", "$0.5$", "$0.75$", "$1.0$"])

plt.axhline(c, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axvline(k0, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('F_K_G_K.pdf'),
    transparent=True
)

# plt.show()