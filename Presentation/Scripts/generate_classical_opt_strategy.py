from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

text_width = 5.6704 # in inches
width_multiplier = .7

plt.rcParams.update({
    "font.size" : 11,
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
})
    
def color(d):
    if d & ACTION_ACCEPT: 
        return RWTH_DEEP_BLUE
    if d & ACTION_IGNORE:  
        return RWTH_LIGHT_LIGHT_BLUE
    if d & ACTION_RESERVE: 
        return RWTH_BLUE
    
    raise ValueError()

def generate(n):
    inf = (n == math.inf)
    if inf:
        n = 100000
        aspect_ratio = 2/1 # width/height
    else:
        aspect_ratio = 2/1.2 # width/height

    o = calculate_offset_matrix(n,1)
    d1 = calculate_decision_first_matrix(n,1, 1,o=o)
    d2 = calculate_decision_second_matrix(n,1, 1,o=o)

    c1 = [[
        color(d2[k][0][0])
        for k in range(1,n+1)
    ]]

    fig, axs = plt.subplots(1,1,figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
    fig.canvas.draw()
    # fig.suptitle(f'Structure of the optimal strategy')

    values = ['Reject',  'Accept']

    colors =  [RWTH_LIGHT_BLUE_NORM, RWTH_DEEP_BLUE_NORM]
    patches = [ mpatches.Patch(color=colors[i], label=values[i] ) for i in range(len(values)) ]
    fig.legend(handles=patches, loc='upper center', ncol=3, prop={'size': 11})

    axs.set_xlabel("candidate position")
    axs.set_title(f"2-candidate", fontsize=11)
    axs.imshow(c1, aspect=15*n/100, interpolation='none', rasterized=True)

    ticks = [0,n-1]
    labels = [1, (n if not inf else "n")]
    
    ticks.append(t3(d1,d2,1)-1)
    if not inf:
        labels.append(t3(d1,d2,1))
    else:
        labels.append('n/2')

    axs.set_xticks(ticks, labels)
    axs.get_yaxis().set_visible(False)

    fig.subplots_adjust(bottom=0.1, top=0.9, left=0.05, right=0.95)

    if not inf:
        fig.text(0.5, 0.05, f'$P(win) = {o[0][0][0]:.2f}$', color='black', ha='center')

    if not inf:
        fig.savefig(pdf_path(f'Postdoc_Optimal_Strategy/n{n}-c{1}.pdf'))
    else:
        fig.savefig(pdf_path(f'Postdoc_Optimal_Strategy/ninf-c{1}.pdf'))

    # plt.show()

generate(5)
generate(10)
generate(50)
generate(100)
generate(1000)
generate(10000)
generate(math.inf)
