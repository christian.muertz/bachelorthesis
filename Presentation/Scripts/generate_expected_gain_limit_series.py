from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt
import math

width_multiplier = 1
aspect_ratio = 20/10 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

def generate(n, s1=False, zoom=False, marker=".", name=""):
    print(f"Generating n={n}")
    if n == math.inf:
        inf = True
        n = 1000
    else:
        inf = False

    x = range(1,n+1)

    k0 = math.ceil(n/2)
    c = 0.2

    o = calculate_offset_matrix(n,c)

    plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

    d1 = calculate_decision_first_matrix(n,c,o=o)
    d2 = calculate_decision_second_matrix(n,c,o=o)
    plt.axvline(t3(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)
    plt.axvline(t4(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)

    if not zoom: 
        plt.axvline(t1(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)

    plt.xticks([1, t1(d1,d2,c), t3(d1,d2,c), t4(d1,d2,c), n], ["$1$" if not inf else "$0$", "$t_1$" if not inf else "$r_1$", "$t_3$" if not inf else "$r_3$", "$t_4$" if not inf else "$r_4$", f"${n}$" if not inf else "$1$"])

    if zoom:
        x = x[int(t3(d1,d2,c)- 0.02*n) : int(t4(d1,d2,c) + 0.02*n)]
        plt.xticks([t3(d1,d2,c), t4(d1,d2,c)], ["$t_3$" if not inf else "$r_3$", "$t_4$" if not inf else "$r_4$"])

    plt.plot(x, [o[k][0][0] for k in x], label="$E(k,0,0)$" if not inf else "$E(r,0,0)$", marker=marker if not inf else "")
    plt.plot(x, [o[k][1][0] - c for k in x], label="$E(k,1,0) - c$" if not inf else "$E(r,1,0) - c$", marker=marker if not inf else "")

    if s1:
        t_5 = t5(d1,d2,c)
        t_4 = t4(d1,d2,c)
        t_3 = t3(d1,d2,c)

        def E10_t5(k):
            return f(n,k) + h(n,k)

        def E10_t4(k):
            return k/(t_5-1) * E10_t5(t_5-1) + k/n * (Harmonic(t_5-2) - Harmonic(k-1)) - c*(t_5 - 1 -k)/(t_5-1) + k*(t_5-1-k)/(n*(n-1))

        def E00_t3(k):
            return f(n,t_4-1) + (g(n,t_4-1)-g(n,k))/2 -  c*(Harmonic(t_4-1) - Harmonic(k))

        def E10_t3(k):
            return k*(E10_t4(t_4-1) + c)/(t_4-1) + (2*n + 3*k - t_4)*(t_4-1-k)/(2*n*(n-1)) -c - c*k * sum([(Harmonic(t_4-1) - Harmonic(i+1))/(i*(i+1)) for i in range(k,t_4-2+1)])

        plt.plot(x[1:n], [E00_t3(k) for k in x[1:n]])
        plt.plot(x[1:n], [E10_t3(k)-c for k in x[1:n]])


    if not zoom:
        plt.ylim(-0.25,0.4)

    plt.legend(loc="upper right")
    plt.tight_layout()

    if not name:
        name = f'n{n if not inf else "inf"}-c{c:.2f}'
        if zoom:
            name += '-zoom'

    plt.savefig(
        pdf_path(f'Expected_Gain_Limit/{name}.pdf'),
        transparent=True
    )

    # plt.show()

for n in list(range(10,50,5)) +[50,100,200,300,500,1000,math.inf]:
    generate(n)

generate(1000, zoom=True)

generate(1000, zoom=True, s1=True, name="n1000-c0.20-s1-zoom")
generate(1000, zoom=False, s1=True, name="n1000-c0.20-s1", marker="")

generate(math.inf, zoom=False, s1=True, name="ninf-c0.20-s1", marker="")