from thesis.constants import *
from thesis.PostDoc.calculations import *

from constants import *

import matplotlib.pyplot as plt
import math

width_multiplier = 1
aspect_ratio = 20/8 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": font_family,
    "font.size": font_size,
})

n = 1000

x = range(1,n+1)

k0 = math.ceil(n/2)
c = 0.2

o = calculate_offset_matrix(n,c)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

d1 = calculate_decision_first_matrix(n,c,o=o)
d2 = calculate_decision_second_matrix(n,c,o=o)

t_5 = t5(d1,d2,c)
t_4 = t4(d1,d2,c)
t_3 = t3(d1,d2,c)

def E10_t5(k):
    return f(n,k) + h(n,k)

def E10_t4(k):
    return k/(t_5-1) * E10_t5(t_5-1) + k/n * (Harmonic(t_5-2) - Harmonic(k-1)) - c*(t_5 - 1 -k)/(t_5-1) + k*(t_5-1-k)/(n*(n-1))

def E00_t3(k):
    return f(n,t_4-1) + (g(n,t_4-1)-g(n,k))/2 -  c*(Harmonic(t_4-1) - Harmonic(k))

def E10_t3(k):
    return k*(E10_t4(t_4-1) + c)/(t_4-1) + (2*n + 3*k - t_4)*(t_4-1-k)/(2*n*(n-1)) -c - c*k * sum([(Harmonic(t_4-1) - Harmonic(i+1))/(i*(i+1)) for i in range(k,t_4-2+1)])

plt.plot(x[1:n], [E10_t3(k)-c-E00_t3(k) for k in x[1:n]])
plt.axhline(0, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)
plt.axvline(t3(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.8)

plt.xticks([1,t3(d1,d2,c), n], ["$0$", "$r_3$", "$1$"])

# plt.legend(loc="upper right")
plt.tight_layout()

plt.savefig(
    pdf_path(f'Expected_Gain_Limit/parabola.pdf'),
    transparent=True
)

# plt.show()