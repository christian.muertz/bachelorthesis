import os

def pdf_path(name):
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), f"../../Thesis/Images/{name}"))
    os.makedirs(os.path.dirname(path), exist_ok=True)
    return path