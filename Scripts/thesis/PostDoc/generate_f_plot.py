from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = 0.5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

n = 1000
x = range(1,n+1)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(x, [f(n,k) for k in x], color=RWTH_BLUE_NORM)
# plt.xlabel('$k$')
# plt.ylabel('$f_k$')

plt.tight_layout()

plt.savefig(
    pdf_path('F_K.pdf'),
    transparent=True
)