import torch
import math
from mpmath import *

mp.dps = 1000

def k0(n):
    return n//2

# k first will end up second
def f(n, k):
    return mpf(k) * mpf(n - k) / (mpf(n) * mpf(n - 1))

# k second will stay second
def g(n, k):
    return mpf(k) * mpf(k - 1) / (mpf(n) * mpf(n - 1))

def h(n, k):
    if k == 1:
        return 1/(n*(n-1))

    return g(n, k) * (Harmonic(n-2)-Harmonic(k-2))

def Harmonic(k):
    res = 0
    for i in range(1, k+1):
        res += 1/i
    return res

def accept1(n, k):
    return f(n,k)

def accept2(n, k):
    return g(n,k)

def reject1_0(o, k):
    return o[k+1][0][0]

def reject1_1(o, k):
    return o[k+1][0][1]

def reject2_0(o, k):
    return o[k+1][0][0]

def reject2_1(o, k):
    return o[k+1][1][0]

def reserve1_0(o, k, c):
    return o[k+1][1][0] - c

def reserve1_1(o, k, c):
    return o[k+1][1][1] - c

def reserve2_0(o, k, c):
    return o[k+1][0][1] - c

def reserve2_1(o, k, c):
    return o[k+1][1][1] - c

def calculate_offset_matrix(n, c, dps=1000):
    mp.dps = dps
    o = list(torch.zeros(n+2, 2, 2, dtype=torch.float64).tolist())

    # k = n
    for b in range(2):
        o[n][b][1] = mpf(1)
        o[n][b][0] = mpf(0)

     # 0 < k < n
    for k in reversed(range(0,n)):
        for b in range(2):
            for s in range(2):
                o[k][b][s] = mpf(k-1)/mpf(k + 1) * o[k+1][b][s]                                       # Next item is not first or second
                o[k][b][s] += mpf(1)/mpf(k+1) * max(o[k+1][0][b], o[k+1][1][b] - c)                  # Next item is locally first
                o[k][b][s] += mpf(1)/mpf(k+1) * max(o[k+1][b][0], o[k+1][b][1] - c)                  # Next item is locally second

    # Convert arbitrary precision back to normal float64
    for k in reversed(range(0,n+1)):
            for b in range(2):
                for s in range(2):
                    o[k][b][s] = float(o[k][b][s])

    return o

# Calculates the value matrix for number of elements n and cost per reservation c
def calculate_value_matrix(n, c, dps=1000):
    mp.dps = dps

    v = list(torch.zeros(n+2, n+2, 2, 2, dtype=torch.float64).tolist())
    o = calculate_offset_matrix(n, c, dps=dps)

    # 0 < k < n
    for k in reversed(range(0,n+1)):
        for r in range(0, n+1):
            for b in range(2):
                for s in range(2):
                    v[k][r][b][s] = -mpf(r)*mpf(c) + o[k][b][s]

    # Convert arbitrary precision back to normal float64
    for k in reversed(range(0,n+1)):
        for r in range(0, n+1):
            for b in range(2):
                for s in range(2):
                    v[k][r][b][s] = float(v[k][r][b][s])

    return v

ACTION_IGNORE = 1
ACTION_RESERVE = 2
ACTION_ACCEPT = 4

def calculate_decision_first_matrix(n, c):
    matrix = list(torch.zeros(n+2, 2, 2, dtype=torch.float64).tolist())
    o = calculate_offset_matrix(n, c)

    for k in reversed(range(0,n+1)):
        for b in range(2):
            for s in range(2):
                m = max(o[k][0][b], o[k][1][b] - c)

                matrix[k][b][s] = 0

                if math.isclose(m, o[k][0][b]):        # Ignore
                    matrix[k][b][s] += ACTION_IGNORE
                # if math.isclose(m, f(n, k)):         # Accept
                #     matrix[k][b][s] += ACTION_ACCEPT
                if math.isclose(m, o[k][1][b] - c):  # Reserve
                    matrix[k][b][s] += ACTION_RESERVE

    return matrix
                    

def calculate_decision_second_matrix(n, c):
    matrix = list(torch.zeros(n+2, 2, 2, dtype=torch.int).tolist())
    o = calculate_offset_matrix(n, c)

    for k in reversed(range(0,n+1)):
        for b in range(2):
            for s in range(2):
                m = max(o[k][b][0], o[k][b][1] - c)

                matrix[k][b][s] = 0

                if math.isclose(m, o[k][b][0]):      # Ignore
                    matrix[k][b][s] +=  ACTION_IGNORE
                if math.isclose(m, o[k][b][1] - c):  # Reserve
                    matrix[k][b][s] +=  ACTION_RESERVE
                # if math.isclose(m, g(n, k)):    # Accept
                #     matrix[k][b][s] +=  ACTION_ACCEPT
                    
    return matrix