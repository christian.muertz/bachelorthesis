from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations_no_accept import *

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

n = 1000
c = 0.15

dpi = int(os.environ.get('DPI', 600))
text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = 0.9
aspect_ratio = 3/1 # width/height

plt.rcParams.update({
    "font.size" : 11,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

def listRightIndex(alist, value):
    try:
        return len(alist) - alist[-1::-1].index(value) -1
    except:
        return -1
    
def color(d):
    if d & ACTION_IGNORE:
        return RWTH_LIGHT_LIGHT_BLUE
    if d & ACTION_RESERVE:
        return RWTH_BLUE
    
    raise ValueError()

d1 = calculate_decision_first_matrix(n, c)
d2 = calculate_decision_second_matrix(n, c)

c1 = [[
    color(d1[k][0][0])
    for k in range(1,n+1)
]]

c2 = [[
    color(d2[k][0][0])
    for k in range(1,n+1)
]]

fig, axs = plt.subplots(1,2,figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
fig.canvas.draw()
# fig.suptitle(f'Structure of the optimal strategy')

values = ['Reserve', 'Reject']
colors = [ value for value in [RWTH_BLUE_NORM, RWTH_LIGHT_BLUE_NORM]]
patches = [ mpatches.Patch(color=colors[i], label=values[i] ) for i in range(len(values)) ]
fig.legend(handles=patches, loc='upper center', ncol=2, prop={'size': 11})

axs[0].set_xlabel("k")
axs[0].set_title("Action for 1-candidates", fontsize=11)
axs[0].imshow(c1, aspect=15*n/100, interpolation='none')
axs[0].set_xticks([
    [d1[k][0][0] for k in range(0,n+1)].index(ACTION_RESERVE) - 1,
    listRightIndex([d1[k][0][0] for k in range(0,n+1)], ACTION_RESERVE) - 0.5
                   ], ["$r_1$","$r_2$"])
axs[0].get_yaxis().set_visible(False)

axs[1].set_xlabel("k")

axs[1].set_xticks([[d2[k][0][0] for k in range(0,n+1)].index(ACTION_RESERVE)- 1], ["$r_3$"])
axs[1].set_title(f"Action for 2-candidates", fontsize='small')
axs[1].imshow(c2, aspect=15*n/100, interpolation='none')
axs[1].get_yaxis().set_visible(False)

fig.tight_layout()
# fig.subplots_adjust(bottom=0.8)

fig.savefig(
    image_path('no_accept_optimal_strategy_structure.png'),
    dpi=dpi,
    transparent=True
)
# plt.show()