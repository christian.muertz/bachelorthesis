from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = 0.5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

n = 10000
x = range(1,n+1)

k0 = math.ceil(n/2)

def E00(k):
    if k >= k0:
        return f(n,k)
    return f(n,k0)

def E01(k):
    if k >= k0:
        return f(n,k) + g(n,k)
    return f(n,k0) + g(n,k)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
plt.plot(x, [E00(k) for k in x], label="$E(k,0,0)$")
plt.plot(x, [E01(k) for k in x], label="$E(k,0,1)$")

plt.xticks([0,k0,n], ["$0$", "$k_0$", "$n$"])
plt.yticks([0,float(f(n, k0)),0.5,0.75,1.0], ["$0$", "$f_{k_0}$", "$0.5$", "$0.75$", "$1.0$"])
# plt.xlabel('$k$')
# plt.ylabel('$g_k$')

plt.axhline(f(n,k0), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axvline(k0, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('E00_E10_CLASSICAL.pdf'),
    transparent=True
)

# plt.show()