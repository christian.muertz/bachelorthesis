from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

n = 10_000

cs = [
    0.025,
    0.05,
    0.075,
    0.1,
    0.125,
    0.15,
    0.175,
    0.2,
    0.225,
    # 0.25,
]

for c in cs:
    o = calculate_offset_matrix(n, c, 10)
    d1 = calculate_decision_first_matrix(n, c, 10)
    d2 = calculate_decision_second_matrix(n, c, 10)

    t_1 = t1(o,n,c)
    t_2 = t2(o,n,c)
    t_3 = t3(o,n,c)
    t_4 = t4(o,n,c)
    t_5 = t5(o,n,c)

    print(f'{"{:.3f}".format(c)} & {t_1/n} & {t_2/n} & {t_3/n} & {t_4/n} & {t_5/n} \\\\ ')