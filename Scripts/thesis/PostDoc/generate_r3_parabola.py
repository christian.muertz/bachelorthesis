import matplotlib.pyplot as plt
import numpy as np

from thesis.PostDoc.limits import *
from thesis.helper import *
from thesis.constants import *

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = .5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

step = 0.01
x = [float(x) for x in np.arange(0,1+step, step)]

def parabola(c, r):
    r_5 = cal_r5(c)
    r_4 = cal_r4(c)
    O_r4_1_0 = r_4 * math.log(r_5/r_4) + r_4 * (r_5 - r_4) - c*(1-2*r_4/r_5)

    p = 1-O_r4_1_0/r_4 - 2*r_4
    q = c

    return -r**2 -p*r - q

plt.axhline(0, color="grey", linestyle="--", alpha=0.7)
plt.plot(x, [parabola(0.0,r) for r in x], label="c=0.0")
plt.plot(x, [parabola(0.1,r) for r in x], label="c=0.1")
plt.plot(x, [parabola(0.2,r) for r in x], label="c=0.2")
plt.plot(x, [parabola(0.3,r) for r in x], label="c=0.3")
plt.legend()

plt.xlim([0,1])

plt.tight_layout()

# plt.show()

plt.savefig(
    pdf_path('R3_PARABOLAS.pdf'),
    transparent=True
)