from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *
from thesis.PostDoc.limits import *
import thesis.Secretary.calculations as s

import numpy as np
import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = .5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

step = 0.001
cs=[float(c) for c in np.arange(0,1+step,step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

def secretary(c):
    print(f"Secretary {c}")
    return s.calculate_offset_matrix(1000,c)[0][0]

def postdoc(c):
    print(f"Postdoc {c}")
    return cal_gain(c)


plt.plot(cs, [postdoc(c) for c in cs], label="Postdoc")
plt.plot(cs, [secretary(c) for c in cs], label="Secretary")
plt.yticks([0.25,secretary(1), 0.5,0.75,1], ["0.25", "1/e", "0.5", "0.75", "1.00"])

plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('GAIN_LIMIT.pdf'),
    transparent=True
)