import math
from thesis.PostDoc.calculations import *

n_row = "$n$"
f_row = "$f_k$"

for n in list(range(2,11)) + [100,1000]:
    n_row += f" & {n}"
    f_row += f" & {float(f(n,math.ceil(n/2))):.3f}"

print(n_row)
print(f_row)