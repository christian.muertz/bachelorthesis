import math

from scipy.optimize import fsolve

def cal_r5(c):
    res = fsolve(lambda r: r-r**2*(1+math.log(r))-c if r >0 else -1, 0.6, full_output=True)

    if res[2] == 1:
        return res[0][0]
    else:
        return cal_r3(c)


def cal_r4(c):
    try:
        return 1/2 + math.sqrt(1/4-c)
    except:
        return 1/2

def cal_r3(c):
    if c == 0:
        return 1
    
    try:
        r5 = cal_r5(c)
        r4 = cal_r4(c)
        b = math.log(r5/r4) + r5-r4-2*c*(1/r4 - 1/r5)
        return (r4+b)/2 + math.sqrt((r4 + b)**2/4 - c)
    except:
        return cal_r2(c)
    

def cal_r3_alternative(c):
    if c == 0:
        return 1
    
    try:
        r_5 = cal_r5(c)
        r_4 = cal_r4(c)
        O_r4_1_0 = r_4 * math.log(r_5/r_4) + r_4 * (r_5 - r_4) - c*(1-2*r_4/r_5)

        p = 1-O_r4_1_0/r_4 - 2*r_4
        q = c
        return -p/2 + math.sqrt((p/2)**2 - q)
    except:
        return cal_r2(c)
    
def cal_r2(c):
    if c < 0.25:
        return math.sqrt(c)
    else:
        return 0.5

def cal_r1(c):
    if c == 0:
        return 0
    
    r_5 = cal_r5(c)
    r_4 = cal_r4(c)
    r_3 = cal_r3(c)
    r_2 = cal_r2(c)
    
    O_r4_1_0 = r_4 * math.log(r_5/r_4) + r_4 * (r_5 - r_4) - c*(1-2*r_4/r_5)
    
    O_r3_0_0 = (r_4**2 - r_3**2)/2 + c*(1-math.log(r_4/r_3))
    O_r3_1_0 = (r_4**2 + 2*r_3*r_4 - 3*r_3**2)/2 + r_3 * (O_r4_1_0 - c)/r_4 + c*(1-math.log(r_4/r_3))

    O_r2_0_0 = r_2/r_3 * O_r3_0_0 + (r_3-r_2)/r_3 * (O_r3_1_0 - 2*c+ r_3 ** 2) - 2*c*(math.log(r_3 / r_2) + r_2/r_3 - 1)
    O_r2_1_0 = O_r3_1_0 - 2*c*math.log(r_3/r_2) - r_2**2 + r_3**2

    res = fsolve(lambda r: O_r2_1_0 + r_2**2/2 - c - r/r_2 * O_r2_0_0 - (r_2-r)/r_2 * (O_r2_1_0 - c + r_2**2/2) + r*r_2/2 + c * ( r/r_2 - 1) - r**2, 0, full_output=True)

    if res[2] == 1:
        return res[0][0]
    else:
        return r_2
    
def cal_r1_alternative(c):
    if c == 0:
        return 0
    
    r_5 = cal_r5(c)
    r_4 = cal_r4(c)
    r_3 = cal_r3(c)
    r_2 = cal_r2(c)
    
    O_r4_1_0 = r_4 * math.log(r_5/r_4) + r_4 * (r_5 - r_4) - c*(1-2*r_4/r_5)
    
    O_r3_0_0 = (r_4**2 - r_3**2)/2 + c*(1-math.log(r_4/r_3))
    O_r3_1_0 = (r_4**2 + 2*r_3*r_4 - 3*r_3**2)/2 + r_3 * (O_r4_1_0 - c)/r_4 + c*(1-math.log(r_4/r_3))

    O_r2_0_0 = r_2/r_3 * O_r3_0_0 + (r_3-r_2)/r_3 * (O_r3_1_0 - 2*c+ r_3 ** 2) - 2*c*(math.log(r_3 / r_2) + r_2/r_3 - 1)
    O_r2_1_0 = O_r3_1_0 - 2*c*math.log(r_3/r_2) - r_2**2 + r_3**2

    try:
        p = (O_r2_0_0 - O_r2_1_0 - r_2**2)/r_2
        return -p/2 - math.sqrt((p/2)**2 - c)
    except:
        return r_2
    
def cal_gain(c):
    if c == 0:
        return 1
    
    if c >= 0.25:
        return .25
    
    r_5 = cal_r5(c)
    r_4 = cal_r4(c)
    r_3 = cal_r3(c)
    r_2 = cal_r2(c)
    r_1 = cal_r1(c)


    O_r4_1_0 = r_4 * math.log(r_5/r_4) + r_4 * (r_5 - r_4) - c*(1-2*r_4/r_5)
    
    O_r3_0_0 = (r_4**2 - r_3**2)/2 + c*(1-math.log(r_4/r_3))
    O_r3_1_0 = (r_4**2 + 2*r_3*r_4 - 3*r_3**2)/2 + r_3 * (O_r4_1_0 - c)/r_4 + c*(1-math.log(r_4/r_3))

    O_r2_0_0 = r_2/r_3 * O_r3_0_0 + (r_3-r_2)/r_3 * (O_r3_1_0 - 2*c+ r_3 ** 2) - 2*c*(math.log(r_3 / r_2) + r_2/r_3 - 1)
    O_r2_1_0 = O_r3_1_0 - 2*c*math.log(r_3/r_2) - r_2**2 + r_3**2

    return r_1/r_2 * O_r2_0_0 + (r_2-r_1)/r_2 * (O_r2_1_0 - c + r_2**2/2) - (r_1*r_2 - r_1**2)/2 - c * (math.log(r_2/r_1) + r_1/r_2 -1)