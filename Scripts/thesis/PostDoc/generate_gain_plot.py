from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import numpy as np
import matplotlib.pyplot as plt
import os

n = 1000
step=0.001

text_width = 6 # in inches
width_multiplier = 0.5
aspect_ratio = 1.2/1 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

cs = np.arange(0,1+step,step)
gains = [1/4 for c in cs]

for i, c in enumerate(cs):
    print(c)
    o = calculate_offset_matrix(n, c, dps=20)
    gains[i] = o[0][0][0]

plt.figure(figsize=(text_width*width_multiplier,text_width*width_multiplier/aspect_ratio))

plt.plot(cs, gains)
plt.yticks([0, 0.25, 0.5, 0.75, 1])

plt.tight_layout()

plt.savefig(pdf_path(f'GAIN_PLOT.pdf'), transparent=True)