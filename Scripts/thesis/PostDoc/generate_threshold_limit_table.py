import numpy as np 
import warnings

from thesis.PostDoc.limits import *

warnings.simplefilter("ignore")

step=0.01
cs = np.arange(0, 0.25+step, step)

for c in cs:
    print(f'{c:.2f} & {cal_r5(c):.7f} &  {cal_r4(c):.7f} & {cal_r3(c):.7f} & {cal_r2(c):.7f} & {cal_r1(c):.7f} &{cal_gain(c):.7f}\\\\ ')