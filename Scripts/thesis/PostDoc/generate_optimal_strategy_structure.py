from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = 1
aspect_ratio = 2/1 # width/height

def generate(n,c, output=None, font_family="Computer Modern", text_width=LATEX_TEXT_WIDTH):
    plt.rcParams.update({
        "font.size" : LATEX_FONT_SIZE,
        "text.usetex": True,
        "font.family": font_family,
    })

    if not output:
        output = f"Postdoc_Optimal_Strategy/n{n}-c{c}.pdf"
        
    def color(d):            
        if d & ACTION_IGNORE:  
            return RWTH_LIGHT_LIGHT_BLUE
        if d & ACTION_ACCEPT: 
            return RWTH_DEEP_BLUE
        if d & ACTION_RESERVE: 
            return RWTH_BLUE

        
        raise ValueError()
    
    inf = (n == math.inf)
    if inf:
        n = 10000

    d1 = calculate_decision_first_matrix(n, c)
    d2 = calculate_decision_second_matrix(n, c)

    c1 = [[
        color(d1[k][0][0])
        for k in range(1,n+1)
    ]]

    c21 = [[
        color(d2[k][1][0])
        for k in range(1,n+1)
    ]]

    c22 = [[
        color(d2[k][0][0])
        for k in range(1,n+1)
    ]]

    fig, axs = plt.subplots(2,2,figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
    fig.canvas.draw()
    # fig.suptitle(f'Structure of the optimal strategy')

    values = ['Reject', 'Reserve', 'Accept']
    colors = [ value for value in [RWTH_LIGHT_BLUE_NORM, RWTH_BLUE_NORM, RWTH_DEEP_BLUE_NORM]]
    patches = [ mpatches.Patch(color=colors[i], label=values[i] ) for i in range(len(values)) ]
    fig.legend(handles=patches, loc='upper center', ncol=3, prop={'size': LATEX_FONT_SIZE})

    axs[0][0].set_title(f"1-candidate $\mid$ previous best not reserved", fontsize=LATEX_FONT_SIZE)
    axs[0][0].imshow(c1, aspect=15*n/100, interpolation='none', rasterized=True)
    axs[0][0].get_yaxis().set_visible(False)

    axs[1][0].set_xlabel("candidate position")
    axs[1][0].set_title(f"1-candidate $\mid$ previous best reserved", fontsize=LATEX_FONT_SIZE)
    axs[1][0].imshow(c1, aspect=15*n/100, interpolation='none', rasterized=True)
    axs[1][0].get_yaxis().set_visible(False)

    if t1(d1,d2,c) == t3(d1,d2,c) or (c ==.25 and inf):
        axs[0][0].set_xticks([0, t1(d1,d2,c) - 1, n-1], ["1", "$t_1/t_3$" if inf else t1(d1,d2,c), "n"])
        axs[1][0].set_xticks([0, t1(d1,d2,c) - 1, n-1], ["1", "$t_1/t_3$" if inf else t1(d1,d2,c), "n"])
    else:
        axs[0][0].set_xticks([0,t1(d1,d2,c) - 1, t3(d1,d2,c) - 1, n-1], ["1", "$t_1$" if inf else t1(d1,d2,c),"$t_3$" if inf else t3(d1,d2,c), "n"])
        axs[1][0].set_xticks([0,t1(d1,d2,c) - 1, t3(d1,d2,c) - 1, n-1], ["1", "$t_1$" if inf else t1(d1,d2,c),"$t_3$" if inf else t3(d1,d2,c), "n"])

    axs[1][1].set_xlabel("candidate position")
    axs[1][1].set_xticks([0, t2(d1,d2,c) - 1, t5(d1,d2,c) - 1, n-1], ["1", "$t_2$" if inf else t2(d1,d2,c), "$t_5$" if inf else t5(d1,d2,c),"n"])
    axs[1][1].set_title(f"2-candidate $\mid$ best reserved", fontsize=LATEX_FONT_SIZE)
    axs[1][1].imshow(c21, aspect=15*n/100, interpolation='none', rasterized=True)
    axs[1][1].get_yaxis().set_visible(False)

    # axs[0][1].set_xlabel("$k \\to$")
    if t2(d1,d2,c) == t4(d1,d2,c) or (c ==.25 and inf):
        axs[0][1].set_xticks([0, t2(d1,d2,c) - 1, n-1], ["1", "$t_2/t_4$" if inf else t2(d1,d2,c), "n"])
    else:
        axs[0][1].set_xticks([0, t2(d1,d2,c) - 1, t4(d1,d2,c) - 1, n-1], ["1", "$t_2$" if inf else t2(d1,d2,c), "$t_4$" if inf else t5(d1,d2,c), "n"])
    axs[0][1].set_title(f"2-candidate $\mid$ best not reserved", fontsize=LATEX_FONT_SIZE)
    axs[0][1].imshow(c22, aspect=15*n/100, interpolation='none', rasterized=True)
    axs[0][1].get_yaxis().set_visible(False)

    fig.subplots_adjust(bottom=0.1, top=0.8, left=0.05, right=0.95)
    # fig.tight_layout()

    fig.savefig(
        pdf_path(output), 
        # transparent=True
    )
    # plt.show()

generate(math.inf,0.2,output="OPTIMAL_STRATEGY_STRUCTURE.pdf")