from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *
from thesis.PostDoc.limits import *

import numpy as np
import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = .5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

step= 0.001
cs=[float(c) for c in np.arange(0.0,0.25+step, step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(cs, [cal_r3(c) for c in cs], label="$r_3$")

plt.xticks([0,0.05,0.1,0.15,0.2,0.25])
plt.xlim(0,0.25)

# plt.legend()
plt.tight_layout()

plt.savefig(
    pdf_path('R3_PLOT.pdf'),
    transparent=True
)
# plt.show()