from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import matplotlib.pyplot as plt

text_width = 6 # in inches
width_multiplier = 0.5
aspect_ratio = 11/10 # width/height


plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

n = 10000
x = range(1,n+1)

k0 = math.ceil(n/2)
c = 0.15

o = calculate_offset_matrix(n,c)
d1 = calculate_decision_first_matrix(n,c)
d2 = calculate_decision_second_matrix(n,c)

def E00(k):
    if k >= k0:
        return f(n,k)
    return f(n,k0)

def E01(k):
    if k >= k0:
        return f(n,k) + g(n,k)
    return f(n,k0) + g(n,k)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
plt.plot(x, [g(n,k) for k in x], label="$g_k$")
plt.plot(x, [f(n,k) for k in x], label="$f_k$")

plt.xticks([0,k0,n], ["$0$", "$k_0$", "$n$"])
plt.yticks([0,float(f(n, k0)),0.5,0.75,1.0], ["$0$", "$f_{k_0}$", "$0.5$", "$0.75$", "$1.0$"])
# plt.xlabel('$k$')
# plt.ylabel('$g_k$')

plt.axhline(c, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axvline(t2(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.axvline(t4(d1,d2,c), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('F_K_G_K_THRESHOLD_VIZ.pdf'),
    transparent=True
)

# plt.show()