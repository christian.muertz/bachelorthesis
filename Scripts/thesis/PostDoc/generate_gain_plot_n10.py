from thesis.constants import *
from thesis.helper import *
from thesis.PostDoc.calculations import *

import matplotlib.pyplot as plt

text_width = 5.6704 # in inches
width_multiplier = 0.5
aspect_ratio = 1.2/1 # width/height

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "Computer Modern Sans Serif",
    "font.size": 11
})

n = 10
c = 0.2
x = range(1,n+1)

k0 = math.ceil(n/2)

o = calculate_offset_matrix(n,c)

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
plt.plot(x, [o[k][0][0] for k in x], label="$E(k,0,0)$", linewidth=1.5)
plt.plot(x, [o[k][1][0] - c for k in x], label="$E(k,1,0) - c$", linewidth=1.5)
plt.plot(x, [f(n,k) for k in x], label="$f_k$", linewidth=1.5)

plt.xticks([0,k0,n], [0, k0, 10])
# plt.yticks([0,float(f(n, k0)),0.5,0.75,1.0], ["$0$", "$f_{k_0}$", "$0.5$", "$0.75$", "$1.0$"])
# plt.xlabel('$k$')
# plt.ylabel('$g_k$')

# plt.axhline(f(n,k0), color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
# plt.axvline(k0, color=RWTH_LIGHT_BLUE_NORM, linestyle='--', alpha=0.5)
plt.legend()

plt.tight_layout()

plt.savefig(
    pdf_path('N10_GAIN_PLOT.pdf'),
    transparent=True
)

# plt.show()