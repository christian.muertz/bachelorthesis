from thesis.constants import *
from thesis.helper import *
from thesis.BestOrWorst.calculations import *

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

n = 1000
c = 0.2

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = 1
aspect_ratio = 2/1 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})
    
def color(d):
    if d & ACTION_IGNORE:  
        return RWTH_LIGHT_LIGHT_BLUE
    if d & ACTION_RESERVE: 
        return RWTH_BLUE
    if d & ACTION_ACCEPT: 
        return RWTH_DEEP_BLUE
    
    raise ValueError()

d1 = calculate_decision_first_matrix(n, c)
d2 = calculate_decision_last_matrix(n, c)

c1 = [[
    color(d1[k][0][0])
    for k in range(1,n+1)
]]

c2 = [[
    color(d2[k][1][1])
    for k in range(1,n+1)
]]

fig, axs = plt.subplots(2,2,figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))
fig.canvas.draw()
# fig.suptitle(f'Structure of the optimal strategy')

values = ['Reject', 'Reserve', 'Accept']
colors = [ value for value in [RWTH_LIGHT_BLUE_NORM, RWTH_BLUE_NORM, RWTH_DEEP_BLUE_NORM]]
patches = [ mpatches.Patch(color=colors[i], label=values[i] ) for i in range(len(values)) ]
fig.legend(handles=patches, loc='upper center', ncol=3, prop={'size': 11})

# axs[0][0].set_xlabel("$k \\to$")
axs[0][0].set_title(f"best-candidate $\mid$ worst not reserved", fontsize=11)
axs[0][0].imshow(c1, aspect=15*n/100, interpolation='none')
axs[0][0].set_xticks([0,
    [d1[k][0][0] for k in range(0,n+1)].index(ACTION_RESERVE) - 1,
    listRightIndex([d1[k][0][0] for k in range(0,n+1)], ACTION_RESERVE) - 0.5,
                   n], ["1", "$t_1$","$t_2$", "n"])
axs[0][0].get_yaxis().set_visible(False)


axs[1][0].set_xlabel("candidate position")
axs[1][0].set_title(f"best-candidate $\mid$ worst reserved", fontsize=11)
axs[1][0].imshow(c2, aspect=15*n/100, interpolation='none')
axs[1][0].set_xticks([0,n], ["1", "n"])
axs[1][0].get_yaxis().set_visible(False)

# axs[0][1].set_xlabel("$k \\to$")
axs[0][1].set_title(f"worst-candidate $\mid$ best not reserved", fontsize=11)
axs[0][1].imshow(c1, aspect=15*n/100, interpolation='none')
axs[0][1].set_xticks([0,
    [d1[k][0][0] for k in range(0,n+1)].index(ACTION_RESERVE) - 1,
    listRightIndex([d1[k][0][0] for k in range(0,n+1)], ACTION_RESERVE) - 0.5,
                   n], ["1", "$t_1$","$t_2$", "n"])
axs[0][1].get_yaxis().set_visible(False)

axs[1][1].set_xlabel("candidate position")
axs[1][1].set_title(f"worst-candidate $\mid$ best reserved", fontsize=11)
axs[1][1].imshow(c2, aspect=15*n/100, interpolation='none')
axs[1][1].set_xticks([0,n], ["1", "n"])
axs[1][1].get_yaxis().set_visible(False)


fig.subplots_adjust(bottom=0.1, top=0.8, left=0.05, right=0.95)
# fig.tight_layout()

fig.savefig(
    pdf_path('BOW_OPTIMAL_STRATEGY_STRUCTURE.pdf'), 
    transparent=True
)