import math
from scipy.special import lambertw

def cal_r2(c):
    if c >= math.log(2)/2:
        return .5
    
    return math.exp(lambertw(-c))

def cal_r1(c):
    if c >= math.log(2)/2:
        return .5
    
    r2 = cal_r2(c)
    return math.sqrt(r2**2 * c / (4*r2**2 - 2 * r2 + c))

def cal_gain(c):
    if c == 0:
        return 1
    if c >= math.log(2)/2:
        return .5

    return cal_r2(c) - cal_r2(c)*math.log(cal_r2(c)) - c*math.log(cal_r2(c)/cal_r1(c))-c