import torch
import math
from mpmath import *

mp.dps = 1000

ACTION_IGNORE = 1
ACTION_RESERVE = 2
ACTION_ACCEPT = 4

def listRightIndex(alist, value):
    if value not in alist:
        return -1
    
    return len(alist) - alist[-1::-1].index(value) -1

# Probabilities

def F(n, k):
    return k/n * sum(1/i for i in range(k, n))

def calculate_offset_matrix(n, c, dps=1000):
    mp.dps = dps
    o = list(torch.zeros(n+2, 2, 2, dtype=torch.float64).tolist())

    # k = n
    o[n][1][1] = mpf(1)
    o[n][0][1] = mpf(1)
    o[n][1][0] = mpf(1)
    o[n][0][0] = mpf(0)

     # 1 < k < n
    for k in reversed(range(1, n)):
        for f in range(2):
            for l in range(2):
                o[k][f][l] = mpf(k-1)/mpf(k+1) * o[k+1][f][l]                                 # Next item is not first or last
                o[k][f][l] += mpf(1)/mpf(k+1) * max(o[k+1][0][l], (k+1)/n, o[k+1][1][l] - c)    # Next item is locally first
                o[k][f][l] += mpf(1)/mpf(k+1) * max(o[k+1][f][0], (k+1)/n, o[k+1][f][1] - c)    # Next item is locally last

    # k = 0
    o[0][0][0] = max(o[1][0][0], 1/n, o[1][1][1] - c)
    # the rest is irrelevant

    # Convert arbitrary precision back to normal float64
    for k in reversed(range(0,n+1)):
            for f in range(2):
                for l in range(2):
                    o[k][f][l] = float(o[k][f][l])

    return o

def calculate_decision_first_matrix(n, c):
    matrix = list(torch.zeros(n+2, 2, 2, dtype=torch.float64).tolist())
    o = calculate_offset_matrix(n, c)

    for k in reversed(range(0,n+1)):
        for f in range(2):
            for l in range(2):
                m = max(o[k][0][l], k/n, o[k][1][l] - c)       

                matrix[k][f][l] = 0

                if math.isclose(m, o[k][0][l]):         # Ignore
                    matrix[k][f][l] += ACTION_IGNORE
                if math.isclose(m, k/n):                # Accept
                    matrix[k][f][l] += ACTION_ACCEPT
                if math.isclose(m, o[k][1][l] - c):     # Reserve
                    matrix[k][f][l] += ACTION_RESERVE

    return matrix
                    

def calculate_decision_last_matrix(n, c):
    matrix = list(torch.zeros(n+2, 2, 2, dtype=torch.int).tolist())
    o = calculate_offset_matrix(n, c)

    for k in reversed(range(0,n+1)):
        for f in range(2):
            for l in range(2):
                m = max(o[k][f][0], k/n, o[k][f][1] - c)

                matrix[k][f][l] = 0

                if math.isclose(m, o[k][f][0]):      # Ignore
                    matrix[k][f][l] +=  ACTION_IGNORE
                if math.isclose(m, o[k][f][1] - c):  # Reserve
                    matrix[k][f][l] +=  ACTION_RESERVE
                if math.isclose(m, k/n):         # Accept
                    matrix[k][f][l] +=  ACTION_ACCEPT
                    
    return matrix