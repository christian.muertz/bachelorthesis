from thesis.constants import *
from thesis.helper import *
from thesis.BestOrWorst.calculations import *
from thesis.BestOrWorst.limits import *

import numpy as np
import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = .5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

step= 0.001
cs=[float(c) for c in np.arange(0.0,0.4+step, step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(cs, [cal_r2(c) for c in cs], label="$r_2$")
plt.plot(cs, [cal_r1(c) for c in cs], label="$r_1$")

plt.legend()
# plt.xticks([0,0.1,0.2,0.3,0.346], ["0.0", "0.1", "0.2", "0.3", "$\ln(\sqrt{2})$"])

plt.tight_layout()

plt.savefig(
    pdf_path('BOW_THRESHOLD_LIMITS.pdf'),
    transparent=True
)
# plt.show()