import numpy as np 
import warnings

from thesis.BestOrWorst.limits import *

warnings.simplefilter("ignore")

step=0.01
cs = np.arange(0, 0.37+step, step)

half_idx = len(cs)//2

for c1, c2 in zip(cs[:half_idx], cs[half_idx:]):
    print(f'{c1:.3f} & {cal_r1(c1):.6f} &  {cal_r2(c1):.6f} &  {cal_gain(c1):.6f} & {c2:.3f} & {cal_r1(c2):.6f} &  {cal_r2(c2):.6f} &  {cal_gain(c2):.6f} \\\\ ')