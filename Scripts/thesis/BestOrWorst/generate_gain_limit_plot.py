from thesis.constants import *
from thesis.helper import *
from thesis.BestOrWorst.calculations import *
from thesis.BestOrWorst.limits import *

import numpy as np
import matplotlib.pyplot as plt

text_width = LATEX_TEXT_WIDTH # in inches
width_multiplier = .5
aspect_ratio = 12/10 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

step = 0.001
cs=[float(c) for c in np.arange(0,0.4+step,step)]

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

plt.plot(cs, [cal_gain(c) for c in cs], color=RWTH_BLUE_NORM)
# plt.yticks([0.25,0.5,0.75,1])

plt.tight_layout()

plt.savefig(
    pdf_path('BOW_GAIN_LIMIT.pdf'),
    transparent=True
)