import torch
import math

from mpmath import *
from thesis.constants import *

mp.dps = 1000

def calculate_offset_matrix(n, c, dps=1000):
    mp.dps = dps
    o = list(torch.zeros(n+1, 2, dtype=torch.float64).tolist())

    # k = n
    o[n][1] = mpf(1)
    o[n][0] = mpf(0)

     # 1 <= k < n
    for k in reversed(range(1, n)):
        for b in range(2):
            o[k][b] = mpf(k)/mpf(k+1) * o[k+1][b]
            o[k][b] += mpf(1)/mpf(k+1) * max(o[k+1][0], (k+1)/n, o[k+1][1] - c)

    # k = 0
    o[0][0] = max(o[1][0], 1/n, o[1][1] - c)

    # Convert arbitrary precision back to normal float64
    for k in reversed(range(0,n+1)):
            for b in range(2):
                o[k][b]= float(o[k][b])

    return o

def calculate_decisions(o, c):
    n = len(o) - 1
    matrix = list(range(n+1))

    for k in reversed(range(0,n+1)):
        m = max(o[k][0], k/n, o[k][1] - c)       

        matrix[k] = 0

        if math.isclose(m, o[k][0]):         # Reject
            matrix[k] += ACTION_IGNORE
        if math.isclose(m, k/n):             # Accept
            matrix[k] += ACTION_ACCEPT
        if math.isclose(m, o[k][1] - c):     # Reserve
            matrix[k] += ACTION_RESERVE

    return matrix

def t1(decisions):
    if ACTION_RESERVE not in decisions:
        return -1
    return decisions.index(ACTION_RESERVE)

def t2(decisions):
    if ACTION_ACCEPT not in decisions:
        return -1
    
    return decisions.index(ACTION_ACCEPT)