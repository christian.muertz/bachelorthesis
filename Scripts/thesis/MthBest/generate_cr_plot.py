import os

import numpy as np
import matplotlib.pyplot as plt

from thesis.MthBest.calculations import *
from thesis.constants import *
from thesis.helper import *

n = 100
step=0.01
m = [1,2,3,4,5]

text_width = 6 # in inches
width_multiplier = 0.5
aspect_ratio = 1.2/1 # width/height

plt.rcParams.update({
    "font.size" : LATEX_FONT_SIZE,
    "text.usetex": True,
    "font.family": "Computer Modern",
})

plt.figure(figsize=(text_width*width_multiplier, text_width*width_multiplier/aspect_ratio))

for m in m:
    cs = np.arange(0,1+step,step)
    gains = [1/4 for c in cs]
    cr = [4 for c in cs]

    for i, c in enumerate(cs):
        print(f'm={m}, c={c}')
        o = calculate_offset_matrix(n, c, m, dps=20)
        gains[i] = o[0][0]
        cr[i] = 1 / gains[i]

    # plt.title("Select m-th best")
    # plt.xlabel("c")
    # plt.ylabel("CR")

    plt.plot(cs, cr, label=f"m={m}" if m <= 2 else None)

plt.legend()
plt.tight_layout()

plt.savefig(
    pdf_path('MTH_BEST_PLOT.pdf'),
    transparent=True
)
