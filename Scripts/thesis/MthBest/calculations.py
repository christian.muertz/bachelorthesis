import torch
import math
from mpmath import *

mp.dps = 1000

ACTION_IGNORE = 1
ACTION_RESERVE = 2
ACTION_ACCEPT = 4

# Current rank: j
# Final rank: m
# Seen so far: k
# Total: n
def calculate_ends_up_mth(n, k, j, m):
    numerator = j
    for i in range(m-j+1, m+1):
        numerator *= i
    for i in range(k-j+1, k+1):
        numerator *= i
    for i in range(n-k-m+j+1, n-k+1):
        numerator *= i

    denominator = m * math.factorial(j)
    for i in range(n-m+1, n+1):
        denominator *= i

    return mpf(numerator) / mpf(denominator)

def state_transition(m, state, action, j):
    if action == ACTION_IGNORE:
        new_state_ignore = ( state & (2**(j-1) - 1) ) | ( ( ( state & ((2**m - 2**(j-1))) ) << 1) % 2**m )
        return new_state_ignore
    elif action == ACTION_RESERVE:
        new_state_reserve = ( state & (2**(j-1) - 1) ) | ( ( ( state & ((2**m - 2**(j-1))) ) << 1) % 2**m ) | (1 << (j-1))
        return new_state_reserve
    else:
        raise ValueError()
    

def calculate_offset_matrix(n, c, m, dps=1000):
    mp.dps = dps
    o = list(torch.zeros(n+2, 2**m, dtype=torch.float64).tolist())

    # k = n
    for state in range(2**m):
        if (state >> (m-1)) % 2 == 1: 
            o[n][state] = 1
        else:
            o[n][state] = 0

     # 0 < k < n
    for k in reversed(range(0,n)):
        for state in range(2**m):
            o[k][state] = mpf(k+1-m)/mpf(k + 1) * o[k+1][state] # Next item is not in first m
            
            for j in range(m):
                j += 1

                new_state_ignore = state_transition(m,state,ACTION_IGNORE,j)
                new_state_reserve = state_transition(m,state,ACTION_RESERVE,j)

                o[k][state] += mpf(1)/mpf(k+1) * max(
                    o[k+1][new_state_ignore],
                    calculate_ends_up_mth(n, k+1, j, m),
                    o[k+1][new_state_reserve] - c
                ) # Next item is locally mth 

    # Convert arbitrary precision back to normal float64
    for k in reversed(range(0,n+1)):
            for state in range(2**m):
                o[k][state] = float(o[k][state])

    return o

def calculate_decision_matrix(n, c, m):
    matrix = list(torch.zeros(n+2, 2**m, m+1, dtype=torch.float64).tolist())
    o = calculate_offset_matrix(n, c, m)

    for k in reversed(range(0,n+1)):
        for state in range(2**m):
            for j in range(1, m+1):
                ignore_expected_gain = o[k][state_transition(m, state, ACTION_IGNORE, j)]
                accept_expected_gain = calculate_ends_up_mth(n, k, j, m)
                reserve_expected_gain = o[k][state_transition(m, state, ACTION_RESERVE, j)] - c 
                maxx = max(
                    ignore_expected_gain,
                    accept_expected_gain,
                    reserve_expected_gain
                )


                matrix[k][state][j] = 0
                if math.isclose(maxx, accept_expected_gain):   # Accept
                    matrix[k][state][j] += ACTION_ACCEPT
                if math.isclose(maxx, reserve_expected_gain):  # Reserve
                    matrix[k][state][j] += ACTION_RESERVE
                if math.isclose(maxx, ignore_expected_gain):   # Ignore
                    matrix[k][state][j] += ACTION_IGNORE

    return matrix