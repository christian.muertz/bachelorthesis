## Thesis
[Download latest version](https://git.rwth-aachen.de/christian.muertz/bachelorthesis/-/jobs/artifacts/main/raw/Thesis.pdf?job=build)

## Presentation
[Download latest version](https://git.rwth-aachen.de/christian.muertz/bachelorthesis/-/jobs/artifacts/main/raw/Presentation.pdf?job=build)